"""
All Rights Reserved.

Copyright (c) 2017-2019, Gyrfalcon technology Inc.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

"""Model helper functions."""
import os
from glob import glob
from random import shuffle
import tensorflow as tf
import tensorflow_addons as tfa
from focal_loss import *
import tensorflow.keras.backend  as K
import tqdm
import beat as beat_model
import numpy as np
import itertools
import beat_input as data_model
from utils.losses import *

FLAGS = tf.compat.v1.flags.FLAGS
_EPSILON = tf.keras.backend.epsilon


def categorical_focal_loss(alpha, gamma=2.):
    """
    Softmax version of focal loss.
    When there is a skew between different categories/labels in your data set, you can try to apply this function as a
    loss.
           m
      FL = ∑  -alpha * (1 - p_o,c)^gamma * y_o,c * log(p_o,c)
          c=1
      where m = number of classes, c = class and o = observation
    Parameters:
      alpha -- the same as weighing factor in balanced cross entropy. Alpha is used to specify the weight of different
      categories/labels, the size of the array needs to be consistent with the number of classes.
      gamma -- focusing parameter for modulating factor (1-p)
    Default value:
      gamma -- 2.0 as mentioned in the paper
      alpha -- 0.25 as mentioned in the paper
    References:
        Official paper: https://arxiv.org/pdf/1708.02002.pdf
        https://www.tensorflow.org/api_docs/python/tf/keras/backend/categorical_crossentropy
    Usage:
     model.compile(loss=[categorical_focal_loss(alpha=[[.25, .25, .25]], gamma=2)], metrics=["accuracy"], optimizer=adam)
    """

    alpha = np.array(alpha, dtype=np.float32)

    def categorical_focal_loss_fixed(y_true, y_pred):
        """
        :param y_true: A tensor of the same shape as `y_pred`
        :param y_pred: A tensor resulting from a softmax
        :return: Output tensor.
        """

        # Clip the prediction value to prevent NaN's and Inf's
        epsilon = K.epsilon()
        y_pred = K.clip(y_pred, epsilon, 1. - epsilon)

        # Calculate Cross Entropy
        cross_entropy = -y_true * K.log(y_pred)

        # Calculate Focal Loss
        loss = alpha * K.pow(1 - y_pred, gamma) * cross_entropy

        # Compute mean loss in mini_batch
        return K.mean(K.sum(loss, axis=-1))

    return categorical_focal_loss_fixed


class ConfusionMatrix(tf.keras.metrics.Metric):
    def __init__(self, classes, name='confusion_matrix'):
        super(ConfusionMatrix, self).__init__(name=name)
        self.save_matrix = self.add_weight(shape=(classes, classes), name='cm',
                                                initializer='zeros', dtype=tf.int32)
        self.num_of_class = classes

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_true = tf.argmax(y_true, axis=-1)
        y_pred = tf.argmax(y_pred, axis=-1)
        y_true = tf.keras.backend.flatten(y_true)
        y_pred = tf.keras.backend.flatten(y_pred)
        confusion_matrix = tf.math.confusion_matrix(labels=y_true, predictions=y_pred, num_classes=self.num_of_class)
        if sample_weight is not None:
            sample_weight = tf.cast(sample_weight, self.dtype)
            sample_weight = tf.broadcast_to(sample_weight, confusion_matrix.shape)
            confusion_matrix = tf.multiply(confusion_matrix, sample_weight)
        self.save_matrix.assign_add(confusion_matrix)

    def result(self):
        return self.save_matrix

    def reset_states(self):
        tf.keras.backend.set_value(self.save_matrix, np.zeros((self.num_of_class, self.num_of_class)))


def configure_model(
        name,
        label,
        gamma=3.0,
        learning_rate=1e-3,
):
    """Configure model for training & evaluation."""

    model = beat_model.build(name=name,
                             is_training=True,
                             inputs=FLAGS.beat_feature_len,
                             num_of_class=FLAGS.beat_class_num,
                             feature_len=FLAGS.beat_feature_len)

    if gamma > 0:
        # loss = SparseCategoricalFocalLoss(gamma=gamma, from_logits=True)
        loss = categorical_focal_loss(alpha=0.25, gamma=gamma)
    else:
        loss = tf.keras.losses.CategoricalCrossentropy(from_logits=False)
        # loss = jaccard_coef_logloss

    optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)

    metrics = [
        tf.keras.metrics.CategoricalAccuracy(name='accuracy'),
        ConfusionMatrix(classes=FLAGS.beat_class_num, name='confusion_matrix'),
        tf.keras.metrics.Recall(name='recall'),
        tf.keras.metrics.Precision(name='precision')
    ]
    beat = [c for _, c in enumerate(label.keys())]
    for i in range(FLAGS.beat_class_num):
        metrics.append(tf.keras.metrics.Recall(class_id=i, name='{}_Se'.format(beat[i])))
        metrics.append(tf.keras.metrics.Precision(class_id=i, name='{}_P'.format(beat[i])))

    model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    return model


def get_vars_to_restore(checkpoint_path):
    """Get list of variables to restore from checkpoint."""
    reader = tf.train.NewCheckpointReader(checkpoint_path)
    # map name to shape in checkpoint variables 
    checkpoint_vars = reader.get_variable_to_shape_map()
    # map name to variable in current graph variables 
    graph_vars = {v.name.split(":")[0]: v for v in tf.global_variables()}
    # intersection of the two sets of variables
    intersection = set(graph_vars.keys()).intersection(set(checkpoint_vars.keys()))
    vars_to_restore = []
    with tf.variable_scope('', reuse=True):
        for i in intersection:
            if graph_vars[i].get_shape().as_list() != checkpoint_vars[i]:
                raise Exception(
                    "Found variable {} with shape conflict, in graph {}, in checkpoint {}".
                        format(i, graph_vars[i].get_shape().as_list(), checkpoint_vars[i])
                )
            vars_to_restore.append(graph_vars[i])
    return vars_to_restore


def train_epoch(session, model_spec, num_samples, batch_size):
    coord = tf.train.Coordinator()
    train_op = model_spec["train_op"]
    metrics = model_spec["metrics"]
    confusion = model_spec["confusion"]
    update_metrics = model_spec["update_metrics"]
    session.run(model_spec["metrics_init_op"])
    num_steps = _calc_num_steps(num_samples, batch_size)
    threads = []
    for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
        if 'evaluating' in qr.name:
            continue
        threads.extend(qr.create_threads(session, coord=coord, daemon=True,
                                         start=True))

    confusion_mean = []
    for _ in tqdm.trange(num_steps):
        if coord.should_stop():
            break
        _, _, c = session.run([train_op, update_metrics, confusion])
        if len(confusion_mean) == 0:
            confusion_mean = c
        else:
            confusion_mean += c

    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=10)
    metrics_values = session.run({k: v[0] for k, v in metrics.items()})

    return metrics_values, confusion_mean


def eval_epoch(session, model_spec, num_samples, batch_size):
    coord = tf.train.Coordinator()
    metrics = model_spec["metrics"]
    confusion = model_spec["confusion"]
    update_metrics = model_spec["update_metrics"]
    session.run(model_spec["metrics_init_op"])
    num_steps = _calc_num_steps(num_samples, batch_size)
    threads = []
    for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
        if 'training' in qr.name:
            continue
        threads.extend(qr.create_threads(session, coord=coord, daemon=True,
                                         start=True))

    confusion_mean = []
    for _ in tqdm.trange(num_steps):
        _, c = session.run([update_metrics, confusion])
        if len(confusion_mean) == 0:
            confusion_mean = c
        else:
            confusion_mean += c

    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=10)

    metrics_values = session.run({k: v[0] for k, v in metrics.items()})
    return metrics_values, confusion_mean


def format_metrics(metrics, sep="; "):
    return sep.join("{}: {:.6f}".format(k, metrics[k]) for k in sorted(metrics.keys()))


def _calc_num_steps(num_samples, batch_size):
    return (num_samples + batch_size - 1) // batch_size


def _preprocess_proto(example_proto):
    """Read sample from protocol buffer."""
    encoding_scheme = {
        'sample': tf.io.FixedLenFeature(shape=[FLAGS.beat_feature_len, 1], dtype=tf.float32),
        'label': tf.io.FixedLenFeature(shape=[FLAGS.beat_block_len], dtype=tf.int64),
    }
    proto = tf.io.parse_single_example(example_proto, encoding_scheme)
    sample = proto["sample"]
    label = proto["label"]
    label = tf.one_hot(label, FLAGS.beat_class_num)
    return sample, label


def _get_tfrecord_filenames(dir_path, is_training):
    if not os.path.exists(dir_path):
        raise FileNotFoundError("{}; No such file or directory.".format(dir_path))
    filenames = sorted(glob(os.path.join(dir_path, "*.tfrecord")))
    if not filenames:
        raise FileNotFoundError("No TFRecords found in {}".format(dir_path))
    if is_training:
        shuffle(filenames)
    return filenames


def load_tfrecords(
        dir_path,
        batch_size=128,
        is_training=False,
        shuffle_buffer_size=8192):
    """Load TFRecords for training and evaluation on PC."""

    filenames = _get_tfrecord_filenames(dir_path, is_training)
    dataset = tf.data.TFRecordDataset(filenames)
    dataset = dataset.map(_preprocess_proto, num_parallel_calls=os.cpu_count())
    if is_training:  # for training, shuffle
        dataset = dataset.shuffle(buffer_size=shuffle_buffer_size)

    dataset = dataset.batch(batch_size)
    dataset = dataset.prefetch(batch_size * 5)
    return dataset


def read_and_decode(filename_queue):
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)
    features = tf.parse_single_example(
        serialized_example,
        # Defaults are not specified since both keys are required.
        features={
            'sample': tf.FixedLenFeature(shape=[FLAGS.beat_feature_len, 1],
                                         dtype=tf.float32),
            'label': tf.FixedLenFeature(shape=[FLAGS.beat_block_len], dtype=tf.int64),
        })
    signal = features['sample']

    if FLAGS.use_fp16:
        signal = tf.cast(signal, tf.float16)

    label = tf.cast(features['label'], tf.int32)
    return signal, label


def print_cm(cm, labels, normalize=False, hide_zeroes=False, hide_diagonal=False, hide_threshold=None):
    """pretty print for confusion matrixes"""
    columnwidth = max([len(x) for x in labels] + [12])  # 5 is value length
    empty_cell = " " * columnwidth
    # Print header
    cm_str = "    " + empty_cell + " "
    print("    " + empty_cell, end=" ")
    for label in labels:
        print("%{0}s".format(columnwidth) % label, end=" ")
        cm_str += "%{0}s".format(columnwidth) % label + " "
    print()
    cm_str += '\n'
    if normalize:
        cm = cm / np.reshape(cm.astype(np.float).sum(axis=1), (-1, 1))
    # Print rows
    for i, label1 in enumerate(labels):
        print("    %{0}s".format(columnwidth) % label1, end=" ")
        cm_str += "    %{0}s".format(columnwidth) % label1 + " "
        for j in range(len(labels)):
            cell = "%{0}.3f".format(columnwidth) % cm[i, j]
            if hide_zeroes:
                cell = cell if float(cm[i, j]) != 0 else empty_cell
            if hide_diagonal:
                cell = cell if i != j else empty_cell
            if hide_threshold:
                cell = cell if cm[i, j] > hide_threshold else empty_cell
            print(cell, end=" ")
            cm_str += cell + " "
        print()
        cm_str += '\n'

    return cm_str
