from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import csv
import json
import os
import re
import shutil
import socket
from os.path import basename, dirname
import tensorflow as tf
import beat_input as data_model
import train_utils
from utils.notiviaemail import NotiForm

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = '0'
USE_GPU_INDEX = 0
FLAGS = tf.compat.v1.flags.FLAGS


tf.compat.v1.flags.DEFINE_integer('beat_block_len', 240, '')
tf.compat.v1.flags.DEFINE_integer('beat_segment_len', 60, '')
tf.compat.v1.flags.DEFINE_float('beat_overlap', 0.0, '')
tf.compat.v1.flags.DEFINE_integer('beat_feature_len', 15360, '')
tf.compat.v1.flags.DEFINE_integer('beat_channel_num', 0, '')
tf.compat.v1.flags.DEFINE_float('beat_offset_len', 0.040, 'ms')
tf.compat.v1.flags.DEFINE_integer('beat_bwr', 3, '')
tf.compat.v1.flags.DEFINE_boolean('beat_norm', True, '')
tf.compat.v1.flags.DEFINE_string('beat_class', "0", '')
tf.compat.v1.flags.DEFINE_integer('beat_class_num', 2, '')


def build_logfile_name(dir_checkpoint, name):
    """Generate logfile name based on training configuration and model params"""
    return '{}checkpoint/{}'.format(dir_checkpoint, name)


def build_save_path(dir_checkpoint, name, version):
    """Generate logfile name based on training configuration and model params"""
    return '{}save/{}'.format(dir_checkpoint, name)


if __name__ == '__main__':
    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
    STUDIES = ['study-22489']

    # PATH_PORTAL_DATA = "/media/dattran/W10/PortalData/"
    PATH_PORTAL_DATA = "/mnt/ai_data/PortalData/data/study-22489"

    MODEL2RUN = '/mnt/ai_data/MegaDataset/beat_classification/201007/f1_score_report_best_metric_ready2run_20_10_07.csv'

    MEDIA_PATH_VERSION = '/mnt/ai_data/MegaDataset/beat_classification/'

    if os.path.exists(MODEL2RUN):
        with open(MODEL2RUN) as csv_file:
            reader = csv.DictReader(csv_file)
            rows = list(reader)
            totalrows = len(rows)
            dict_data = []
            for row, dict_row in enumerate(rows):
                print("+++++++++++++++++++++++++++++++" +
                      "[{}/{}] ".format(row + 1, totalrows) +
                      dict_row["Case"] + "+++++++++++++++++++++++++++++++\n\n")

                tf.compat.v1.reset_default_graph()

                checkpoint_dir = dict_row["Checkpoint"]
                path_check_point = dirname(dirname(dirname(checkpoint_dir)))
                # train_utils.correct_checkpoint_file(checkpoint_dir)
                with open('{}/num_samples.txt'.format(path_check_point), 'r') as json_file:
                    num_samples = json.load(json_file)

                    FLAGS.beat_block_len = num_samples["beat_block_len"]
                    FLAGS.beat_segment_len = num_samples["beat_segment_len"]
                    FLAGS.beat_overlap = num_samples["beat_overlap"]
                    # FLAGS.beat_overlap = 51
                    FLAGS.beat_channel_num = num_samples["beat_channel_num"]
                    FLAGS.beat_channel_num = 1
                    FLAGS.beat_offset_len = num_samples["beat_offset_len"]
                    FLAGS.beat_feature_len = num_samples["beat_feature_len"]
                    FLAGS.beat_bwr = num_samples["beat_bwr"]
                    FLAGS.beat_norm = num_samples["beat_norm"]
                    FLAGS.beat_class = num_samples["beat_class"]
                    FLAGS.beat_class_num = len(num_samples["beat_class_dict"].keys())

                ain_ext = train_utils.check_studies(use_gpu_index=USE_GPU_INDEX,
                                                    path2model_noise=None,
                                                    path2model_beat=checkpoint_dir,
                                                    path2portal_data=PATH_PORTAL_DATA,
                                                    study_ids=STUDIES,
                                                    num_of_process=1)

                if "ain_ext" not in dict_row:
                    dict_row["ain_ext"] = ain_ext

                dict_data.append(dict_row)

            try:
                csv_columns = ['#',
                               'Case',
                               'Error',
                               'Checkpoint',
                               'ain_ext']
                with open(MODEL2RUN, 'w') as csvfile:
                    writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
                    writer.writeheader()
                    for data in dict_data:
                        writer.writerow(data)

            except IOError:
                print("I/O error")

            # noti = \
            #     NotiForm("[{}]Noti from {}.".format(socket.gethostname(), basename(MODEL2RUN)),
            #              "Hello {},\n"
            #              "Your program has finished, please teamview to your computer to check.\n"
            #              "Thanks,".format("DatTran"))
            # noti.send_email('dattran@itrvn.com')
    else:
        print('{} is not exist'.format(MODEL2RUN))
