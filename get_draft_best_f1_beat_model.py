import os
import re
from glob import glob

import numpy as np
from datetime import datetime
import csv


def main():
    top_select = 1
    best_metric = True

    directory = '/mnt/ai_data/MegaDataset/beat_classification/210210'

    data_model = [x[0] for x in os.walk(directory)
                  if "beatv" in os.path.basename(x[0])]

    now = datetime.now()  # current date and time
    if best_metric:
        str_version = now.strftime("best_f1_%y_%m_%d")
    else:
        str_version = now.strftime("best_loss_ready2run_%y_%m_%d")

    squared_error_report_file = open(directory + '/{}.csv'.format(str_version), mode='w')
    squared_error_fieldnames = ['#',
                                'Case',
                                'Error',
                                'Checkpoint']
    squared_error_report_writer = csv.DictWriter(squared_error_report_file, fieldnames=squared_error_fieldnames)
    squared_error_report_writer.writeheader()

    metric_sel = []
    metric_model_name = []
    metric_checkpoint = []
    for i, g in enumerate(data_model):
        if os.path.exists(g + "/checkpoint"):
            models = os.listdir(g + "/checkpoint")
            folder = os.path.basename(g)
            for j, model in enumerate(models):
                # if "beat_seq2" in model:
                #     continue

                row_item = dict()
                row_item["#"] = i * len(models) + j
                log_file_name = g + "/{}_{}_training_log.txt".format(folder, model)
                if os.path.exists(log_file_name):
                    with open(log_file_name) as flog_file_name:
                        lines = flog_file_name.readlines()

                        if len(lines) > 0 and "End :" not in lines[-1] and "Beginning at epoch" not in lines[-1]:
                            continue

                check_exist_best_new_metric = sorted(glob(g + '/checkpoint/{}/best_new_f1'.format(model) + '/*.index'))
                index_best_new_metric = -1
                index_best_loss = -1
                if best_metric == 1 and len(check_exist_best_new_metric) > 0 and \
                        os.path.exists(g + '/checkpoint/{}/best_new_f1/checkpoint'.format(model)):

                    file_checkpoint = open(g + '/checkpoint/{}/best_new_f1/checkpoint'.format(model), "r")
                    line_checkpoint = file_checkpoint.readlines()
                    begin_at_epoch = line_checkpoint[-1].split('/')[-1][:-2]
                    index_best_new_metric = int(re.findall(r'\d+', begin_at_epoch)[-1])

                elif not best_metric and os.path.exists(g + '/checkpoint/{}/best_loss/checkpoint'.format(model)):
                    file_checkpoint = open(g + '/checkpoint/{}/best_loss/checkpoint'.format(model), "r")
                    line_checkpoint = file_checkpoint.readlines()
                    begin_at_epoch = line_checkpoint[-1].split('/')[-1][:-2]
                    index_best_loss = int(re.findall(r'\d+', begin_at_epoch)[-1])

                if index_best_new_metric == index_best_loss and index_best_loss < 0:
                    continue

                if os.path.exists(g + "/{}_{}_log.csv".format(folder, model)):
                    with open(g + "/{}_{}_log.csv".format(folder, model), mode='r') as infile:
                        reader = csv.DictReader(infile)
                        model_name = os.path.basename(g)
                        for row, dict_row in enumerate(reader):
                            if dict_row["epoch"].isdigit() and 0 <= index_best_new_metric == int(dict_row["epoch"]):
                                metric_sel.append(dict_row["f1_score_metrics_eval"])
                                row_item["Error"] = dict_row["f1_score_metrics_eval"]
                                model_name += "/{}/best_new_f1".format(model)
                                checkpoint = g + '/checkpoint/{}/best_new_f1'.format(model)
                                # checkpoint = checkpoint.replace(directory, "")
                                row_item["Checkpoint"] = checkpoint
                                break
                            elif dict_row["epoch"].isdigit() and 0 <= index_best_loss == int(dict_row["epoch"]):
                                metric_sel.append(dict_row["loss_eval"])
                                row_item["Error"] = dict_row["loss_eval"]
                                model_name += "/{}/best_loss".format(model)
                                checkpoint = g + '/checkpoint/{}/best_loss'.format(model)
                                # checkpoint = checkpoint.replace(directory, "")
                                row_item["Checkpoint"] = checkpoint
                                break

                        metric_model_name.append(model_name)
                        metric_checkpoint.append(checkpoint)
                        row_item["Case"] = model_name

                    squared_error_report_writer.writerow(row_item)

    squared_error_report_file.close()

    index_squared_error_gross = np.asarray([x for _, x in sorted(zip(metric_sel,
                                                                     np.arange(len(metric_sel))),
                                                                 reverse=False)])

    if len(index_squared_error_gross) < top_select:
        top_select = len(index_squared_error_gross)

    gtmp = directory.split('/')
    gt = 0

    import getpass
    for gt, gstr in enumerate(gtmp):
        if getpass.getuser() in gstr:
            break

    dir_model_selected = ""
    for gm in range(gt, len(gtmp) - 1):
        dir_model_selected += gtmp[gm] + "/"

    if not os.path.exists('model_selected/{}'.format(dir_model_selected)):
        os.makedirs('model_selected/{}'.format(dir_model_selected))

    # if not os.path.exists('model_selected/{}/{}.csv'.format(dir_model_selected, str_version)):
    all_model_2_run_file = open('model_selected/{}/{}.csv'.format(dir_model_selected, str_version),
                                mode='w')
    model_2_run_fieldnames = ['#',
                              'Case',
                              'Error',
                              'Checkpoint']
    all_model_2_run_writer = csv.DictWriter(all_model_2_run_file, fieldnames=model_2_run_fieldnames)
    all_model_2_run_writer.writeheader()
    count = 0
    all_row = dict()

    model_2_run = open('model_selected/{}/{}_{}.csv'.format(dir_model_selected, str_version, 0),
                       mode='w')
    model_2_run_writer = csv.DictWriter(model_2_run, fieldnames=model_2_run_fieldnames)
    model_2_run_writer.writeheader()

    for n, g in enumerate(index_squared_error_gross):
        row = dict()
        row["#"] = n
        all_row["#"] = count

        row["Case"] = metric_model_name[g]
        row["Error"] = metric_sel[g]
        row["Checkpoint"] = metric_checkpoint[g]
        all_row["Case"] = metric_model_name[g]
        all_row["Error"] = metric_sel[g]
        all_row["Checkpoint"] = metric_checkpoint[g]
        all_model_2_run_writer.writerow(all_row)
        count += 1
        if n < top_select:
            model_2_run_writer.writerow(row)

    model_2_run.close()

    all_model_2_run_file.close()

    # print('\n>>>>>>>>>>>>>>>>>> {} with error {}<<<<<<<<<<<<<<<<<'.
    #       format(os.path.dirname(metric_model_name[index_squared_error_gross[0]]),
    #              metric_sel[index_squared_error_gross[0]]))


if __name__ == "__main__":
    main()
