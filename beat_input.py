from datetime import datetime
import inspect
import sys
import re
import csv
import numpy as np
from glob import glob
from multiprocessing import Process, JoinableQueue, Lock
import wfdb as wf
import copy
from matplotlib import pyplot as plt
from wfdb.processing import resample_singlechan, resample_sig
from utils.reprocessing import (
    bwr,
    bwr_dwt,
    bwr_dwt2,
    norm,
    beat_annotations,
    butter_bandpass_filter,
    butter_lowpass_filter,
    butter_highpass_filter,
    agglomerative_clustering,
    select_beat_from_group)
import os
from os.path import basename, dirname
import tensorflow as tf
import json
import random
import shutil
import beat as model
from collections import OrderedDict
import train_utils

# SERVING
DEFAULT_TF_SERVER_NAME = '172.17.0.2'
DEFAULT_TF_SERVER_PORT = 9000
# SYS
SAMPLING_RATE = 256
NUM_NORMALIZATION = int(0.6 * SAMPLING_RATE)
HES_SAMPLING_RATE = 200
MAX_SAMPLE_PROCESS = int(15 * 60)
NUM_OVERLAP = float(60.0)
BATCH_SIZE = 1024
MIN_RR_INTERVAL = 0.2

LABEL_ARRHYTHMIA = OrderedDict(
    [("0", OrderedDict([("NOTBEAT", []),
                        ("NOR", ["N", "L", "R", "A", "a", "S", "J", "e", "j"]),
                        ("VES", ["V", "E"])])),
     ("1", OrderedDict([("NOTBEAT", []),
                        ("NOR", ["N", "A", "a", "S", "J", "e", "j"]),
                        ("B", ["L", "R"]),
                        ("VES", ["V", "E"])])),
     ("2", OrderedDict([("NOTBEAT", []),
                        ("NOR", ["N", "A", "a", "S", "J", "e", "j"]),
                        ("L", ["L"]),
                        ("R", ["R"]),
                        ("VES", ["V", "E"])])),
     ("3", OrderedDict([("NOTBEAT", []),
                        ("NOR", ["N", "L", "R"]),
                        ("VES", ["V", "E"]),
                        ("SVES", ["A", "a", "S", "J", "e", "j"])])),
     ("4", OrderedDict([("NOTBEAT", []),
                        ("NOR", ["N", "L", "R", "A", "a", "S", "J", "e", "j", "n"]),
                        ("VES", ["V", "E"]),
                        ("OTHER", ["/", "Q", "?", "r", "F", "!"])])),
     ("5", OrderedDict([("NOTBEAT", []),
                        ("NOR", ["N", "L", "R", "e", "j", "n"]),
                        ("VES", ["V", "E"]),
                        ("SVES", ["A", "a", "S", "J"]),
                        ("OTHER", ["/", "Q", "?", "r", "F", "!"])])),
     ("6", OrderedDict([("NOTBEAT", []),
                        ("NOR", ["N", "L", "R", "A", "a", "S", "J", "e", "j", "n"]),
                        ("VES", ["V", "E"]),
                        ("OTHER", ["/", "Q", "?", "r", "F", "f", "!"])])),
     ("7", OrderedDict([("NOTBEAT", []),
                        ("NOR", ["N", "L", "R", "e", "j", "n"]),
                        ("VES", ["V", "E"]),
                        ("SVES", ["A", "a", "S", "J"]),
                        ("OTHER", ["/", "Q", "?", "r", "F", "f", "!"])])),
     ("8", OrderedDict([("NOTBEAT", []),
                        ("NOR", ["N", "L", "R", "e", "j", "n", "F", "f"]),
                        ("VES", ["V", "E", "!", "r"]),
                        ("SVES", ["A", "a", "S", "J"])])),
     ("9", OrderedDict([("NOTBEAT", []),
                        ("NOR", ["N", "L", "R", "e", "j", "n", "F", "f", "V", "E", "!", "r", "A", "a", "S", "J"])])),
     ("10", OrderedDict([("NOTBEAT", []),
                         ("NOR", ["N", "L", "R", "e", "j", "n", "F", "f", "V", "E", "!", "r", "A", "a", "S", "J"]),
                         ("ARTIFACTS", ["~"])])),
     ("11", OrderedDict([("NOTBEAT", []),
                         ("NOR", ["N", "L", "R", "e", "j", "F", "B"]),
                         ("VES", ["V", "E", "!"]),
                         ("SVES", ["A", "a", "S", "J", "n"])])),
     ("12", OrderedDict([("NOTBEAT", []),
                         ("NOR", ["N", "L", "R", "B"]),
                         ("VES", ["V", "E", "r"]),
                         ("SVES", ["A", "a", "J", "S", "e", "j", "n"])])),
     ("13", OrderedDict([("NOTBEAT", []),
                         ("NOR", ["N", "L", "R", "e", "J"]),
                         ("VES", ["V", "E"]),
                         ("SVES", ["A", "a", "S", "j"])])),
     ])


LABEL_NOISE = OrderedDict([("USABLE", ['N', 'L', 'R', 'A', 'V', '/', 'a', '!', 'F', 'j', 'f', 'E', 'J', 'e', 'Q', 'S']),
                           ("UNUSABLE", ['~'])])

FLAGS = tf.compat.v1.flags.FLAGS

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = '1'


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


def update(d, other):
    d.update(other)
    return d


def _int64_feature(value):
    """Wrapper for inserting int64 features into Example proto."""
    if not isinstance(value, list):
        value = [value]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def _bytes_feature(value):
    """Wrapper for inserting bytes features into Example proto."""
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def np_to_tfrecords(sample_buffer, label_buffer, writer):
    def _dtype_feature(ndarray):
        """match appropriate tf.train.Feature class with dtype of ndarray. """
        assert isinstance(ndarray, np.ndarray)
        dtype_ = ndarray.dtype
        if dtype_ == np.float64 or dtype_ == np.float32:
            return lambda array: tf.train.Feature(float_list=tf.train.FloatList(value=array))
        elif dtype_ == np.int64:
            return lambda array: tf.train.Feature(int64_list=tf.train.Int64List(value=array))
        else:
            raise ValueError("The input should be numpy ndarray. \
                               Instaed got {}".format(ndarray.dtype))

    assert isinstance(sample_buffer, np.ndarray)
    assert len(sample_buffer.shape) == 2  # If X has a higher rank,
    # it should be rshape before fed to this function.
    assert isinstance(label_buffer, np.ndarray) or label_buffer is None

    # load appropriate tf.train.Feature class depending on dtype
    dtype_feature_x = _dtype_feature(sample_buffer)
    if label_buffer is not None:
        assert sample_buffer.shape[0] == label_buffer.shape[0]
        assert len(label_buffer.shape) == 2
        dtype_feature_y = _dtype_feature(label_buffer)

    # iterate over each sample,
    # and serialize it as ProtoBuf.
    for idx in range(sample_buffer.shape[0]):
        x = sample_buffer[idx]
        if label_buffer is not None:
            y = label_buffer[idx]

        d_feature = dict()
        d_feature['sample'] = dtype_feature_x(x)
        if label_buffer is not None:
            d_feature['label'] = dtype_feature_y(y)

        features = tf.train.Features(feature=d_feature)
        example = tf.train.Example(features=features)
        serialized = example.SerializeToString()
        writer.write(serialized)


def cal_num_process_and_num_shard(files, org_num_processes, org_num_shards):
    if len(files) >= org_num_shards:
        num_processes = org_num_processes
        num_shards = org_num_shards
    else:
        for n_threads in reversed(range(org_num_processes)):
            if len(files) // n_threads >= 1:
                num_processes = n_threads
                num_shards = (len(files) // n_threads) * n_threads
                break
    return num_processes, num_shards


def create_dataset_subject_based_tfrecord(use_gpu_index,
                                          data_model_dir,
                                          train_dir,
                                          eval_dir,
                                          data_dir,
                                          db_train,
                                          db_eval,
                                          save_image,
                                          org_num_processes=os.cpu_count(),
                                          org_num_shards=os.cpu_count()):
    """

    :param use_gpu_index:
    :param check_point_qrs:
    :param data_model_dir:
    :param train_dir:
    :param eval_dir:
    :param data_dir:
    :param db_train:
    :param db_eval:
    :param save_image:
    :param org_num_processes:
    :param org_num_shards:
    :return:
    """

    if not os.path.exists(data_model_dir):
        os.makedirs(data_model_dir)
    else:
        shutil.rmtree(data_model_dir)
        os.makedirs(data_model_dir)

    if not os.path.exists(train_dir):
        os.makedirs(train_dir)
    else:
        shutil.rmtree(train_dir)
        os.makedirs(train_dir)

    if not os.path.exists(eval_dir):
        os.makedirs(eval_dir)
    else:
        shutil.rmtree(eval_dir)
        os.makedirs(eval_dir)

    fstatus = open(data_model_dir + '/start.txt', 'w')
    fstatus.writelines(str(datetime.now()))
    fstatus.close()

    res_db_dict = dict()
    res_db_dict["train"] = dict()
    res_db_dict["eval"] = dict()
    res_db_dict["beat_block_len"] = FLAGS.beat_block_len
    res_db_dict["beat_segment_len"] = FLAGS.beat_segment_len
    res_db_dict["beat_overlap"] = FLAGS.beat_overlap
    res_db_dict["beat_channel_num"] = FLAGS.beat_channel_num
    res_db_dict["beat_offset_len"] = FLAGS.beat_offset_len
    res_db_dict["beat_feature_len"] = FLAGS.beat_feature_len
    res_db_dict["beat_bwr"] = FLAGS.beat_bwr
    res_db_dict["beat_norm"] = FLAGS.beat_norm
    res_db_dict["beat_class"] = FLAGS.beat_class
    res_db_dict["beat_class_num"] = FLAGS.beat_class_num
    res_db_dict["beat_class_dict"] = LABEL_ARRHYTHMIA[FLAGS.beat_class]
    res_db_dict["beat_db_train"] = db_train
    res_db_dict["beat_db_eval"] = db_eval

    res_db_dict['previous_shards'] = dict()
    res_db_dict['previous_shards']['train'] = 0
    res_db_dict['previous_shards']['eval'] = 0

    res_db_dict["train"]["total_sample"] = 0
    res_db_dict["eval"]["total_sample"] = 0
    for key in LABEL_ARRHYTHMIA[FLAGS.beat_class].keys():
        res_db_dict["eval"][key] = 0
        res_db_dict["train"][key] = 0

    lst_file_to_train = list()
    lst_file_to_eval = list()

    # region create data training
    db_process_info = dict()
    try:
        for _, db in enumerate(db_train):
            db_process_info[db[0]] = dict()
            files = glob(data_dir.format(db[0]) + '/*.dat')
            files = [p[:-4] for p in files
                     if basename(p)[:-4] not in ['07162', '104', '102', '107', '217', 'bw', 'em', 'ma']
                     if '_200hz' not in basename(p)[:-4]
                     if '56' != basename(p)[:-4]]

            inx_random = np.arange(len(files))
            np.random.shuffle(inx_random)
            files = np.asarray(files)[inx_random]

            ds_lbl_train = None
            ds_file_train = files.copy()

            num_processes, num_shards = cal_num_process_and_num_shard(ds_file_train, org_num_processes, org_num_shards)

            db_process_info[db[0]]['ds_train'] = ds_file_train
            db_process_info[db[0]]['ds_train_lbl'] = ds_lbl_train
            db_process_info[db[0]]['processors'] = num_processes
            db_process_info[db[0]]['shards'] = num_shards

        total_shards = np.asarray([db_process_info[k]['shards'] for k in db_process_info]).sum()
        for _, db in enumerate(db_train):
            res_db_dict, lst_file_to_train = \
                build_tfrecord(use_gpu_index=use_gpu_index,
                               db_process_info=db_process_info[db[0]],
                               total_shards=total_shards,
                               res_db_dict=res_db_dict,
                               lst_file_to_handle=lst_file_to_train,
                               ann_ext_all=[db[1] for _ in range(len(db_process_info[db[0]]['ds_train']))],
                               output_train_directory=train_dir,
                               output_eval_directory=None,
                               save_image=save_image)

            print('num_train_samples = {}'.format(res_db_dict['train']['total_sample']))
    except Exception as e:
        print(e)
        print('SKIPPED: Unexpected error while decoding train %s.' % db_train[0])
    # endregion create data training

    # region create data eval
    db_process_info = dict()
    try:
        for _, db in enumerate(db_eval):
            db_process_info[db[0]] = dict()
            files = glob(data_dir.format(db[0]) + '/*.dat')
            files = [p[:-4] for p in files
                     if basename(p)[:-4] not in ['07162', '104', '102', '107', '217', 'bw', 'em', 'ma']
                     if '_200hz' not in basename(p)[:-4]
                     if '56' != basename(p)[:-4]]

            inx_random = np.arange(len(files))
            np.random.shuffle(inx_random)
            files = np.asarray(files)[inx_random]

            ds_lbl_eval = None
            ds_file_eval = files.copy()

            num_processes, num_shards = cal_num_process_and_num_shard(ds_file_eval, org_num_processes, org_num_shards)

            db_process_info[db[0]]['ds_eval'] = ds_file_eval
            db_process_info[db[0]]['ds_eval_lbl'] = ds_lbl_eval
            db_process_info[db[0]]['processors'] = num_processes
            db_process_info[db[0]]['shards'] = num_shards

        total_shards = np.asarray([db_process_info[k]['shards'] for k in db_process_info]).sum()
        for _, db in enumerate(db_eval):
            res_db_dict, lst_file_to_eval = \
                build_tfrecord(use_gpu_index=use_gpu_index,
                               db_process_info=db_process_info[db[0]],
                               total_shards=total_shards,
                               res_db_dict=res_db_dict,
                               lst_file_to_handle=lst_file_to_eval,
                               ann_ext_all=[db[1] for _ in range(len(db_process_info[db[0]]['ds_eval']))],
                               output_train_directory=None,
                               output_eval_directory=eval_dir,
                               save_image=save_image)

            print('num_eval_samples = {}'.format(res_db_dict['eval']['total_sample']))

    except Exception as e:
        print(e)
        print('SKIPPED: Unexpected error while decoding eval %s.' % db_eval[0])
    # endregion create data eval

    write_log(data_model_dir,
              res_db_dict,
              inspect.stack()[0][3],
              lst_file_to_train,
              lst_file_to_eval)

    fstatus = open(data_model_dir + '/finish.txt', 'w')
    fstatus.writelines(str(datetime.now()))
    fstatus.close()


def _qrs_detection(buf_ecg_qrs, bwr_buf_ecg, check_point_qrs, use_gpu_index):
    # region QRS
    with tf.device('/gpu:{}'.format(use_gpu_index if use_gpu_index >= 0 else 0)):
        qrs = tf.Graph()
        with qrs.as_default():
            str_version = check_point_qrs.split('/')
            for i, v in enumerate(str_version):
                if v == "checkpoint":
                    break

            version_qrs = str_version[i - 1]
            model_name_qrs = str_version[i + 1]
            qrs_inputs = tf.placeholder(tf.float32, shape=(None, FLAGS.beat_qrs_feature_len, 1))
            with tf.variable_scope("{}_{}".format(model_name_qrs, version_qrs)):
                qrs_logits = model.build(
                    name=model_name_qrs,
                    inputs=qrs_inputs,
                    num_of_class=FLAGS.beat_qrs_num_class,
                    is_training=False
                )
                qrs_predictions = tf.argmax(qrs_logits, axis=1)

    index_qrs_sample = np.arange(FLAGS.beat_qrs_feature_len)[None, :] + \
                       np.arange(len(buf_ecg_qrs) - FLAGS.beat_qrs_feature_len)[:, None]

    nor_buf_qrs = buf_ecg_qrs[index_qrs_sample]
    batch_index = 0
    qrs_candidate = []
    samp_len = len(nor_buf_qrs)
    batch_size = BATCH_SIZE
    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True

    if use_gpu_index >= 0:
        config.gpu_options.visible_device_list = str(use_gpu_index)

    with tf.Session(config=config, graph=qrs) as sess:
        sess.run(tf.global_variables_initializer())
        ckpt = tf.train.get_checkpoint_state(check_point_qrs)
        if ckpt and ckpt.model_checkpoint_path:
            restorer = tf.train.Saver()
            restorer.restore(sess, tf.train.latest_checkpoint(check_point_qrs))
        else:
            print('No check_point_qrs file found')
            return

        while batch_index < len(index_qrs_sample):
            try:
                _data = np.reshape(
                    nor_buf_qrs[batch_index: min(batch_index + batch_size, samp_len)],
                    [-1, FLAGS.beat_qrs_feature_len, 1])
                if len(_data) == 0:
                    break

                if len(qrs_candidate) == 0:
                    qrs_candidate = (sess.run(qrs_predictions,
                                              feed_dict={qrs_inputs: _data})).flatten()
                else:
                    qrs_candidate = np.concatenate((qrs_candidate,
                                                    (sess.run(qrs_predictions,
                                                              feed_dict={qrs_inputs: _data})).flatten()))

                batch_index += _data.shape[0]
            except Exception as err:
                print(err)
                return

        # region post-processing NEW
        groups, groups_len = agglomerative_clustering(qrs_candidate, SAMPLING_RATE)
        if len(groups) > 0:
            peaks_pred = select_beat_from_group(groups,
                                                groups_len,
                                                bwr_buf_ecg,
                                                SAMPLING_RATE,
                                                FLAGS.beat_qrs_offset_len,
                                                MIN_RR_INTERVAL)

        else:
            peaks_pred = []
    # endregion
    return peaks_pred


def process_feature_len(start,
                        end,
                        beats,
                        point,
                        buf_ecg,
                        fs,
                        count2save=100,
                        samp_from=0,
                        interp=False,
                        split_input=False,
                        key_save=None,
                        save_file=False,
                        file_name=None,
                        key=None,
                        dir_img_deb=None):
    """

    :param start:
    :param end:
    :param beats:
    :param point:
    :param buf_ecg:
    :param fs:
    :param count2save:
    :param samp_from:
    :param interp:
    :param split_input:
    :param key_save:
    :param save_file:
    :param file_name:
    :param key:
    :param dir_img_deb:
    :return:
    """
    sy = buf_ecg[start: end]
    morph = []
    if not interp:
        ssy = sy.copy()
        if len(sy) > FLAGS.arrhythmia_feature_len:
            a = beats[point] - \
                int(round(((beats[point] - start) * FLAGS.arrhythmia_feature_len) / len(sy)))
            b = beats[point] + \
                int(round((((end - beats[point]) * FLAGS.arrhythmia_feature_len) / len(sy))))
            if a < 0 or b >= len(buf_ecg):
                return morph

            sy = buf_ecg[a: b]

        if len(sy) < FLAGS.arrhythmia_feature_len:
            a = int(round(((beats[point] - start) * FLAGS.arrhythmia_feature_len) / len(sy))) - \
                (beats[point] - start)
            b = int(round(((end - beats[point]) * FLAGS.arrhythmia_feature_len) / len(sy))) - \
                (end - beats[point])

            if start - a < 0 or end + b >= len(buf_ecg):
                return morph

            if split_input:
                sy = np.concatenate((np.full(a, sy[0]), sy, np.full(b, sy[-1])))
            else:
                sy = buf_ecg[start - a: end + b]

        if len(sy) != FLAGS.arrhythmia_feature_len:
            sy = np.concatenate((sy, np.full(abs(FLAGS.arrhythmia_feature_len - len(sy)), sy[-1])))

        if save_file and key == key_save and count2save < 10:
            fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1)
            t = np.arange(len(ssy)) / fs
            ax1.plot(t, ssy)
            t = np.arange(len(sy)) / fs
            ax2.plot(t, sy)
            img_name = basename(file_name) + "_" + key + "_" + str(samp_from + beats[point])
            fig.savefig(dir_img_deb + img_name)
            plt.close(fig)

        morph = sy.copy()
    else:
        sx = np.arange(len(sy), dtype=float) / SAMPLING_RATE
        rx = np.arange(0, len(sy), len(sy) / FLAGS.arrhythmia_feature_len,
                       dtype=float) / SAMPLING_RATE
        if len(rx) > FLAGS.arrhythmia_feature_len:
            rx = rx[:FLAGS.arrhythmia_feature_len]
        elif len(rx) < FLAGS.arrhythmia_feature_len:
            rx = np.concatenate((rx, rx[len(rx) - FLAGS.arrhythmia_feature_len]),
                                axis=0)
        morph = np.interp(rx, sx, sy)

        return morph

    return morph


def _process_sample(use_gpu_index,
                    lock,
                    file_name,
                    ann_ext,
                    ds_type,
                    res_db_dict,
                    lst_file_to_handle,
                    writer,
                    output_directory,
                    save_image):
    lock.acquire()
    try:
        print('file: ', file_name)
    finally:
        lock.release()

    header = wf.rdheader(file_name)

    fs_origin = header.fs
    dict_log = dict()
    dict_log["File"] = basename(dirname(file_name)) + '/' + basename(file_name)
    dir_img_deb = output_directory + 'img/'
    if not os.path.exists(dir_img_deb):
        os.makedirs(dir_img_deb)

    if FLAGS.beat_channel_num < 2:
        samp_to = 0
        samp_from = 0

        if basename(file_name) == '114' or \
                basename(file_name) == 's20151' or \
                basename(file_name) == 's20161' or \
                basename(file_name) == 's20171' or \
                basename(file_name) == 's20211' or \
                basename(file_name) == 's20221' or \
                basename(file_name) == 's20274' or \
                basename(file_name) == 's20311' or \
                basename(file_name) == 's30671' or \
                basename(file_name) == 's30681' or \
                basename(file_name) == 's30691' or \
                basename(file_name) == 's30701' or \
                basename(file_name) == 's30711' or \
                basename(file_name) == 's30731' or \
                basename(file_name) == 's30771' or \
                basename(file_name) == 's30791' or \
                basename(file_name) == 's30801':
            channel_ecg = 1 - FLAGS.beat_channel_num
        elif ann_ext == 'itr':
            print('itr')
            channel_ecg = 1
        else:
            channel_ecg = FLAGS.beat_channel_num

        count2save = 6

        while samp_to <= header.sig_len:
            try:
                if int(NUM_OVERLAP * fs_origin) >= (header.sig_len - samp_from):
                    break

                # region Process
                samp_len = min(int(MAX_SAMPLE_PROCESS * fs_origin), (header.sig_len - samp_from))
                samp_to = samp_from + samp_len
                record = wf.rdsamp(file_name, sampfrom=samp_from, sampto=samp_to, channels=[channel_ecg])
                # Avoid cases where the value is NaN
                buf_record = np.nan_to_num(record[0][:, 0])
                fs_origin = record[1].get('fs')
                ann = wf.rdann(file_name, sampfrom=samp_from, sampto=samp_to, shift_samps=True, extension=ann_ext)

                if fs_origin != SAMPLING_RATE:
                    buf_ecg, ann = resample_singlechan(buf_record,
                                                       ann,
                                                       fs_origin,
                                                       SAMPLING_RATE)
                else:
                    buf_ecg = buf_record.copy()

                buf_ecg = butter_lowpass_filter(buf_ecg, 40, SAMPLING_RATE)
                if FLAGS.beat_bwr == 0:
                    buf_ecg = butter_highpass_filter(buf_ecg, 0.5, SAMPLING_RATE)
                    if FLAGS.beat_norm:
                        buf_ecg = norm(buf_ecg, NUM_NORMALIZATION)
                elif FLAGS.beat_bwr == 1:
                    buf_ecg = bwr(buf_ecg, SAMPLING_RATE)
                    if FLAGS.beat_norm:
                        buf_ecg = norm(buf_ecg, NUM_NORMALIZATION)
                elif FLAGS.beat_bwr == 2:
                    buf_ecg = bwr_dwt(buf_ecg, SAMPLING_RATE)
                    if FLAGS.beat_norm:
                        buf_ecg = norm(buf_ecg, NUM_NORMALIZATION)
                elif FLAGS.beat_bwr == 3:
                    buf_ecg = bwr_dwt2(buf_ecg, SAMPLING_RATE)
                    if FLAGS.beat_norm:
                        buf_ecg = norm(buf_ecg, NUM_NORMALIZATION)

                sample, symbol = beat_annotations(ann)
                sig_len = len(buf_ecg)

                if FLAGS.beat_overlap == 0:
                    sig_len = sig_len // FLAGS.beat_feature_len * FLAGS.beat_feature_len
                    beat_label_len = FLAGS.beat_feature_len // FLAGS.beat_block_len
                    frame_index = np.arange(FLAGS.beat_feature_len)[None, :] + \
                                  np.arange(0, sig_len, FLAGS.beat_feature_len)[:, None]

                    label_index = np.arange(beat_label_len)[None, None, :] + \
                                  np.arange(0, FLAGS.beat_feature_len, beat_label_len)[None, :, None] + \
                                  np.arange(0, sig_len, FLAGS.beat_feature_len)[:, None, None]
                else:
                    sig_len = sig_len - FLAGS.beat_feature_len
                    beat_label_len = FLAGS.beat_feature_len // FLAGS.beat_block_len
                    frame_index = np.arange(FLAGS.beat_feature_len)[None, :] + \
                                  np.arange(0, sig_len, FLAGS.beat_overlap)[:, None]

                    label_index = np.arange(beat_label_len)[None, None, :] + \
                                  np.arange(0, FLAGS.beat_feature_len, beat_label_len)[None, :, None] + \
                                  np.arange(0, sig_len, FLAGS.beat_overlap)[:, None, None]

                check_sample = sample < sig_len
                sample = sample[check_sample]
                symbol = symbol[check_sample]

                lbl_samp = np.full(sig_len, 0, dtype=int)

                ind = {k: i for i, k in enumerate(res_db_dict["beat_class_dict"].keys())}
                beat_offset_num = int(FLAGS.beat_offset_len * SAMPLING_RATE)
                if len(sample) > 1:
                    for key in res_db_dict["beat_class_dict"].keys():
                        syms = res_db_dict["beat_class_dict"][key]
                        lbl_pos = np.where(np.isin(symbol, np.asarray(syms), invert=False))[0]
                        lbl_int = int(ind[key])
                        if lbl_int == 0:
                            continue

                        if len(lbl_pos) > 0:
                            if beat_offset_num > 0:
                                for p in range(len(lbl_pos)):
                                    start_samp = sample[lbl_pos[p]] - beat_offset_num
                                    stop_samp = sample[lbl_pos[p]] + beat_offset_num

                                    if start_samp < 0 or stop_samp >= sig_len:
                                        continue

                                    lbl_samp[start_samp: stop_samp] = np.full(stop_samp - start_samp, lbl_int,
                                                                              dtype=int)
                            else:
                                lbl_samp[sample[lbl_pos]] = lbl_int

                        OTHER = ["|", "/", "Q", "?", "P", "f", "!", "F", "B"]
                        lbl_pos = np.where(np.isin(symbol, np.asarray(OTHER), invert=False))[0]
                        if len(lbl_pos) > 0:
                            lbl_samp[sample[lbl_pos]] = len(res_db_dict["beat_class_dict"].keys()) + 1

                frame_data = buf_ecg[frame_index]
                lbl_samp = lbl_samp[label_index]

                frame_lbl = np.asarray([np.max(lbl, axis=1) for lbl in lbl_samp], dtype=int)
                process_data = []
                process_label = []

                for fr in zip(frame_lbl, frame_data):
                    lbl, frame = fr

                    if max(lbl) == len(res_db_dict["beat_class_dict"].keys()) + 1:
                        continue

                    res_db_dict[ds_type]["total_sample"] += 1

                    for key in res_db_dict["beat_class_dict"].keys():
                        lbl_int = int(ind[key])
                        lbl_pos = np.where(frame_lbl == lbl_int)[0]
                        if len(lbl_pos) > 0:
                            res_db_dict[ds_type][key] += len(lbl_pos)
                            if key not in dict_log.keys():
                                dict_log[key] = len(lbl_pos)
                            else:
                                dict_log[key] += len(lbl_pos)

                    process_data.append(frame)
                    process_label.append(lbl)

                    if save_image and max(lbl) > 1 and count2save > 0:
                        ind_revert = {i: k for i, k in enumerate(res_db_dict["beat_class_dict"].keys())}
                        fig, (ax1, ax2, ax3) = plt.subplots(nrows=3, ncols=1, figsize=(19.20, 10.80))
                        fig.suptitle('dataset: {}; file: {}; event_type: {}'.format(
                            basename(dirname(dirname(file_name))),
                            basename(dirname(file_name)),
                            ind_revert[max(lbl)]), fontsize=11)
                        # region ax1
                        buf_frame = frame[:len(frame) // 3]
                        buf_lbl = lbl[:len(lbl) // 3]
                        t = np.arange(len(buf_frame)) / SAMPLING_RATE
                        ax1.plot(t, buf_frame)
                        plt_lbl = np.zeros(len(buf_frame), dtype=int)
                        index_plt_lbl = np.arange(beat_label_len)[None, :] + \
                                        np.arange(0, len(buf_frame), beat_label_len)[:, None]

                        for ix in range(len(buf_lbl)):
                            try:
                                plt_lbl[index_plt_lbl[ix]] = np.full(beat_label_len, buf_lbl[ix], dtype=int)
                            except:
                                pass

                        group_index = np.where(abs(np.diff(plt_lbl)) > 0)[0] + 1
                        group = np.split(plt_lbl, group_index)
                        tgroup = np.split(t, group_index)
                        for t, g in zip(tgroup, group):
                            if g[0] == 0:
                                ax1.plot(t, g, c='black', label='notbeat')
                            elif g[0] == 1:
                                ax1.plot(t, g, c='g', label='n')
                            elif g[0] == 2:
                                ax1.plot(t, g, c='r', label='v')
                            else:
                                ax1.plot(t, g, c='b', label='s')

                        major_ticks = np.arange(0, len(buf_frame), SAMPLING_RATE) / SAMPLING_RATE
                        minor_ticks = np.arange(0, len(buf_frame), beat_label_len) / SAMPLING_RATE
                        ax1.set_xticks(major_ticks)
                        ax1.set_xticks(minor_ticks, minor=True)
                        ax1.grid(which='major', color='#CCCCCC', linestyle='--')
                        ax1.grid(which='minor', color='#CCCCCC', linestyle=':')
                        # endregion ax1

                        # region ax2
                        buf_frame = frame[len(frame) // 3: (2 * len(frame)) // 3]
                        buf_lbl = lbl[len(lbl) // 3: (2 * len(lbl)) // 3]

                        t = np.arange(len(buf_frame)) / SAMPLING_RATE + 20
                        ax2.plot(t, buf_frame)
                        plt_lbl = np.zeros(len(buf_frame), dtype=int)
                        index_plt_lbl = np.arange(beat_label_len)[None, :] + \
                                        np.arange(0, len(buf_frame), beat_label_len)[:, None]

                        for ix in range(len(buf_lbl)):
                            try:
                                plt_lbl[index_plt_lbl[ix]] = np.full(beat_label_len, buf_lbl[ix], dtype=int)
                            except:
                                pass

                        group_index = np.where(abs(np.diff(plt_lbl)) > 0)[0] + 1
                        group = np.split(plt_lbl, group_index)
                        tgroup = np.split(t, group_index)
                        for t, g in zip(tgroup, group):
                            if g[0] == 0:
                                ax2.plot(t, g, c='black', label='notbeat')
                            elif g[0] == 1:
                                ax2.plot(t, g, c='g', label='n')
                            elif g[0] == 2:
                                ax2.plot(t, g, c='r', label='v')
                            else:
                                ax2.plot(t, g, c='b', label='s')

                        major_ticks = np.arange(0, len(buf_frame), SAMPLING_RATE) / SAMPLING_RATE + 20
                        minor_ticks = np.arange(0, len(buf_frame), beat_label_len) / SAMPLING_RATE + 20
                        ax2.set_xticks(major_ticks)
                        ax2.set_xticks(minor_ticks, minor=True)
                        ax2.grid(which='major', color='#CCCCCC', linestyle='--')
                        ax2.grid(which='minor', color='#CCCCCC', linestyle=':')
                        # endregion ax2

                        # region ax3
                        buf_frame = frame[(2 * len(frame)) // 3:]
                        buf_lbl = lbl[(2 * len(lbl)) // 3:]

                        t = np.arange(len(buf_frame)) / SAMPLING_RATE + 40
                        ax3.plot(t, buf_frame)
                        plt_lbl = np.zeros(len(buf_frame), dtype=int)
                        index_plt_lbl = np.arange(beat_label_len)[None, :] + \
                                        np.arange(0, len(buf_frame), beat_label_len)[:, None]

                        for ix in range(len(buf_lbl)):
                            try:
                                plt_lbl[index_plt_lbl[ix]] = np.full(beat_label_len, buf_lbl[ix], dtype=int)
                            except:
                                pass

                        group_index = np.where(abs(np.diff(plt_lbl)) > 0)[0] + 1
                        group = np.split(plt_lbl, group_index)
                        tgroup = np.split(t, group_index)
                        for t, g in zip(tgroup, group):
                            if g[0] == 0:
                                ax3.plot(t, g, c='black', label='notbeat')
                            elif g[0] == 1:
                                ax3.plot(t, g, c='g', label='n')
                            elif g[0] == 2:
                                ax3.plot(t, g, c='r', label='v')
                            else:
                                ax3.plot(t, g, c='b', label='s')

                        major_ticks = np.arange(0, len(buf_frame), SAMPLING_RATE) / SAMPLING_RATE + 40
                        minor_ticks = np.arange(0, len(buf_frame), beat_label_len) / SAMPLING_RATE + 40
                        ax3.set_xticks(major_ticks)
                        ax3.set_xticks(minor_ticks, minor=True)
                        ax3.grid(which='major', color='#CCCCCC', linestyle='--')
                        ax3.grid(which='minor', color='#CCCCCC', linestyle=':')
                        # endregion ax3

                        DEBUG_IMG = False
                        if not DEBUG_IMG:
                            img_name = basename(file_name) + "_" + \
                                       str(max(lbl)) + "_" + \
                                       str(channel_ecg) + "_" + \
                                       str(samp_from) + "_" + \
                                       str(count2save)
                            fig.savefig(dir_img_deb + img_name, format='svg', dpi=1200)
                            plt.close(fig)
                        else:
                            plt.show()

                        count2save -= 1

                process_data = np.asarray(process_data)
                process_label = np.asarray(process_label, dtype=int)
                if len(process_data) > 0:
                    # uniform_random = np.arange(len(process_data))
                    # np.random.shuffle(uniform_random)
                    # process_data = process_data[uniform_random]
                    # process_label = process_label[uniform_random]
                    np_to_tfrecords(sample_buffer=process_data,
                                    label_buffer=process_label,
                                    writer=writer)

                if samp_from < (samp_to - int(NUM_OVERLAP * fs_origin)):
                    samp_to -= int(NUM_OVERLAP * fs_origin)

                samp_from = samp_to

            except Exception as e:
                print("File {}: {}".format(file_name, e))
                break
    else:
        for channel_ecg in range(header.n_sig):
            samp_to = 0
            samp_from = 0

            if basename(file_name) == '114' or \
                    basename(file_name) == 's20151' or \
                    basename(file_name) == 's20161' or \
                    basename(file_name) == 's20171' or \
                    basename(file_name) == 's20211' or \
                    basename(file_name) == 's20221' or \
                    basename(file_name) == 's20274' or \
                    basename(file_name) == 's20311' or \
                    basename(file_name) == 's30671' or \
                    basename(file_name) == 's30681' or \
                    basename(file_name) == 's30691' or \
                    basename(file_name) == 's30701' or \
                    basename(file_name) == 's30711' or \
                    basename(file_name) == 's30731' or \
                    basename(file_name) == 's30771' or \
                    basename(file_name) == 's30791' or \
                    basename(file_name) == 's30801':
                channel_ecg = 1 - FLAGS.beat_channel_num
            else:
                channel_ecg = FLAGS.beat_channel_num

            count2save = 5

            while samp_to <= header.sig_len:
                try:
                    if int(NUM_OVERLAP * fs_origin) >= (header.sig_len - samp_from):
                        break

                    # region Process
                    samp_len = min(int(MAX_SAMPLE_PROCESS * fs_origin), (header.sig_len - samp_from))
                    samp_to = samp_from + samp_len
                    record = wf.rdsamp(file_name, sampfrom=samp_from, sampto=samp_to, channels=[channel_ecg])
                    # Avoid cases where the value is NaN
                    buf_record = np.nan_to_num(record[0][:, 0])
                    fs_origin = record[1].get('fs')
                    ann = wf.rdann(file_name, sampfrom=samp_from, sampto=samp_to, shift_samps=True, extension=ann_ext)

                    if fs_origin != SAMPLING_RATE:
                        buf_ecg, ann = resample_singlechan(buf_record,
                                                           ann,
                                                           fs_origin,
                                                           SAMPLING_RATE)
                    else:
                        buf_ecg = buf_record.copy()

                    buf_ecg = butter_lowpass_filter(buf_ecg, 40, SAMPLING_RATE)

                    if FLAGS.beat_bwr == 0:
                        buf_ecg = butter_highpass_filter(buf_ecg, 0.5, SAMPLING_RATE)
                        if FLAGS.beat_norm:
                            buf_ecg = norm(buf_ecg, NUM_NORMALIZATION)
                    elif FLAGS.beat_bwr == 1:
                        buf_ecg = bwr(buf_ecg, SAMPLING_RATE)
                        if FLAGS.beat_norm:
                            buf_ecg = norm(buf_ecg, NUM_NORMALIZATION)
                    elif FLAGS.beat_bwr == 2:
                        buf_ecg = bwr_dwt(buf_ecg, SAMPLING_RATE)
                        if FLAGS.beat_norm:
                            buf_ecg = norm(buf_ecg, NUM_NORMALIZATION)
                    elif FLAGS.beat_bwr == 3:
                        buf_ecg = bwr_dwt2(buf_ecg, SAMPLING_RATE)
                        if FLAGS.beat_norm:
                            buf_ecg = norm(buf_ecg, NUM_NORMALIZATION)

                    sample, symbol = beat_annotations(ann)
                    sig_len = len(buf_ecg)

                    if FLAGS.beat_overlap == 0:
                        sig_len = sig_len // FLAGS.beat_feature_len * FLAGS.beat_feature_len
                        beat_label_len = FLAGS.beat_feature_len // FLAGS.beat_block_len
                        frame_index = np.arange(FLAGS.beat_feature_len)[None, :] + \
                                      np.arange(0, sig_len, FLAGS.beat_feature_len)[:, None]

                        label_index = np.arange(beat_label_len)[None, None, :] + \
                                      np.arange(0, FLAGS.beat_feature_len, beat_label_len)[None, :, None] + \
                                      np.arange(0, sig_len, FLAGS.beat_feature_len)[:, None, None]
                    else:
                        sig_len = sig_len - FLAGS.beat_feature_len
                        beat_label_len = FLAGS.beat_feature_len // FLAGS.beat_block_len
                        frame_index = np.arange(FLAGS.beat_feature_len)[None, :] + \
                                      np.arange(0, sig_len, FLAGS.beat_overlap)[:, None]

                        label_index = np.arange(beat_label_len)[None, None, :] + \
                                      np.arange(0, FLAGS.beat_feature_len, beat_label_len)[None, :, None] + \
                                      np.arange(0, sig_len, FLAGS.beat_overlap)[:, None, None]

                    check_sample = sample < sig_len
                    sample = sample[check_sample]
                    symbol = symbol[check_sample]

                    lbl_samp = np.full(sig_len, 0, dtype=int)

                    ind = {k: i for i, k in enumerate(res_db_dict["beat_class_dict"].keys())}
                    beat_offset_num = int(FLAGS.beat_offset_len * SAMPLING_RATE)
                    if len(sample) > 1:
                        for key in res_db_dict["beat_class_dict"].keys():
                            syms = res_db_dict["beat_class_dict"][key]
                            lbl_pos = np.where(np.isin(symbol, np.asarray(syms), invert=False))[0]
                            lbl_int = int(ind[key])
                            if lbl_int == 0:
                                continue

                            if len(lbl_pos) > 0:
                                if beat_offset_num > 0:
                                    for p in range(len(lbl_pos)):
                                        start_samp = sample[lbl_pos[p]] - beat_offset_num
                                        stop_samp = sample[lbl_pos[p]] + beat_offset_num

                                        if start_samp < 0 or stop_samp >= sig_len:
                                            continue

                                        lbl_samp[start_samp: stop_samp] = np.full(stop_samp - start_samp, lbl_int,
                                                                                  dtype=int)
                                else:
                                    lbl_samp[sample[lbl_pos]] = lbl_int

                            OTHER = ["|", "/", "Q", "?", "P", "f", "!", "F", "B"]
                            lbl_pos = np.where(np.isin(symbol, np.asarray(OTHER), invert=False))[0]
                            if len(lbl_pos) > 0:
                                lbl_samp[sample[lbl_pos]] = len(res_db_dict["beat_class_dict"].keys()) + 1

                    frame_data = buf_ecg[frame_index]
                    lbl_samp = lbl_samp[label_index]

                    frame_lbl = np.asarray([np.max(lbl, axis=1) for lbl in lbl_samp], dtype=int)
                    process_data = []
                    process_label = []

                    for fr in zip(frame_lbl, frame_data):
                        lbl, frame = fr

                        if max(lbl) == len(res_db_dict["beat_class_dict"].keys()) + 1:
                            continue

                        res_db_dict[ds_type]["total_sample"] += 1

                        for key in res_db_dict["beat_class_dict"].keys():
                            lbl_int = int(ind[key])
                            lbl_pos = np.where(frame_lbl == lbl_int)[0]
                            if len(lbl_pos) > 0:
                                res_db_dict[ds_type][key] += len(lbl_pos)
                                if key not in dict_log.keys():
                                    dict_log[key] = len(lbl_pos)
                                else:
                                    dict_log[key] += len(lbl_pos)

                        process_data.append(frame)
                        process_label.append(lbl)

                        if save_image and max(lbl) > 1 and count2save > 0:
                            ind_revert = {i: k for i, k in enumerate(res_db_dict["beat_class_dict"].keys())}
                            fig, (ax1, ax2, ax3) = plt.subplots(nrows=3, ncols=1, figsize=(19.20, 10.80))
                            fig.suptitle('dataset: {}; file: {}; event_type: {}'.format(
                                basename(dirname(dirname(file_name))),
                                basename(dirname(file_name)),
                                ind_revert[max(lbl)]), fontsize=11)
                            # region ax1
                            buf_frame = frame[:len(frame) // 3]
                            buf_lbl = lbl[:len(lbl) // 3]
                            t = np.arange(len(buf_frame)) / SAMPLING_RATE
                            ax1.plot(t, buf_frame)
                            plt_lbl = np.zeros(len(buf_frame), dtype=int)
                            index_plt_lbl = np.arange(beat_label_len)[None, :] + \
                                            np.arange(0, len(buf_frame), beat_label_len)[:, None]

                            for ix in range(len(buf_lbl)):
                                try:
                                    plt_lbl[index_plt_lbl[ix]] = np.full(beat_label_len, buf_lbl[ix], dtype=int)
                                except:
                                    pass

                            group_index = np.where(abs(np.diff(plt_lbl)) > 0)[0] + 1
                            group = np.split(plt_lbl, group_index)
                            tgroup = np.split(t, group_index)
                            for t, g in zip(tgroup, group):
                                if g[0] == 0:
                                    ax1.plot(t, g, c='black', label='notbeat')
                                elif g[0] == 1:
                                    ax1.plot(t, g, c='g', label='n')
                                elif g[0] == 2:
                                    ax1.plot(t, g, c='r', label='v')
                                else:
                                    ax1.plot(t, g, c='b', label='s')

                            major_ticks = np.arange(0, len(buf_frame), SAMPLING_RATE) / SAMPLING_RATE
                            minor_ticks = np.arange(0, len(buf_frame), beat_label_len) / SAMPLING_RATE
                            ax1.set_xticks(major_ticks)
                            ax1.set_xticks(minor_ticks, minor=True)
                            ax1.grid(which='major', color='#CCCCCC', linestyle='--')
                            ax1.grid(which='minor', color='#CCCCCC', linestyle=':')
                            # endregion ax1

                            # region ax2
                            buf_frame = frame[len(frame) // 3: (2 * len(frame)) // 3]
                            buf_lbl = lbl[len(lbl) // 3: (2 * len(lbl)) // 3]

                            t = np.arange(len(buf_frame)) / SAMPLING_RATE + 20
                            ax2.plot(t, buf_frame)
                            plt_lbl = np.zeros(len(buf_frame), dtype=int)
                            index_plt_lbl = np.arange(beat_label_len)[None, :] + \
                                            np.arange(0, len(buf_frame), beat_label_len)[:, None]

                            for ix in range(len(buf_lbl)):
                                try:
                                    plt_lbl[index_plt_lbl[ix]] = np.full(beat_label_len, buf_lbl[ix], dtype=int)
                                except:
                                    pass

                            group_index = np.where(abs(np.diff(plt_lbl)) > 0)[0] + 1
                            group = np.split(plt_lbl, group_index)
                            tgroup = np.split(t, group_index)
                            for t, g in zip(tgroup, group):
                                if g[0] == 0:
                                    ax2.plot(t, g, c='black', label='notbeat')
                                elif g[0] == 1:
                                    ax2.plot(t, g, c='g', label='n')
                                elif g[0] == 2:
                                    ax2.plot(t, g, c='r', label='v')
                                else:
                                    ax2.plot(t, g, c='b', label='s')

                            major_ticks = np.arange(0, len(buf_frame), SAMPLING_RATE) / SAMPLING_RATE + 20
                            minor_ticks = np.arange(0, len(buf_frame), beat_label_len) / SAMPLING_RATE + 20
                            ax2.set_xticks(major_ticks)
                            ax2.set_xticks(minor_ticks, minor=True)
                            ax2.grid(which='major', color='#CCCCCC', linestyle='--')
                            ax2.grid(which='minor', color='#CCCCCC', linestyle=':')
                            # endregion ax2

                            # region ax3
                            buf_frame = frame[(2 * len(frame)) // 3:]
                            buf_lbl = lbl[(2 * len(lbl)) // 3:]

                            t = np.arange(len(buf_frame)) / SAMPLING_RATE + 40
                            ax3.plot(t, buf_frame)
                            plt_lbl = np.zeros(len(buf_frame), dtype=int)
                            index_plt_lbl = np.arange(beat_label_len)[None, :] + \
                                            np.arange(0, len(buf_frame), beat_label_len)[:, None]

                            for ix in range(len(buf_lbl)):
                                try:
                                    plt_lbl[index_plt_lbl[ix]] = np.full(beat_label_len, buf_lbl[ix], dtype=int)
                                except:
                                    pass

                            group_index = np.where(abs(np.diff(plt_lbl)) > 0)[0] + 1
                            group = np.split(plt_lbl, group_index)
                            tgroup = np.split(t, group_index)
                            for t, g in zip(tgroup, group):
                                if g[0] == 0:
                                    ax3.plot(t, g, c='black', label='notbeat')
                                elif g[0] == 1:
                                    ax3.plot(t, g, c='g', label='n')
                                elif g[0] == 2:
                                    ax3.plot(t, g, c='r', label='v')
                                else:
                                    ax3.plot(t, g, c='b', label='s')

                            major_ticks = np.arange(0, len(buf_frame), SAMPLING_RATE) / SAMPLING_RATE + 40
                            minor_ticks = np.arange(0, len(buf_frame), beat_label_len) / SAMPLING_RATE + 40
                            ax3.set_xticks(major_ticks)
                            ax3.set_xticks(minor_ticks, minor=True)
                            ax3.grid(which='major', color='#CCCCCC', linestyle='--')
                            ax3.grid(which='minor', color='#CCCCCC', linestyle=':')
                            # endregion ax3

                            DEBUG_IMG = False
                            if not DEBUG_IMG:
                                img_name = basename(file_name) + "_" + \
                                           str(max(lbl)) + "_" + \
                                           str(channel_ecg) + "_" + \
                                           str(samp_from) + "_" + \
                                           str(count2save)
                                fig.savefig(dir_img_deb + img_name, format='svg', dpi=1200)
                                plt.close(fig)
                            else:
                                plt.show()

                            count2save -= 1

                    process_data = np.asarray(process_data)
                    process_label = np.asarray(process_label, dtype=int)
                    if len(process_data) > 0:
                        # uniform_random = np.arange(len(process_data))
                        # np.random.shuffle(uniform_random)
                        # process_data = process_data[uniform_random]
                        # process_label = process_label[uniform_random]

                        np_to_tfrecords(sample_buffer=process_data,
                                        label_buffer=process_label,
                                        writer=writer)

                    if samp_from < (samp_to - int(NUM_OVERLAP * fs_origin)):
                        samp_to -= int(NUM_OVERLAP * fs_origin)

                    samp_from = samp_to

                except Exception as e:
                    print("File {}: {}".format(file_name, e))
                    break

            if ds_type != "train":
                break

    lst_file_to_handle.append(dict_log)
    return res_db_dict, lst_file_to_handle


def _process_files_batch(use_gpu_index,
                         queue,
                         lock,
                         process_index,
                         ranges,
                         file_names,
                         labels,
                         output_train_directory,
                         output_eval_directory,
                         num_shards,
                         total_shards,
                         ann_ext_all,
                         save_image):
    """

     :param process_index:
     :param ranges:
     :param file_names:
     :param output_train_directory:
     :param output_eval_directory:
     :param num_shards:
     :param ann_ext_all:
     :return:
     """
    global process_res_db_dict
    global process_lst_file_to_handle

    # Each process produces N shards where N = num_shards.
    # For instance, if num_shards = 128, and the num_processes = 2, then the first
    # Each processor would produce shards [0, 128).
    num_processes = len(ranges)
    assert not num_shards % num_processes
    num_shards_per_batch = int(num_shards / num_processes)

    shard_ranges = np.linspace(ranges[process_index][0],
                               ranges[process_index][1],
                               num_shards_per_batch + 1).astype(int)
    num_files_in_thread = ranges[process_index][1] - ranges[process_index][0]

    # Initial parameter for each process
    res_db_dict = process_res_db_dict[process_index]
    lst_file_to_handle = process_lst_file_to_handle[process_index]

    counter = 0
    ds_type = "train" if output_train_directory is not None else "eval"
    for s in range(num_shards_per_batch):
        # Generate a sharded version of the file name, e.g. 'train-00002-of-00010'
        shard = process_index * num_shards_per_batch + s
        output_filename = '%s_%.5d-of-%.5d.tfrecord' % (ds_type,
                                                        res_db_dict['previous_shards'][ds_type] + shard + 1,
                                                        total_shards)
        if 'train' in ds_type:
            output_file = os.path.join(output_train_directory, output_filename)
        else:
            output_file = os.path.join(output_eval_directory, output_filename)
        shard_counter = 0
        files_in_shard = np.arange(shard_ranges[s], shard_ranges[s + 1], dtype=int)
        writer = tf.io.TFRecordWriter(output_file)

        for i in files_in_shard:
            file_name = file_names[i]
            ann_ext = ann_ext_all[i]
            try:
                output_directory = output_train_directory if output_train_directory is not None else output_eval_directory
                res_db_dict, lst_file_to_handle = _process_sample(use_gpu_index=use_gpu_index,
                                                                  lock=lock,
                                                                  file_name=file_name,
                                                                  ann_ext=ann_ext,
                                                                  ds_type=ds_type,
                                                                  res_db_dict=res_db_dict,
                                                                  lst_file_to_handle=lst_file_to_handle,
                                                                  writer=writer,
                                                                  output_directory=output_directory,
                                                                  save_image=save_image)

                process_res_db_dict[process_index] = res_db_dict
                process_lst_file_to_handle[process_index] = lst_file_to_handle
            except Exception as e:
                lock.acquire()
                try:
                    print(e)
                    print('SKIPPED: Unexpected error while decoding %s.' % file_name)
                finally:
                    lock.release()
                continue

            shard_counter += 1
            counter += 1

            if not counter % 1000:
                lock.acquire()
                try:
                    print('%s [processor %d]: Processed %d of %d files in processing batch.' %
                          (datetime.now(), process_index, counter, num_files_in_thread))
                    sys.stdout.flush()
                finally:
                    lock.release()

        writer.close()

        lock.acquire()
        try:
            print('%s [processor %d]: Wrote %d files to %s' %
                  (datetime.now(), process_index, shard_counter, output_file))

            sys.stdout.flush()
        finally:
            lock.release()

        shard_counter = 0

    queue.put({'process_lst_file_to_handle': process_lst_file_to_handle[process_index],
               'process_res_db_dict': process_res_db_dict[process_index]})
    lock.acquire()
    try:
        print('%s [processor %d]: Wrote %d files to %d shards.' %
              (datetime.now(), process_index, counter, num_files_in_thread))
        sys.stdout.flush()
    finally:
        lock.release()

    queue.task_done()


def initiate_process_parameters(res_db_dict, lbl_train, ranges):
    return_dict = dict()
    return_dict['process_res_db_dict'] = [0] * len(ranges)
    return_dict['process_lst_file_to_handle'] = [0] * len(ranges)

    for i in range(len(ranges)):
        return_dict['process_res_db_dict'][i] = copy.deepcopy(res_db_dict)
        return_dict['process_res_db_dict'][i]["eval"]["total_sample"] = 0
        return_dict['process_res_db_dict'][i]["train"]["total_sample"] = 0
        for key in lbl_train.keys():
            return_dict['process_res_db_dict'][i]["eval"][key] = 0
            return_dict['process_res_db_dict'][i]["train"][key] = 0

        return_dict['process_lst_file_to_handle'][i] = list()

    return return_dict


def build_tfrecord(use_gpu_index,
                   db_process_info,
                   total_shards,
                   res_db_dict,
                   lst_file_to_handle,
                   ann_ext_all,
                   output_train_directory,
                   output_eval_directory,
                   save_image):
    """

    :param use_gpu_index:
    :param db_process_info:
    :param total_shards:
    :param res_db_dict:
    :param lst_file_to_handle:
    :param ann_ext_all:
    :param output_train_directory:
    :param output_eval_directory:
    :param save_image:
    :return:
    """
    global process_res_db_dict
    global process_lst_file_to_handle

    all_file = db_process_info['ds_train'] if output_train_directory is not None else db_process_info['ds_eval']
    all_lbl = db_process_info['ds_train_lbl'] if output_train_directory is not None else db_process_info['ds_eval_lbl']
    num_processes = db_process_info['processors']
    num_shards = db_process_info['shards']

    if output_train_directory is not None:
        print("Step: Create data train")
    else:
        print("Step: Create data eval")

    # Break all images into batches with a [ranges[i][0], ranges[i][1]].
    spacing = np.linspace(0, len(all_file), num_processes + 1).astype(np.int)
    ranges = []
    for i in range(len(spacing) - 1):
        ranges.append([spacing[i], spacing[i + 1]])

    # Launch a processor for each batch.
    print('Launching %d processors for spacings: %s' % (num_processes, ranges))
    sys.stdout.flush()
    # Create a mechanism for monitoring when all processors are finished.
    coord = tf.train.Coordinator()

    # Initiate parameter for each process
    process_data_dict = initiate_process_parameters(res_db_dict, LABEL_ARRHYTHMIA[FLAGS.beat_class], ranges)

    process_res_db_dict = process_data_dict['process_res_db_dict']
    process_lst_file_to_handle = process_data_dict['process_lst_file_to_handle']

    num_shards = min(num_shards, len(all_file))
    processors = list()
    process_queue = [list() for i in range(num_processes)]
    process_lock = [list() for i in range(num_processes)]

    for process_index in range(len(ranges)):
        process_queue[process_index] = JoinableQueue()
        process_lock[process_index] = Lock()
        args = (use_gpu_index,
                process_queue[process_index],
                process_lock[process_index],
                process_index,
                ranges,
                all_file,
                all_lbl,
                output_train_directory,
                output_eval_directory,
                num_shards,
                total_shards,
                ann_ext_all,
                save_image)
        t = Process(target=_process_files_batch, args=args)
        t.start()
        processors.append(t)

    # Get output of processes
    for process_index in range(len(ranges)):
        process_returned_data = process_queue[process_index].get()
        process_lst_file_to_handle[process_index] = process_returned_data['process_lst_file_to_handle']
        process_res_db_dict[process_index] = process_returned_data['process_res_db_dict']
        processors[process_index].terminate()

    # Wait for all the processors to terminate.
    coord.join(processors)

    # Concatenate processes returned output !!!
    ds_type = "train" if output_train_directory is not None else "eval"
    lst_file_to_handle += sum(process_lst_file_to_handle, [])
    res_db_dict['previous_shards'][ds_type] += num_shards

    res_db_dict[ds_type]["total_sample"] += np.asarray([t[ds_type]["total_sample"] for t in process_res_db_dict]).sum()
    for key in LABEL_ARRHYTHMIA[FLAGS.beat_class].keys():
        res_db_dict[ds_type][key] += np.asarray([t[ds_type][key] for t in process_res_db_dict]).sum()

    print('%s: Finished writing all %d files in data set.' %
          (datetime.now(), len(all_file)))
    sys.stdout.flush()

    # return process_data_ds, process_lbl_ds
    return res_db_dict, lst_file_to_handle


def write_log(log_path, res_db_dict, func_name=None, lst_file_train=None,
              lst_file_to_eval=None):
    log_file = open(log_path + 'num_samples.txt', 'w')
    log_training = dict()
    log_training["train"] = dict()
    log_training["eval"] = dict()
    log_training["train"]["total_sample"] = int(res_db_dict["train"]["total_sample"])
    log_training["eval"]["total_sample"] = int(res_db_dict["eval"]["total_sample"])
    for key in LABEL_ARRHYTHMIA[FLAGS.beat_class].keys():
        log_training["train"][key] = int(res_db_dict["train"][key])
        log_training["eval"][key] = int(res_db_dict["eval"][key])

    log_training["func_create_data"] = os.path.basename(inspect.getfile(inspect.currentframe()))[:-3] + "." + func_name

    log_training["beat_block_len"] = res_db_dict["beat_block_len"]
    log_training["beat_segment_len"] = res_db_dict["beat_segment_len"]
    log_training["beat_overlap"] = res_db_dict["beat_overlap"]
    log_training["beat_channel_num"] = res_db_dict["beat_channel_num"]
    log_training["beat_offset_len"] = res_db_dict["beat_offset_len"]
    log_training["beat_feature_len"] = res_db_dict["beat_feature_len"]
    log_training["beat_bwr"] = res_db_dict["beat_bwr"]
    log_training["beat_norm"] = res_db_dict["beat_norm"]
    log_training["beat_class"] = res_db_dict["beat_class"]
    log_training["beat_class_num"] = res_db_dict["beat_class_num"]
    log_training["beat_class_dict"] = res_db_dict["beat_class_dict"]
    log_training["beat_db_train"] = res_db_dict["beat_db_train"]
    log_training["beat_db_eval"] = res_db_dict["beat_db_eval"]

    json.dump(log_training, log_file)
    log_file.close()
    if lst_file_to_eval is not None:
        ds1_file = open(log_path + 'ds1.csv', 'w')
        fieldnames = ['File']
        for key in LABEL_ARRHYTHMIA[FLAGS.beat_class].keys():
            fieldnames.append(key)

        ds1_writer = csv.DictWriter(ds1_file, fieldnames=fieldnames)
        ds1_writer.writeheader()
        for item in lst_file_train:
            ds1_writer.writerow(item)

        ds1_file.close()

        ds2_file = open(log_path + 'ds2.csv', 'w')
        ds2_writer = csv.DictWriter(ds2_file, fieldnames=fieldnames)
        ds2_writer.writeheader()
        for item in lst_file_to_eval:
            ds2_writer.writerow(item)

        ds2_file.close()

    else:
        ds1_file = open(log_path + 'ds.csv', 'w')
        fieldnames = ['File']
        for key in LABEL_ARRHYTHMIA[FLAGS.beat_class].keys():
            fieldnames.append(key)

        ds1_writer = csv.DictWriter(ds1_file, fieldnames=fieldnames)
        ds1_writer.writeheader()
        for item in lst_file_train:
            ds1_writer.writerow(item)

        ds1_file.close()


def main(argv=None):
    MEDIA_PATH = '/home/dattran/MegaDataset/beat_classification/debug'
    tf.app.flags.DEFINE_integer('beat_block_len', 480, '')
    tf.app.flags.DEFINE_integer('beat_segment_len', 60, '')
    tf.app.flags.DEFINE_integer('beat_overlap', 0, '')
    tf.app.flags.DEFINE_integer('beat_feature_len', 15360, '')
    tf.app.flags.DEFINE_integer('beat_channel_num', 2, '')
    tf.app.flags.DEFINE_float('beat_offset_len', 0.075, 'ms')
    tf.app.flags.DEFINE_boolean('beat_bwr', True, '')
    tf.app.flags.DEFINE_boolean('beat_norm', True, '')
    tf.app.flags.DEFINE_string('beat_class', "0", '')
    tf.app.flags.DEFINE_integer('beat_class_num', 2, '')

    model_split_input = ['0.0.0.11']

    model_len_input = ['480.60.0.0']

    for s in model_split_input:
        for l in model_len_input:
            data_model_dir = 'data.{}.{}'.format(l, s)
            print("\n>>>>>>>>>>> {} <<<<<<<<<<<\n".format(data_model_dir))
            version = 'beatv63subject.{}.{}.{}'.format(l, s, 0)
            DATA_MODEL_DIR = MEDIA_PATH + '/{}/'.format(data_model_dir)
            if not os.path.exists('{}/num_samples.txt'.format(DATA_MODEL_DIR)):
                TRAIN_DIR = '{}/train/'.format(DATA_MODEL_DIR)
                EVAL_DIR = '{}/eval/'.format(DATA_MODEL_DIR)
                FLAGS.beat_block_len = int(version.split('.')[1])
                FLAGS.beat_segment_len = int(version.split('.')[2])
                FLAGS.beat_overlap = float(version.split('.')[3]) / 100
                FLAGS.beat_channel_num = int(version.split('.')[4])
                FLAGS.beat_offset_len = float(version.split('.')[5]) / 1000
                FLAGS.beat_feature_len = int(version.split('.')[2]) * SAMPLING_RATE
                FLAGS.beat_bwr = int(version.split('.')[6])
                FLAGS.beat_norm = bool(int(version.split('.')[7]))
                FLAGS.beat_class = version.split('.')[8]

                FLAGS.beat_class_num = len(LABEL_ARRHYTHMIA[FLAGS.beat_class].keys())

                DB_TRAINING = [["ltdb", "atr", "atr"],
                               ["stdb", "atr", "atr"],
                               ["ltstdb", "atr", "atr"],
                               ["svdb", "atr", "atr"],
                               ["chfdb", "ecg", "ecg"]]

                # DB_TRAINING = [["svdb", "atr", "atr"]]

                DB_TESTING = [["ahadb", "atr", "atr"],
                              ["escdb", "atr", "atr"],
                              ["mitdb", "atr", "atr"]]

                # DB_TESTING = [["mitdb", "atr", "atr"]]

                create_dataset_subject_based_tfrecord(use_gpu_index=0,
                                                      data_model_dir=DATA_MODEL_DIR,
                                                      train_dir=TRAIN_DIR,
                                                      eval_dir=EVAL_DIR,
                                                      data_dir='/home/dattran/PhysionetData/{}',
                                                      db_train=DB_TRAINING,
                                                      db_eval=DB_TESTING,
                                                      save_image=True,
                                                      org_num_processes=1,
                                                      org_num_shards=1)


if __name__ == '__main__':
    tf.logging.set_verbosity(tf.logging.ERROR)
    tf.compat.v1.app.run()
