import numpy as np
from utils.reprocessing import smooth, iir_bandpass


def p_t_wave_detection(buf_ecg, peak, sampling_rate):
    """
    Find QRS_onset, QRS_offset, T_peak, T_offset, P_pesado_onset
    :param buf_ecg: signal ecg affer pass bandpass filter 0-40Hz
    :param peak: R location
    :param sampling_rate:
    :return: array qrs_onset, qrs_offset, t_peak, t_offset, p_onset, qt, qtc, st_slope, st_level, hr
    """
    dur_window1 = int(0.070 * sampling_rate)
    dur_window2 = int(0.14 * sampling_rate)
    # maximum dynamic interval between the T wave and the current R peak
    rt_dynamic_interval_max = 0.583
    t_offset_dynamic_interval_max = 0.07
    tp_isoelectric_segment = 0.05
    num_onset_area_candidate = int(0.06 * sampling_rate)
    num_offset_area_candidate = int(0.1 * sampling_rate)

    """ Find QRS onset and offset """
    # region QRS on, offset
    if len(peak) > 0:
        x_r = np.concatenate((peak, [min(peak[-1] + int(0.2 * sampling_rate), len(buf_ecg) - 1)]))
        x_l = np.concatenate(([max(0, peak[0] - int(0.2 * sampling_rate))], peak))
        rr_range = [np.arange(start, stop, dtype=int) for start, stop in zip(x_l + 1, x_r)]
        y_r = buf_ecg[x_r]
        y_l = buf_ecg[x_l]
        y = y_r - y_l
        x = x_r - x_l
        xy = np.multiply(x_r, y_l) - np.multiply(y_r, x_l)

        wd = [
            np.multiply(
                np.divide(
                    np.abs(y[xi] * rr_range[xi] -
                           x[xi] * buf_ecg[rr_range[xi]] +
                           np.repeat(xy[xi], len(rr_range[xi]))),
                    np.repeat(np.sqrt(np.power(y[xi], 2) + np.power(x[xi], 2)), len(rr_range[xi]))),
                np.cos(2 * np.pi * np.divide((rr_range[xi] - x_l[xi]), x[xi])))
            for xi in range(len(rr_range))]

        qs_point = np.asarray([(x_l[pi] + len(wd[pi]) // 2 + np.argmax(wd[pi][len(wd[pi]) // 2:]),
                                x_l[pi] + np.argmax(wd[pi][:len(wd[pi]) // 2])) for pi in range(len(wd))], dtype=int)

        # Junctions of Q wave and S wave respectively
        qi = qs_point[:-1, 0]
        si = qs_point[1:, 1]
        # QRS Onset And Offset Detection
        # qrs_onset Detection of XR beats after qrs_q detection
        k = 4
        onset_area = np.asarray([np.arange(stop - num_onset_area_candidate, stop, dtype=int) for stop in qi])
        onset_y = buf_ecg[onset_area]
        offset_area = np.asarray([np.arange(start, start + num_offset_area_candidate, dtype=int) for start in si])
        offset_y = buf_ecg[offset_area]
        theta_onset = np.asarray([(np.abs(np.pi - np.arctan((onset_y[:, i] - onset_y[:, i + k]) / (k / sampling_rate))) -
                                   np.abs(np.arctan((onset_y[:, i + k] - onset_y[:, i + 2*k]) / (k / sampling_rate)))) *
                                  180 / np.pi
                                  for i in range(num_onset_area_candidate - k * 2)])

        theta_offset = np.asarray([(np.abs(np.pi - np.arctan((offset_y[:, i] - offset_y[:, i + k]) / (k / sampling_rate))) -
                                    np.abs(np.arctan((offset_y[:, i + k] - offset_y[:, i + 2 * k]) / (k / sampling_rate)))) *
                                   180 / np.pi
                                   for i in range(num_offset_area_candidate - k * 2)])

        qrs_onset = (qi - num_onset_area_candidate) + np.argmax(theta_onset, axis=0)
        qrs_offset = si + np.argmax(theta_offset, axis=0)

        qrs_onset[qrs_onset == 0] = peak[qrs_onset == 0]
        qrs_onset[qrs_offset == 0] = peak[qrs_offset == 0]

        """ Find T wave and T offset """
        # region find T wave and T offset
        buf_ecg_10hz = buf_ecg.copy()
        buf_ecg_10hz = iir_bandpass(buf_ecg_10hz, 1.0, 10, sampling_rate)
        # Generating Blocks of Interest
        ma_ppeak = smooth(buf_ecg_10hz, dur_window1)
        ma_pthr = smooth(buf_ecg_10hz, dur_window2)
        rr_sample = np.diff(np.concatenate((peak, [min(peak[-1] + int(0.2 * sampling_rate), len(buf_ecg) - 1)])))
        rr_sample[rr_sample > 500] = 500  # 30 pbm = 2s = 500 sample
        r_t_min = qrs_offset    # (rr_sample * 0.111).astype(int)
        r_t_max = peak + (rr_sample * rt_dynamic_interval_max).astype(int)
        block_t_wave = [np.asarray(ma_ppeak[np.arange(start, min(stop, len(buf_ecg_10hz)), dtype=int)] >
                                   ma_pthr[np.arange(start, min(stop, len(buf_ecg_10hz)), dtype=int)], dtype=int)
                        for start, stop in zip(r_t_min, r_t_max)]

        group_block_t_wave = [np.split(b, np.where(np.diff(b) != 0)[0] + 1) for b in block_t_wave]

        t_peak = np.zeros(len(peak), dtype=int)

        for idx, group in enumerate(group_block_t_wave):
            position = 0
            y_peak = -1

            for i in range(len(group)):
                if len(group[i]) > 0 and group[i][0] == 1 and len(group[i]) >= dur_window1:
                    t_peak_group = buf_ecg[r_t_min[idx] + position: r_t_min[idx] + position + len(group[i])]
                    if np.max(t_peak_group) > y_peak:
                        t_peak[idx] = r_t_min[idx] + position + np.argmax(t_peak_group)
                        y_peak = np.max(t_peak_group)

                position += len(group[i])
            if t_peak[idx] == 0:
                position = 0
                y_peak = -1
                for i in range(len(group)):
                    if (len(group[i]) > 0 and group[i][0] == 1) or (len(group) == 1 and len(group[0]) > 0):
                        t_peak_group = buf_ecg[r_t_min[idx] + position: r_t_min[idx] + position + len(group[i])]
                        if np.max(t_peak_group) > y_peak:
                            t_peak[idx] = r_t_min[idx] + position + np.argmax(t_peak_group)
                            y_peak = np.max(t_peak_group)

                    position += len(group[i])

        t_wave_offset = t_peak + int(t_offset_dynamic_interval_max * sampling_rate)

        t_wave_area = np.asarray([np.arange(start, min(stop, len(buf_ecg_10hz)), dtype=int)
                                  for start, stop in zip(t_peak, t_wave_offset)], dtype=int)
        y_t_wave_area = buf_ecg_10hz[t_wave_area]
        y_t_wave_area_grad = np.absolute(np.gradient(y_t_wave_area, axis=0))
        xm = t_peak + np.argmax(y_t_wave_area_grad, axis=1)
        ym = buf_ecg_10hz[xm]

        xr = xm + int(tp_isoelectric_segment * sampling_rate)

        t_offset_wave_area = np.asarray([np.arange(start, min(stop, len(buf_ecg_10hz)), dtype=int)
                                         for start, stop in zip(xm, xr)], dtype=int)

        area_a = 0.5 * (np.repeat(np.reshape(ym, (-1, 1)), int(tp_isoelectric_segment * sampling_rate), axis=1) -
                        buf_ecg_10hz[t_offset_wave_area]) * \
                 (2 * np.repeat(np.reshape(xr, (-1, 1)), int(tp_isoelectric_segment * sampling_rate), axis=1) -
                  t_offset_wave_area -
                  np.repeat(np.reshape(xm, (-1, 1)), int(tp_isoelectric_segment * sampling_rate), axis=1))
        t_offset = xm + np.argmax(area_a, axis=1)
        t_peak[t_peak < peak[0]] = 0
        t_offset[t_offset < peak[0]] = 0
        p_onset = (t_offset[:-1] + (qrs_onset[1:] - t_offset[:-1]) // 2).astype(int)
        p_onset = np.concatenate(([max(peak[0] - int(0.25 * sampling_rate), 0)], p_onset))
        p_onset[t_peak == 0] = 0
        qt = (t_offset - qrs_onset)
        qt[(qt < 0) + (qt > 500)] = 0
        qt = qt / sampling_rate

        qt[qt < 0] = 0
        rr_interval = np.divide(rr_sample, sampling_rate)
        qtc = np.around(np.divide(qt, np.sqrt(rr_interval)), decimals=4)

        hr = (60 / rr_interval).astype(int)
        st_seg_post = qrs_offset + int(0.06 * sampling_rate)
        st_seg_post[hr < 120] = qrs_offset[hr < 120] + int(0.08 * sampling_rate)
        st_segment = st_seg_post - qrs_offset
        st_slope = np.around(np.divide((buf_ecg[st_seg_post] - buf_ecg[qrs_offset]), st_segment), decimals=4)

        base_line = np.asarray([np.average(buf_ecg[np.arange(start, min(stop, len(buf_ecg_10hz)), dtype=int)])
                                for start, stop in zip(p_onset, t_offset)])
        st_level = np.around(buf_ecg[qrs_offset] - base_line, decimals=4)
        st_level[t_peak == 0] = 0

    else:
        qrs_onset = []
        qrs_offset = []
        t_peak = []
        t_offset = []
        p_onset = []
        qt = []
        qtc = []
        st_slope = []
        st_level = []
        hr = []

    return qrs_onset, qrs_offset, t_peak, t_offset, p_onset, qt, qtc, st_slope, st_level, hr
