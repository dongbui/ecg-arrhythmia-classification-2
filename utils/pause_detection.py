import numpy as np
import os

HES_SAMPLING_RATE = 200
NRR = 6

IDLE = 0
PAUSE1 = 1  # one missing QRS complex
PAUSE2 = 1  # two missing QRS complex
ASYSTOLE = 1  # – no QRS activity (QRS complex) for at least 5 seconds


class PauseEvents:
    def __init__(self, pause_thresh=2500):
        self.state = IDLE
        self.rrLimit = HES_SAMPLING_RATE * (pause_thresh / 2000)
        self.pauseThresh = pause_thresh
        self.NSamples = 0
        self.LastBeatSample = 0
        self.LastMeanRrInSample = 0
        self.RRs = np.zeros(NRR)
        self.RRTop = 0
        self.NSamples = 0

    '''
    // Return value: -2: First beat with non-zero sample
    //               -1: Beat with same sample
    //                0: No event is detected
    //                1: PAUSE1 is detected
    //                2: PAUSE2 is detected
    //                3: ASYSTOLE is detected
    '''
    def pause_detect(self, beat_sample, is_noise):
        if is_noise:
            self.LastBeatSample = 0
            return -3

        if beat_sample != self.LastBeatSample:
            self.NSamples += 1
            if self.LastBeatSample == 0:
                self.LastBeatSample = beat_sample
                return -2

            rr = beat_sample - self.LastBeatSample
            self.LastBeatSample = beat_sample
            tmp_rr = self.RRs[self.RRTop]
            self.RRs[self.RRTop] = rr
            self.RRTop += 1
            if self.RRTop >= NRR:
                self.RRTop = 0
        else:
            self.state = IDLE
            tmp_rr = -1
            return -1

        if self.NSamples < NRR + 1:
            nSUM = self.NSamples - 1
        else:
            nSUM = NRR

        RRmax = self.RRs[0]
        RRmin = self.RRs[0]
        RRsum = self.RRs[0]

        for i in range(1, nSUM):
            RRsum += self.RRs[i]
            if self.RRs[i] > RRmax:
                RRmax = self.RRs[i]
            if self.RRs[i] < RRmin:
                RRmin = self.RRs[i]

        if nSUM < 3:
            RR = (1.0 * RRsum) / nSUM
        else:
            RR = (1.0 * (RRsum - RRmax - RRmin)) / (nSUM - 2)

        MeanRrInSample = round(RR)

        lastRrInMillisecond = rr * 1000 / HES_SAMPLING_RATE
        if lastRrInMillisecond >= self.pauseThresh:
            if self.state == IDLE:
                self.state = PAUSE1
            elif self.state == PAUSE1:
                self.state = PAUSE2
            elif self.state == PAUSE2:
                self.state = ASYSTOLE

            if tmp_rr > 0:
                self.RRs[self.RRTop - 1] = tmp_rr

        else:
            self.state = IDLE

        self.LastMeanRrInSample = MeanRrInSample
        return self.state
