import numpy as np

HES_SR = 200
N_SRUN_EXIT = 3
SRUN_EXIT_INDEX_MAX = int(pow(2, N_SRUN_EXIT))
N_VT_EXIT = 3
VT_EXIT_INDEX_MAX = int(pow(2, N_VT_EXIT))
SWITCH_VT2BG = 1
SWITCH_VT2TRG = 1
N_VT2BG = 3
N_VT2TRG = 3
K_BG = 2 * N_VT2BG
K_TRG = 3 * N_VT2TRG

SV_states = ['start', 'idleB', 'idleT', 'Sx', 'Vx', 'SRUN', 'VT',
             'NV', 'NNV', 'BGwN', 'BGwV', 'TRwN0', 'TRwN1', 'TRwV']
SV_events = ['N', 'S1', 'S2', 'S3', 'S_RUN', 'V1', 'V2', 'V3', 'V_T',
             'V_Bigeminy', 'V_Trigeminy', 'S1_in_VT', 'S2_in_VT', 'V1_in_SRUN', 'V2_in_SRUN']

SRUN_EXIT_STATES1 = ['idleB', 'Vx']
SRUN_EXIT_EVENTS1 = ['N', 'N']
SRUN_EXIT_OFFSETS1 = [0, 0]
SRUN_EXIT_DURATIONS1 = [0, 0]
SRUN_EXIT_VCOUNTS1 = [0, 1]

SRUN_EXIT_STATES2 = ['idleT', 'NV', 'idleB', 'Vx']
SRUN_EXIT_EVENTS2 = ['N', 'N', 'V1', 'N']
SRUN_EXIT_OFFSETS2 = [0, 0, 1, 0]
SRUN_EXIT_DURATIONS2 = [0, 0, 1, 0]
SRUN_EXIT_VCOUNTS2 = [0, 1, 0, 2]

SRUN_EXIT_STATES3 = ['idleT', 'NNV', 'NV', 'Vx', 'idleT', 'NV', 'idleB', 'Vx']
SRUN_EXIT_EVENTS3 = ['N', 'N', 'N', 'N', 'V1', 'V1', 'V2', 'N']
SRUN_EXIT_OFFSETS3 = [0, 0, 0, 0, 2, 2, 2, 0]
SRUN_EXIT_DURATIONS3 = [0, 0, 0, 0, 1, 1, 2, 0]
SRUN_EXIT_VCOUNTS3 = [0, 1, 2, 2, 0, 1, 0, 3]

SRUN_NO_EXIT_EVENTS = ['N', 'V1_in_SRUN', 'V1_in_SRUN', 'V2_in_SRUN']
SRUN_NO_EXIT_OFFSETS = [0, 1, 2, 2]
SRUN_NO_EXIT_DURATIONS = [0, 1, 1, 2]

VT_EXIT_STATES1 = ['idleB', 'Sx']
VT_EXIT_EVENTS1 = ['N', 'N']
VT_EXIT_OFFSETS1 = [0, 0]
VT_EXIT_DURATIONS1 = [0, 0]
VT_EXIT_SCOUNTS1 = [0, 1]

VT_EXIT_STATES2 = ['idleT', 'Sx', 'idleB', 'Sx']
VT_EXIT_EVENTS2 = ['N', 'N', 'S1', 'N']
VT_EXIT_OFFSETS2 = [0, 0, 1, 0]
VT_EXIT_DURATIONS2 = [0, 0, 1, 0]
VT_EXIT_SCOUNTS2 = [0, 1, 0, 2]

VT_EXIT_STATES3 = ['idleT', 'Sx', 'idleB', 'Sx', 'idleT', 'Sx', 'idleB', 'Sx']
VT_EXIT_EVENTS3 = ['N', 'N', 'S1', 'N', 'S1', 'S1', 'S2', 'N']
VT_EXIT_OFFSETS3 = [0, 0, 1, 0, 2, 2, 2, 0]
VT_EXIT_DURATIONS3 = [0, 0, 1, 0, 1, 1, 2, 0]
VT_EXIT_SCOUNTS3 = [0, 1, 0, 2, 0, 1, 0, 3]

VT_NO_EXIT_EVENTS = ['N', 'S1_in_VT', 'S1_in_VT', 'S2_in_VT']
VT_NO_EXIT_OFFSETS = [0, 1, 2, 2]
VT_NO_EXIT_DURATIONS = [0, 1, 1, 2]

'''
// SVES/VES events detector function
// DetectedBeatSample -> beatsample ; HesBeatStatus -> beattype
// Set init = 1 for the first call, set init = 0 afterwards.
// beatsample must be int32 for about 124 days max. study duration
'''


class switcher(object):
    def __init__(self, state, init):  # , SV_E1, SV_E2, SV_D1, SV_D2, SV_OS1, SV_OS2):

        # self.beatsample = beatsample
        self.beattype = ''
        self.init = init
        # self.SV_E1 = SV_E1      #C pointer
        # self.SV_E2 = SV_E2      #C pointer
        # self.SV_D1 = SV_D1      #C pointer
        # self.SV_D2 = SV_D2      #C pointer
        # self.SV_OS1 = SV_OS1    #C pointer
        # self.SV_OS2 = SV_OS2

        # self.old_beatsample = 0
        self.state = state
        # self.SV_event1 = 'N'
        # self.SV_event2 = 'N'
        # self.SV_duration1 = 0
        # self.SV_duration2 = 0
        # self.SV_offset1 = 0
        # self.SV_offset2 = 0

        self.Scount = 0
        self.Vcount = 0
        self.Ncount = 0
        self.exit_index = 0
        self.NVcount = 0
        self.NNVcount = 0

    def indirect(self, beattype):

        self.beattype = beattype
        method_name = self.state
        method = getattr(self, method_name, lambda: 'Invalid')
        return method()
        # return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def start(self):

        self.SV_event1 = 'N'
        self.SV_event2 = 'N'
        self.SV_offset1 = 0
        self.SV_duration1 = 0
        self.SV_offset2 = 0
        self.SV_duration2 = 0
        self.Scount = 0
        self.Vcount = 0
        self.Ncount = 0

        if (self.beattype == '01'):
            self.state = 'idleB'
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            self.state = 'Vx'
            self.Vcount = 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            self.state = 'Sx'
            self.Scount = 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def idleB(self):

        self.SV_event1 = 'N'
        self.SV_event2 = 'N'
        self.SV_offset1 = 0
        self.SV_duration1 = 0
        self.SV_offset2 = 0
        self.SV_duration2 = 0
        self.Scount = 0
        self.Vcount = 0
        self.Ncount = 0
        if self.beattype == '01':
            self.state = 'idleT'
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            self.state = 'NV'
            self.Vcount = 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            self.Scount = 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def idleT(self):

        self.SV_event1 = 'N'
        self.SV_event2 = 'N'
        self.SV_offset1 = 0
        self.SV_duration1 = 0
        self.SV_offset2 = 0
        self.SV_duration2 = 0
        self.Scount = 0
        self.Vcount = 0
        self.Ncount = 0
        if self.beattype == '01':
            self.state = 'idleT'
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            self.state = 'NNV'
            self.Vcount = 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            self.state = 'Sx'
            self.Scount = 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def Sx(self):

        self.SV_event1 = 'N'
        self.SV_event2 = 'N'
        self.SV_offset1 = 0
        self.SV_duration1 = 0
        self.SV_offset2 = 0
        self.SV_duration2 = 0

        if self.beattype == '01':
            if self.Scount == 1:
                self.SV_event1 = 'S1'
                self.SV_offset1 = 1
                self.SV_duration1 = 1

            if self.Scount == 2:
                self.SV_event1 = 'S2'
                self.SV_offset1 = 2
                self.SV_duration1 = 2

            if self.Scount == 3:
                self.SV_event1 = 'S3'
                self.SV_offset1 = 3
                self.SV_duration1 = 3

            self.Scount = 0
            self.Vcount = 0
            self.state = 'idleB'
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            if self.Scount == 1:
                self.SV_event1 = 'S1'
                self.SV_offset1 = 1
                self.SV_duration1 = 1

            if self.Scount == 2:
                self.SV_event1 = 'S2'
                self.SV_offset1 = 2
                self.SV_duration1 = 2

            if self.Scount == 3:
                self.SV_event1 = 'S3'
                self.SV_offset1 = 3
                self.SV_duration1 = 3

            self.state = 'Vx'
            self.Vcount = 1
            self.Scount = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            self.Scount += 1
            if self.Scount > 3:
                self.state = 'SRUN'
                self.SV_event1 = 'S_RUN'
                self.SV_offset1 = 3
                self.SV_duration1 = self.Scount
                self.Ncount = 0
                self.exit_index = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def Vx(self):
        self.SV_event1 = 'N'
        self.SV_event2 = 'N'
        self.SV_offset1 = 0
        self.SV_duration1 = 0
        self.SV_offset2 = 0
        self.SV_duration2 = 0

        if self.beattype == '01':
            if self.Vcount == 1:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 1
                self.SV_duration1 = 1

            if self.Vcount == 2:
                self.SV_event1 = 'V2'
                self.SV_offset1 = 2
                self.SV_duration1 = 2

            if self.Vcount == 3:
                self.SV_event1 = 'V3'
                self.SV_offset1 = 3
                self.SV_duration1 = 3

            self.Scount = 0
            self.Vcount = 0
            self.state = 'idleB'
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            self.Vcount += 1
            if self.Vcount > 3:
                self.state = 'VT'
                self.SV_event1 = 'V_T'
                self.SV_offset1 = 3
                self.SV_duration1 = self.Vcount
                self.Ncount = 0
                self.exit_index = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            if self.Vcount == 1:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 1
                self.SV_duration1 = 1

            if self.Vcount == 2:
                self.SV_event1 = 'V2'
                self.SV_offset1 = 2
                self.SV_duration1 = 2

            if self.Vcount == 3:
                self.SV_event1 = 'V3'
                self.SV_offset1 = 3
                self.SV_duration1 = 3

            self.state = 'Sx'
            self.Scount = 1
            self.Vcount = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def SRUN(self):
        if self.beattype == '70':
            self.Scount += 1
            self.SV_event1 = 'S_RUN'
            self.SV_offset1 += 1
            self.SV_duration1 += 1

            self.SV_event2 = list(SRUN_NO_EXIT_EVENTS)[int(self.exit_index)]
            self.SV_offset2 = list(SRUN_NO_EXIT_OFFSETS)[int(self.exit_index)]
            self.SV_duration2 = list(SRUN_NO_EXIT_DURATIONS)[int(self.exit_index)]

            self.Ncount = 0
            self.exit_index = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        else:
            if self.exit_index == 0:
                self.SV_event2 = 'N'
                self.SV_offset2 = 0
                self.SV_duration2 = 0

            self.Ncount += 1
            self.exit_index = self.exit_index * 2 + int(int(self.beattype) / 60)
            self.exit_index = self.exit_index % SRUN_EXIT_INDEX_MAX

            if self.Ncount == N_SRUN_EXIT:
                if N_SRUN_EXIT == 1:
                    self.SV_offset2 = 1
                    self.SV_duration2 = self.SV_duration1

                    self.state = list(SRUN_EXIT_STATES1)[int(self.exit_index)]
                    self.SV_event1 = list(SRUN_EXIT_EVENTS1)[int(self.exit_index)]
                    self.SV_offset1 = list(SRUN_EXIT_OFFSETS1)[int(self.exit_index)]
                    self.SV_duration1 = list(SRUN_EXIT_DURATIONS1)[int(self.exit_index)]
                    self.Vcount = list(SRUN_EXIT_VCOUNTS1)[int(self.exit_index)]

                if N_SRUN_EXIT == 2:
                    self.SV_offset2 = 2
                    self.SV_duration2 = self.SV_duration1 - 1

                    self.state = list(SRUN_EXIT_STATES2)[int(self.exit_index)]
                    self.SV_event1 = list(SRUN_EXIT_EVENTS2)[int(self.exit_index)]
                    self.SV_offset1 = list(SRUN_EXIT_OFFSETS2)[int(self.exit_index)]
                    self.SV_duration1 = list(SRUN_EXIT_DURATIONS2)[int(self.exit_index)]
                    self.Vcount = list(SRUN_EXIT_VCOUNTS2)[int(self.exit_index)]

                if N_SRUN_EXIT == 3:
                    self.SV_offset2 = 3
                    self.SV_duration2 = self.SV_duration1 - 2

                    self.state = list(SRUN_EXIT_STATES3)[int(self.exit_index)]
                    self.SV_event1 = list(SRUN_EXIT_EVENTS3)[int(self.exit_index)]
                    self.SV_offset1 = list(SRUN_EXIT_OFFSETS3)[int(self.exit_index)]
                    self.SV_duration1 = list(SRUN_EXIT_DURATIONS3)[int(self.exit_index)]
                    self.Vcount = list(SRUN_EXIT_VCOUNTS3)[int(self.exit_index)]

                self.exit_index = 0
                self.Scount = 0
                self.SV_event2 = 'N'

            else:
                self.SV_event1 = 'S_RUN'
                self.SV_offset1 += 1
                self.SV_duration1 += 1

            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2


    def VT(self):
        if self.beattype == '60':
            self.Vcount += 1
            self.SV_event1 = 'V_T'
            self.SV_offset1 += 1
            self.SV_duration1 += 1

            self.SV_event2 = list(VT_NO_EXIT_EVENTS)[int(self.exit_index)]
            self.SV_offset2 = list(VT_NO_EXIT_OFFSETS)[int(self.exit_index)]
            self.SV_duration2 = list(VT_NO_EXIT_DURATIONS)[int(self.exit_index)]

            if self.exit_index == 0:
                if self.Ncount == 1:
                    self.NVcount += 1
                    self.NNVcount = 0

                if self.Ncount == 2:
                    self.NNVcount += 1
                    self.NVcount = 0

                if self.Ncount == 0:
                    self.NVcount = 0
                    self.NNVcount = 0

            else:
                self.NVcount = 0
                self.NNVcount = 0

            if self.NVcount == N_VT2BG and SWITCH_VT2BG == 1:
                self.SV_event2 = 'N'
                self.SV_offset2 = K_BG
                self.SV_duration2 = self.SV_duration1 - K_BG

                self.SV_event1 = 'V_Bigeminy'
                self.SV_offset1 = K_BG - 1
                self.SV_duration1 = K_BG
                self.state = 'BGwN'
                self.Vcount = 0
                self.NVcount = 0
                self.NNVcount = 0
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.NNVcount == N_VT2TRG and SWITCH_VT2TRG == 1:
                self.SV_event2 = 'N'
                self.SV_offset2 = K_TRG
                self.SV_duration2 = self.SV_duration1 - K_TRG

                self.SV_event1 = 'V_Trigeminy'
                self.SV_offset1 = K_TRG - 1
                self.SV_duration1 = K_TRG
                self.state = 'TRwN0'
                self.Vcount = 0
                self.NVcount = 0
                self.NNVcount = 0
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            self.Ncount = 0
            self.exit_index = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        else:
            if self.exit_index == 0:
                self.SV_event2 = 'N'
            self.SV_offset2 = 0
            self.SV_duration2 = 0

            self.Ncount += 1
            self.exit_index = self.exit_index * 2 + int(int(self.beattype) / 60)
            self.exit_index = self.exit_index % VT_EXIT_INDEX_MAX
            if self.Ncount == N_VT_EXIT:
                if N_VT_EXIT == 1:
                    self.SV_offset2 = 1
                    self.SV_duration2 = self.SV_duration1

                    self.state = list(VT_EXIT_STATES1)[int(self.exit_index)]
                    self.SV_event1 = list(VT_EXIT_EVENTS1)[int(self.exit_index)]
                    self.SV_offset1 = list(VT_EXIT_OFFSETS1)[int(self.exit_index)]
                    self.SV_duration1 = list(VT_EXIT_DURATIONS1)[int(self.exit_index)]
                    self.Scount = list(VT_EXIT_SCOUNTS1)[int(self.exit_index)]

                if N_VT_EXIT == 2:
                    self.SV_offset2 = 2
                    self.SV_duration2 = self.SV_duration1 - 1

                    self.state = list(VT_EXIT_STATES2)[int(self.exit_index)]
                    self.SV_event1 = list(VT_EXIT_EVENTS2)[int(self.exit_index)]
                    self.SV_offset1 = list(VT_EXIT_OFFSETS2)[int(self.exit_index)]
                    self.SV_duration1 = list(VT_EXIT_DURATIONS2)[int(self.exit_index)]
                    self.Scount = list(VT_EXIT_SCOUNTS2)[int(self.exit_index)]

                if N_VT_EXIT == 3:
                    self.SV_offset2 = 3
                    self.SV_duration2 = self.SV_duration1 - 2

                    self.state = list(VT_EXIT_STATES3)[int(self.exit_index)]
                    self.SV_event1 = list(VT_EXIT_EVENTS3)[int(self.exit_index)]
                    self.SV_offset1 = list(VT_EXIT_OFFSETS3)[int(self.exit_index)]
                    self.SV_duration1 = list(VT_EXIT_DURATIONS3)[int(self.exit_index)]
                    self.Scount = list(VT_EXIT_SCOUNTS3)[int(self.exit_index)]

                self.exit_index = 0
                self.Vcount = 0
                self.NVcount = 0
                self.NNVcount = 0
                self.SV_event2 = 'N'

            else:
                self.SV_event1 = 'V_T'
                self.SV_offset1 += 1
                self.SV_duration1 += 1

            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def NV(self):
        self.SV_event1 = 'N'
        self.SV_event2 = 'N'
        self.SV_offset1 = 0
        self.SV_duration1 = 0
        self.SV_offset2 = 0
        self.SV_duration2 = 0

        if self.beattype == '01':
            if self.Vcount == 1:
                self.Vcount = 2
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 2:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 2
                self.SV_duration1 = 1
                self.Scount = 0
                self.Vcount = 0
                self.state = 'idleT'
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 3:
                self.Vcount = 4
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 4:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 4
                self.SV_duration1 = 1
                self.SV_event2 = 'V1'
                self.SV_offset2 = 2
                self.SV_duration2 = 1
                self.Scount = 0
                self.Vcount = 0
                self.state = 'idleT'
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            if self.Vcount == 1:
                self.state = 'Vx'
                self.Vcount = 2
                self.Scount = 0
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 2:
                self.Vcount = 3
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 3:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 3
                self.SV_duration1 = 1
                self.state = 'Vx'
                self.Vcount = 2
                self.Scount = 0
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 4:
                self.SV_event1 = 'V_Bigeminy'
                self.SV_offset1 = 5
                self.SV_duration1 = 6
                self.state = 'BGwN'
                self.Vcount = 0
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            if self.Vcount == 1:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 1
                self.SV_duration1 = 1

            if self.Vcount == 2:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 2
                self.SV_duration1 = 1

            if self.Vcount == 3:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 3
                self.SV_duration1 = 1
                self.SV_event2 = 'V1'
                self.SV_offset2 = 1
                self.SV_duration2 = 1

            if self.Vcount == 4:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 4
                self.SV_duration1 = 1
                self.SV_event2 = 'V1'
                self.SV_offset2 = 2
                self.SV_duration2 = 1

            self.state = 'Sx'
            self.Scount = 1
            self.Vcount = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def BGwN(self):
        self.SV_event2 = 'N'
        self.SV_offset2 = 0
        self.SV_duration2 = 0

        if self.beattype == '01':
            self.state = 'BGwV'
            self.SV_event1 = 'V_Bigeminy'
            self.SV_offset1 += 1
            self.SV_duration1 += 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            self.state = 'Vx'
            self.Vcount = 1
            self.Scount = 0

            self.SV_event2 = 'N'
            self.SV_offset2 = 1
            self.SV_duration2 = self.SV_duration1

            self.SV_event1 = 'N'
            self.SV_offset1 = 0
            self.SV_duration1 = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            self.state = 'Sx'
            self.Scount = 1
            self.Vcount = 0

            self.SV_event2 = 'N'
            self.SV_offset2 = 1
            self.SV_duration2 = self.SV_duration1

            self.SV_event1 = 'N'
            self.SV_offset1 = 0
            self.SV_duration1 = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def BGwV(self):
        self.SV_event2 = 'N'
        self.SV_offset2 = 0
        self.SV_duration2 = 0

        if self.beattype == '01':
            self.Scount = 0
            self.Vcount = 0
            self.state = 'idleT'

            self.SV_event2 = 'N'
            self.SV_offset2 = 2
            self.SV_duration2 = self.SV_duration1 - 1

            self.SV_event1 = 'N'
            self.SV_offset1 = 0
            self.SV_duration1 = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            self.state = 'BGwN'
            self.SV_event1 = 'V_Bigeminy'
            self.SV_offset1 += 1
            self.SV_duration1 += 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            self.state = 'Sx'
            self.Scount = 1
            self.Vcount = 0

            self.SV_event2 = 'N'
            self.SV_offset2 = 2
            self.SV_duration2 = self.SV_duration1 - 1

            self.SV_event1 = 'N'
            self.SV_offset1 = 0
            self.SV_duration1 = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def NNV(self):
        self.SV_event1 = 'N'
        self.SV_event2 = 'N'
        self.SV_offset1 = 0
        self.SV_duration1 = 0
        self.SV_offset2 = 0
        self.SV_duration2 = 0

        if self.beattype == '01':
            if self.Vcount == 1:
                self.Vcount = 2
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 2:
                self.Vcount = 3
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 3:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 3
                self.SV_duration1 = 1
                self.Scount = 0
                self.Vcount = 0
                self.state = 'idleT'
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 4:
                self.Vcount = 5
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 5:
                self.Vcount = 6
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 6:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 6
                self.SV_duration1 = 1
                self.SV_event2 = 'V1'
                self.SV_offset2 = 3
                self.SV_duration2 = 1
                self.Scount = 0
                self.Vcount = 0
                self.state = 'idleT'
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            if (self.Vcount == 1):
                self.state = 'Vx'
                self.Vcount = 2
                self.Scount = 0
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 2:
                self.state = 'NV'
                self.Vcount = 3
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 3:
                self.Vcount = 4
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 4:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 4
                self.SV_duration1 = 1
                self.state = 'Vx'
                self.Vcount = 2
                self.Scount = 0
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 5:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 5
                self.SV_duration1 = 1
                self.SV_event2 = 'V1'
                self.SV_offset2 = 2
                self.SV_duration2 = 1
                self.state = 'NV'
                self.Vcount = 1
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

            if self.Vcount == 6:
                self.SV_event1 = 'V_Trigeminy'
                self.SV_offset1 = 8
                self.SV_duration1 = 9
                self.state = 'TRwN0'
                self.Vcount = 0
                return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                    self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            if self.Vcount == 1:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 1
                self.SV_duration1 = 1

            if self.Vcount == 2:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 2
                self.SV_duration1 = 1

            if self.Vcount == 3:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 3
                self.SV_duration1 = 1

            if self.Vcount == 4:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 4
                self.SV_duration1 = 1
                self.SV_event2 = 'V1'
                self.SV_offset2 = 1
                self.SV_duration2 = 1

            if self.Vcount == 5:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 5
                self.SV_duration1 = 1
                self.SV_event2 = 'V1'
                self.SV_offset2 = 2
                self.SV_duration2 = 1

            if self.Vcount == 6:
                self.SV_event1 = 'V1'
                self.SV_offset1 = 6
                self.SV_duration1 = 1
                self.SV_event2 = 'V1'
                self.SV_offset2 = 3
                self.SV_duration2 = 1

            self.state = 'Sx'
            self.Scount = 1
            self.Vcount = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def TRwN0(self):
        self.SV_event2 = 'N'
        self.SV_offset2 = 0
        self.SV_duration2 = 0

        if self.beattype == '01':
            self.state = 'TRwN1'
            self.SV_event1 = 'V_Trigeminy'
            self.SV_offset1 += 1
            self.SV_duration1 += 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            self.state = 'Vx'
            self.Vcount = 1
            self.Scount = 0

            self.SV_event2 = 'N'
            self.SV_offset2 = 1
            self.SV_duration2 = self.SV_duration1

            self.SV_event1 = 'N'
            self.SV_offset1 = 0
            self.SV_duration1 = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            self.state = 'Sx'
            self.Scount = 1
            self.Vcount = 0

            self.SV_event2 = 'N'
            self.SV_offset2 = 1
            self.SV_duration2 = self.SV_duration1

            self.SV_event1 = 'N'
            self.SV_offset1 = 0
            self.SV_duration1 = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def TRwN1(self):
        self.SV_event2 = 'N'
        self.SV_offset2 = 0
        self.SV_duration2 = 0

        if self.beattype == '01':
            self.state = 'TRwV'
            self.SV_event1 = 'V_Trigeminy'
            self.SV_offset1 += 1
            self.SV_duration1 += 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            self.state = 'NV'
            self.Vcount = 1

            self.SV_event2 = 'N'
            self.SV_offset2 = 2
            self.SV_duration2 = self.SV_duration1 - 1

            self.SV_event1 = 'N'
            self.SV_offset1 = 0
            self.SV_duration1 = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            self.state = 'Sx'
            self.Scount = 1
            self.Vcount = 0

            self.SV_event2 = 'N'
            self.SV_offset2 = 2
            self.SV_duration2 = self.SV_duration1 - 1

            self.SV_event1 = 'N'
            self.SV_offset1 = 0
            self.SV_duration1 = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

    def TRwV(self):
        self.SV_event2 = 'N'
        self.SV_offset2 = 0
        self.SV_duration2 = 0

        if self.beattype == '01':
            self.Scount = 0
            self.Vcount = 0
            self.state = 'idleT'

            self.SV_event2 = 'N'
            self.SV_offset2 = 3
            self.SV_duration2 = self.SV_duration1 - 2

            self.SV_event1 = 'N'
            self.SV_offset1 = 0
            self.SV_duration1 = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '60':
            self.state = 'TRwN0'
            self.SV_event1 = 'V_Trigeminy'
            self.SV_offset1 += 1
            self.SV_duration1 += 1
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2

        if self.beattype == '70':
            self.state = 'Sx'
            self.Scount = 1
            self.Vcount = 0

            self.SV_event2 = 'N'
            self.SV_offset2 = 3
            self.SV_duration2 = self.SV_duration1 - 2

            self.SV_event1 = 'N'
            self.SV_offset1 = 0
            self.SV_duration1 = 0
            return self.state, list(SV_events).index(self.SV_event1), list(SV_events).index(
                self.SV_event2), self.SV_duration1, self.SV_duration2, self.SV_offset1, self.SV_offset2
