import os
import re
import numpy as np
from datetime import datetime
import csv
from utils.notiviaemail import NotiForm
import socket


def main():
    top_select = 5
    db = ['mitdb', 'ahadb', 'nstdb', 'escdb']
    sves_statistics = ["mitdb", "nstdb"]
    directory = '/home/dattran/MegaDataset/beat_classification/200914/'
    directory_ref = '/media/dattran/MegaDataset/ec57_ref/'
    data_model = [x[0] for x in os.walk(directory) if os.path.basename(x[0]) in db]
    data_ref = [x[0] for x in os.walk(directory_ref) if os.path.basename(x[0]) in db]
    path_model = set(map(lambda x: x[:-(len(os.path.basename(x)) + 1)], data_model))
    group_model = [[y for y in data_model if x in y] for x in path_model]
    group_model.sort()
    group_model.insert(0, data_ref)
    squared_error_gross = np.zeros(len(group_model))
    squared_error_average = np.zeros(len(group_model))

    now = datetime.now()  # current date and time
    str_now = now.strftime("%y_%m_%d_%H_%M_%S")
    squared_error_report_file = open(directory + '/squared_error_report_{}.csv'.format(str_now), mode='w')
    squared_error_fieldnames = ['#',
                                'Case',
                                'Squared error gross',
                                'Squared error average',
                                'Squared error gross (ref)',
                                'Squared error average (ref)']
    squared_error_report_writer = csv.DictWriter(squared_error_report_file, fieldnames=squared_error_fieldnames)
    squared_error_report_writer.writeheader()

    detail_report_file = open(directory + '/detail_report_{}.csv'.format(str_now), mode='w')
    detail_fieldnames = ['#',
                         'Case',
                         'Metric',
                         'Database',
                         'N(se)',
                         'N(p+)',
                         'N_ref(se)',
                         'N_ref(p+)',
                         'V(se)',
                         'V(p+)',
                         'V_ref(se)',
                         'V_ref(p+)',
                         'S(se)',
                         'S(p+)',
                         'S_ref(se)',
                         'S_ref(p+)']
    detail_report_writer = csv.DictWriter(detail_report_file, fieldnames=detail_fieldnames)
    detail_report_writer.writeheader()

    group_report_file = open(directory + '/group_report_{}.csv'.format(str_now), mode='w')
    group_fieldnames = ['#',
                        'Case',
                        'Squared error gross',
                        'Squared error average',
                        'Squared error gross (ref)',
                        'Squared error average (ref)']
    group_report_writer = csv.DictWriter(group_report_file, fieldnames=group_fieldnames)
    group_report_writer.writeheader()

    index = 1

    ref_squared_error = dict()

    ref_detail = dict()

    for i, g in enumerate(group_model):
        isref = False
        row_item = dict()
        row_group_item = dict()
        row_detail_item = dict()
        row_tmp = g[0].replace(directory, '')
        if "ec57-2020-beat0-segment0" in row_tmp:
            isref = True

        row_item["Case"] = row_tmp.split('/')[0] + '/' + row_tmp.split('/')[2] + '/' + row_tmp.split('/')[3]
        row_detail_item["Case"] = row_tmp.split('/')[0] + '/' + row_tmp.split('/')[2]

        row_detail_item["Metric"] = row_tmp.split('/')[3]
        row_item["#"] = i
        row_group_item["#"] = i
        row_group_item["Case"] = row_tmp.split('/')[0] + '/' + row_tmp.split('/')[2] + '/' + row_tmp.split('/')[3]

        for f in g:
            row_detail_item["#"] = index
            row_detail_item["Database"] = os.path.basename(f)
            path_out = '{}/{}_QRS_report_line.out'.format(f, os.path.basename(f))
            file_out = open(path_out)
            content = file_out.readlines()
            gross_value = re.findall(r'[-+]?\d*\.\d+|\d+', content[-5])
            average_value = re.findall(r'[-+]?\d*\.\d+|\d+', content[-4])

            squared_error_gross[i] += (1 - float(gross_value[0]) / 100) * (1 - float(gross_value[0]) / 100) + \
                                      (1 - float(gross_value[1]) / 100) * (1 - float(gross_value[1]) / 100) + \
                                      (1 - float(gross_value[2]) / 100) * (1 - float(gross_value[2]) / 100) + \
                                      (1 - float(gross_value[3]) / 100) * (1 - float(gross_value[3]) / 100)

            row_detail_item["N(se)"] = float(gross_value[0]) / 100
            row_detail_item["N(p+)"] = float(gross_value[1]) / 100

            row_detail_item["V(se)"] = float(gross_value[2]) / 100
            row_detail_item["V(p+)"] = float(gross_value[3]) / 100

            try:
                if os.path.basename(f) in sves_statistics:
                    squared_error_gross[i] += (1 - float(gross_value[4]) / 100) * (1 - float(gross_value[4]) / 100) + \
                                              (1 - float(gross_value[5]) / 100) * (1 - float(gross_value[5]) / 100)

                    row_detail_item["S(se)"] = float(gross_value[4]) / 100
                    row_detail_item["S(p+)"] = float(gross_value[5]) / 100
                else:
                    row_detail_item["S(se)"] = "-"
                    row_detail_item["S(p+)"] = "-"
            except:
                print('get error in {}'.format(f))

            squared_error_average[i] += (1 - float(average_value[0]) / 100) * (1 - float(average_value[0]) / 100) + \
                                        (1 - float(average_value[1]) / 100) * (1 - float(average_value[1]) / 100) + \
                                        (1 - float(average_value[2]) / 100) * (1 - float(average_value[2]) / 100) + \
                                        (1 - float(average_value[3]) / 100) * (1 - float(average_value[3]) / 100)

            if os.path.basename(f) in sves_statistics:
                squared_error_average[i] += (1 - float(average_value[4]) / 100) * (1 - float(average_value[4]) / 100) + \
                                            (1 - float(average_value[5]) / 100) * (1 - float(average_value[5]) / 100)

            if isref:
                ref_detail[os.path.basename(f)] = dict()
                ref_detail[os.path.basename(f)]["N_ref(se)"] = row_detail_item["N(se)"]
                ref_detail[os.path.basename(f)]["N_ref(p+)"] = row_detail_item["N(p+)"]
                ref_detail[os.path.basename(f)]["V_ref(se)"] = row_detail_item["V(se)"]
                ref_detail[os.path.basename(f)]["V_ref(p+)"] = row_detail_item["V(p+)"]
                ref_detail[os.path.basename(f)]["S_ref(se)"] = row_detail_item["S(se)"]
                ref_detail[os.path.basename(f)]["S_ref(p+)"] = row_detail_item["S(p+)"]
            else:
                row_detail_item["N_ref(se)"] = ref_detail[os.path.basename(f)]["N_ref(se)"]
                row_detail_item["N_ref(p+)"] = ref_detail[os.path.basename(f)]["N_ref(p+)"]
                row_detail_item["V_ref(se)"] = ref_detail[os.path.basename(f)]["V_ref(se)"]
                row_detail_item["V_ref(p+)"] = ref_detail[os.path.basename(f)]["V_ref(p+)"]
                row_detail_item["S_ref(se)"] = ref_detail[os.path.basename(f)]["S_ref(se)"]
                row_detail_item["S_ref(p+)"] = ref_detail[os.path.basename(f)]["S_ref(p+)"]

                detail_report_writer.writerow(row_detail_item)
                index += 1

        if isref:
            ref_squared_error["gross"] = squared_error_gross[i]
            ref_squared_error["average"] = squared_error_average[i]
        else:
            row_item["Squared error gross"] = squared_error_gross[i]
            row_item["Squared error average"] = squared_error_average[i]
            row_item["Squared error gross (ref)"] = ref_squared_error["gross"]
            row_item["Squared error average (ref)"] = ref_squared_error["average"]
            squared_error_report_writer.writerow(row_item)

            row_group_item["Squared error gross"] = squared_error_gross[i]
            row_group_item["Squared error average"] = squared_error_average[i]
            row_group_item["Squared error gross (ref)"] = ref_squared_error["gross"]
            row_group_item["Squared error average (ref)"] = ref_squared_error["average"]
            group_report_writer.writerow(row_group_item)

    squared_error_report_file.close()
    group_report_file.close()
    detail_report_file.close()
    index_squared_error_gross = np.asarray([x for _, x in sorted(zip(squared_error_gross,
                                                                     np.arange(len(squared_error_gross))),
                                                                 reverse=False)])

    if len(index_squared_error_gross) < top_select:
        top_select = len(index_squared_error_gross)

    now = datetime.now()  # current date and time
    str_version = ''.join(sves_statistics) + now.strftime("ready2run_%y_%m_%d")
    gtmp = directory.split('/')
    gt = 0

    import getpass
    for gt, gstr in enumerate(gtmp):
        if getpass.getuser() in gstr:
            break

    dir_model_selected = ""
    for gm in range(gt, len(gtmp) - 1):
        dir_model_selected += gtmp[gm] + "/"

    if not os.path.exists('model_selected/{}'.format(dir_model_selected)):
        os.makedirs('model_selected/{}'.format(dir_model_selected))

    all_model_2_run_file = open('model_selected/{}/{}.csv'.format(dir_model_selected, str_version), mode='w')

    model_2_run_fieldnames = ['#',
                              'Case',
                              'SumSquaresError',
                              'Checkpoint',
                              'ain_ext']

    all_model_2_run_writer = csv.DictWriter(all_model_2_run_file, fieldnames=model_2_run_fieldnames)
    all_model_2_run_writer.writeheader()
    count = 0
    all_row = dict()

    model_2_run = open('model_selected/{}/{}_{}.csv'.format(dir_model_selected, str_version, 0), mode='w')
    model_2_run_writer = csv.DictWriter(model_2_run, fieldnames=model_2_run_fieldnames)
    model_2_run_writer.writeheader()

    for n, g in enumerate(index_squared_error_gross):
        row = dict()

        row["#"] = n
        all_row["#"] = count
        if 'best_entropy_loss' in group_model[g][0]:
            checkpoint_path = group_model[g][0].replace('best_entropy_loss', 'best_loss')
        else:
            checkpoint_path = group_model[g][0].replace('best_squared_err', 'best_new_metric')

        checkpoint_path = os.path.dirname(checkpoint_path.replace('ec57', 'checkpoint'))
        # checkpoint_path = checkpoint_path.replace(directory, '')

        row_tmp = group_model[g][0].replace(directory, '')
        if "ec57-2020-beat0-segment0" in row_tmp:
            continue

        row["Case"] = row_tmp.split('/')[0] + '/' + row_tmp.split('/')[2] + '/' + row_tmp.split('/')[3]
        row["Checkpoint"] = checkpoint_path
        row["SumSquaresError"] = squared_error_gross[g]

        all_row["Case"] = row_tmp.split('/')[0] + '/' + row_tmp.split('/')[2] + '/' + row_tmp.split('/')[3]
        all_row["Checkpoint"] = checkpoint_path
        all_row["SumSquaresError"] = squared_error_gross[g]

        str_version = checkpoint_path.split('/')
        for i, v in enumerate(str_version):
            if v == "checkpoint":
                break

        beat_ext_ai = str_version[i - 1]
        model_beat_ext_ai = str_version[i + 1]

        replacements = dict()
        for m in range(26):
            replacements[str(m)] = str(chr(m + 97))

        for m in range(26, 52):
            replacements[str(m)] = str(chr(m - 26 + 65))

        for m in range(52, 78):
            replacements[str(m)] = 'a'

        for m in range(78, 104):
            replacements[str(m)] = 'b'

        for m in range(104, 130):
            replacements[str(m)] = 'c'

        for m in range(130, 156):
            replacements[str(m)] = 'd'

        for m in range(156, 182):
            replacements[str(m)] = 'e'

        for m in range(182, 208):
            replacements[str(m)] = 'f'

        for m in range(208, 2500):
            replacements[str(m)] = 'g'

        beat_ext_ai = beat_ext_ai.replace("beat", "")
        beat_ext_ai = beat_ext_ai.replace("subject", "")

        beat_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], beat_ext_ai)
        beat_ext_ai = beat_ext_ai.replace('.', '')
        beat_ext_ai = beat_ext_ai.replace('-', '')
        model_beat_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], model_beat_ext_ai)
        model_beat_ext_ai = model_beat_ext_ai.replace('.', '')
        model_beat_ext_ai = model_beat_ext_ai.replace('-', '')
        model_beat_ext_ai = model_beat_ext_ai.replace('_', '')

        if 'best_loss' in checkpoint_path:
            beat_ext_ai += model_beat_ext_ai + 'l'
        else:
            beat_ext_ai += model_beat_ext_ai + 'm'

        ext_ai = beat_ext_ai
        ext_ai = ''.join(sorted(set(ext_ai), key=ext_ai.index))

        row["ain_ext"] = ext_ai
        all_row["ain_ext"] = ext_ai

        all_model_2_run_writer.writerow(all_row)
        count += 1
        if n < top_select:
            model_2_run_writer.writerow(row)

    model_2_run.close()

    all_model_2_run_file.close()

    print()
    print('>>>>>>>>>>>>>>>>>> {} with squared_error_gross {}<<<<<<<<<<<<<<<<<'.
          format(os.path.dirname(group_model[index_squared_error_gross[0]][0]),
                 squared_error_gross[index_squared_error_gross[0]]))


if __name__ == "__main__":
    main()
