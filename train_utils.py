from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import csv
import datetime
import json
import math
import os
import re
import sys
import time
from glob import glob
from multiprocessing import Pool
from os.path import basename, dirname
from utils.pause_detection import PauseEvents
from utils.tb_detection import TBEvents
from wfdb.processing import resample_sig
from utils.logging import TextLogging
import numpy as np
import tensorflow as tf
from collections import defaultdict
import beat as model
import beat_input as data_model
import define
import wfdb as wf
from beat_utils import (
    configure_model,
    format_metrics,
    load_tfrecords,
    print_cm,
    _calc_num_steps
)
from utils.ec57_test import ec57_eval, del_result, clone_result
from utils.reprocessing import (
    norm,
    bwr,
    bwr_dwt,
    bwr_dwt2,
    butter_lowpass_filter,
    agglomerative_clustering,
    select_beat_from_group
)
from wfdb.processing import resample_sig

FLAGS = tf.compat.v1.flags.FLAGS
ain_ext = ['aina', 'ainb', 'ainc']


def check_type_event(event, type):
    return int(event & type == type)


def remove_short_event(list_event,
                       event,
                       afib_len,
                       brady_len,
                       tachy_len,
                       pause_len,
                       is_last_beat,
                       num_beat_remove=6):
    """

    :param list_event:
    :param event:
    :param afib_len:
    :param brady_len:
    :param tachy_len:
    # :param pause_len:
    :param is_last_beat:
    :param num_beat_remove:
    :return:
    """

    if check_type_event(event, define.NOISE) == 1:
        event = define.NOISE

    if len(list_event) > 0:
        # TACHY
        if check_type_event(list_event[-1], define.TACHY) == 0:
            if check_type_event(event, define.TACHY) == 1:
                list_event[-1] |= define.TACHY
                tachy_len += 1

        elif check_type_event(event, define.TACHY) == 0:
            if tachy_len < num_beat_remove:
                for i in range(-tachy_len, 0):
                    list_event[i] = list_event[i] & (define.BRADY + define.AFIB + define.PAUSE + define.NOISE)

            tachy_len = 0

        # BRADY
        if check_type_event(list_event[-1], define.BRADY) == 0:
            if check_type_event(event, define.BRADY) == 1:
                list_event[-1] |= define.BRADY
                brady_len += 1

        elif check_type_event(event, define.BRADY) == 0:
            if brady_len < num_beat_remove:
                for i in range(-brady_len, 0):
                    list_event[i] = list_event[i] & (define.TACHY + define.AFIB + define.PAUSE + define.NOISE)

            brady_len = 0

        # AFIB
        if check_type_event(list_event[-1], define.AFIB) == 0:
            if check_type_event(event, define.AFIB) == 1:
                list_event[-1] |= define.AFIB
                afib_len += 1

        elif check_type_event(event, define.AFIB) == 0:
            if afib_len < num_beat_remove:
                for i in range(-afib_len, 0):
                    list_event[i] = list_event[i] & (define.TACHY + define.BRADY + define.PAUSE + define.NOISE)

            afib_len = 0

        # PAUSE
        if check_type_event(list_event[-1], define.PAUSE) == 0:
            if check_type_event(event, define.PAUSE) == 1:
                list_event[-1] |= define.PAUSE
                pause_len += 1

        elif check_type_event(event, define.PAUSE) == 0:
            pause_len = 0

    tachy_len += check_type_event(event, define.TACHY)
    brady_len += check_type_event(event, define.BRADY)
    afib_len += check_type_event(event, define.AFIB)
    pause_len += check_type_event(event, define.PAUSE)

    if is_last_beat:
        if tachy_len < num_beat_remove and (check_type_event(event, define.TACHY) == 1):
            event = event & (define.BRADY + define.AFIB + define.PAUSE)
            for i in range(-tachy_len, 0):
                list_event[i] = list_event[i] & (define.BRADY + define.AFIB + define.PAUSE + define.NOISE)

        if brady_len < num_beat_remove and (check_type_event(event, define.BRADY) == 1):
            event = event & (define.TACHY + define.AFIB + define.PAUSE)
            for i in range(-brady_len, 0):
                list_event[i] = list_event[i] & (define.TACHY + define.AFIB + define.PAUSE + define.NOISE)

        if afib_len < num_beat_remove and (check_type_event(event, define.AFIB) == 1):
            event = event & (define.TACHY + define.BRADY + define.PAUSE)
            for i in range(-afib_len, 0):
                list_event[i] = list_event[i] & (define.TACHY + define.BRADY + define.PAUSE + define.NOISE)

    list_event.append(event)
    return list_event, tachy_len, brady_len, afib_len, pause_len


def correct_checkpoint_file(checkpoint_dir_path):
    if checkpoint_dir_path is None:
        return -1

    checkpoint_content = list()
    with open(checkpoint_dir_path + '/checkpoint', 'r') as checkpoint_file:
        a = checkpoint_file.readlines()
        checkpoint_content.append(a[0].split('/')[0] + checkpoint_dir_path + '/' + a[0].split('/')[-1])
        checkpoint_content.append(a[1].split('/')[0] + checkpoint_dir_path + '/' + a[1].split('/')[-1])

    with open(checkpoint_dir_path + '/checkpoint', 'w') as checkpoint_file:
        checkpoint_file.writelines(checkpoint_content)


# def train_beat(use_gpu_index,
#                name,
#                ver,
#                output_dir,
#                model_dir,
#                lbl_train,
#                class_weights,
#                resume_from,
#                train_directory,
#                eval_directory,
#                batch_size,
#                epoch_num):
#     arg_list = [[use_gpu_index,
#                  name,
#                  ver,
#                  output_dir,
#                  model_dir,
#                  lbl_train,
#                  class_weights,
#                  resume_from,
#                  train_directory,
#                  eval_directory,
#                  batch_size,
#                  epoch_num]]
#
#     with Pool(processes=1) as pool:
#         result = pool.starmap(_train_beat, arg_list)
#
#     return result


class CustomCallback(tf.keras.callbacks.Callback):
    def __init__(self,
                 name,
                 log_train,
                 stopped_epoch,
                 last_checkpoint_dir,
                 best_loss_checkpoint_dir,
                 best_ozsun_checkpoint_dir,
                 best_f1_checkpoint_dir,
                 bk_metric,
                 lbl_train,
                 output_dir,
                 ver,
                 field_name,
                 best_loss=-1,
                 best_squared_error_metrics=-1,
                 best_f1_score_metrics=-1,
                 valid_freq=6,
                 length_train=None,
                 length_valid=None,
                 tensorboard_dir=None
                 ):
        super(CustomCallback, self).__init__()
        self.bk_metric = bk_metric
        self.log_train = log_train
        self.last_checkpoint_dir = last_checkpoint_dir
        self.best_loss_checkpoint_dir = best_loss_checkpoint_dir
        self.best_ozsun_checkpoint_dir = best_ozsun_checkpoint_dir
        self.best_f1_checkpoint_dir = best_f1_checkpoint_dir
        self.name = name
        self.stopped_epoch = stopped_epoch
        self.lbl_train = lbl_train
        self.best_loss = best_loss
        self.best_squared_error_metrics = best_squared_error_metrics
        self.best_f1_score_metrics = best_f1_score_metrics
        self.output_dir = output_dir
        self.ver = ver
        self.fieldnames = field_name
        self.train_progressbar = None
        self.length_train = length_train
        self.length_valid = length_valid
        self.progress = 0
        self.valid_freq = valid_freq
        self.tensorboard_dir = tensorboard_dir

    def on_test_batch_end(self, batch, logs=None):
        self.progress += 1
        if self.length_train is not None and self.length_valid is not None:
            self.train_progressbar.update(self.progress)

    def on_train_batch_end(self, batch, logs=None):
        self.progress += 1
        if self.length_train is not None:
            self.train_progressbar.update(self.progress)

    def on_epoch_begin(self, epoch, logs=None):
        epoch += 1
        print("Epoch {}/{}".format(epoch, self.stopped_epoch))
        self.log_train.write_mylines("Epoch {}/{}\n".format(epoch, self.stopped_epoch))
        self.progress = 0
        if epoch >= self.valid_freq and epoch % self.valid_freq == 0:
            if self.length_train is not None and self.length_valid is not None:
                self.train_progressbar = tf.keras.utils.Progbar(self.length_train + self.length_valid)
        else:
            if self.length_train is not None:
                self.train_progressbar = tf.keras.utils.Progbar(self.length_train)

    def on_epoch_end(self, epoch, logs=None):
        epoch += 1
        report_row = dict()
        report_row['epoch'] = epoch
        report_row['accuracy_train'] = logs['accuracy']
        report_row['loss_train'] = logs['loss']
        report_row['precision_train'] = logs['precision']
        report_row['recall_train'] = logs['recall']

        train_metrics = {
            'accuracy': logs['accuracy'],
            'loss': logs['loss'],
            'precision': logs['precision'],
            'recall': logs['recall'],
        }
        print("Training " + format_metrics(train_metrics))
        self.log_train.write_mylines("Training " + format_metrics(train_metrics) + '\n')

        confusion_matrix = logs['confusion_matrix']
        cm_str = print_cm(confusion_matrix.copy(), self.lbl_train.keys(), False)
        self.log_train.write_mylines("Confusion \n" + cm_str + '\n')
        self.model.save_weights(os.path.join(self.last_checkpoint_dir, self.name + "-epoch-{}".format(epoch)))
        FP = confusion_matrix.sum(axis=0) - np.diag(confusion_matrix)
        FN = confusion_matrix.sum(axis=1) - np.diag(confusion_matrix)
        TP = np.diag(confusion_matrix)
        TPR = TP / (TP + FN)
        PPV = TP / (TP + FP)
        squared_error_metrics_train = 0
        f1_score_metrics_train = 0
        for i, c in enumerate(self.lbl_train.keys()):

            if (1 - TPR[i]) > 0 and (1 - PPV[i]) > 0:
                if i > 1:
                    squared_error_metrics_train += (1 - TPR[i]) * (1 - TPR[i]) + (1 - PPV[i]) * (1 - PPV[i])
                    f1_score_metrics_train += 2 * TPR[i] * PPV[i] / (TPR[i] + PPV[i])
                print('{} - se: {}; p+: {}; f1: {}'.format(c, TPR[i], PPV[i], 2 * TPR[i] * PPV[i] / (TPR[i] + PPV[i])))
                self.log_train.write_mylines('{} - se: {}; p+: {}; f1: {}'.format(c, TPR[i], PPV[i], 2 * TPR[i] * PPV[i]
                                                                                  / (TPR[i] + PPV[i])) + '\n')
            else:
                print('{} - se: {}; p+: {}; f1: {}'.format(c, TPR[i], PPV[i], 2 * TPR[i] * PPV[i] / (TPR[i] + PPV[i])))
                self.log_train.write_mylines('{} - se: {}; p+: {}; f1: {}'.format(c, TPR[i], PPV[i], 2 * TPR[i] * PPV[i]
                                                                                  / (TPR[i] + PPV[i])) + '\n')
                squared_error_metrics_train = np.nan
                f1_score_metrics_train = np.nan
                break

        f1_score_metrics_train = f1_score_metrics_train / (len(self.lbl_train.keys()) - 2)
        if self.tensorboard_dir is not None:
            with tf.summary.create_file_writer(self.tensorboard_dir + '/train').as_default():
                tf.summary.scalar('squared_error', squared_error_metrics_train, step=epoch - 1)
                tf.summary.scalar('f1_score', f1_score_metrics_train, step=epoch - 1)

        report_row['squared_error_metrics_train'] = squared_error_metrics_train
        report_row['f1_score_metrics_train'] = f1_score_metrics_train

        print('squared_error_metrics_train :{}'.format(squared_error_metrics_train))
        print('f1_score_metrics_train :{}'.format(f1_score_metrics_train))
        self.log_train.write_mylines('squared_error_metrics_train :{}'.format(squared_error_metrics_train) + '\n')
        self.log_train.write_mylines('f1_score_metrics_train :{}'.format(f1_score_metrics_train) + '\n')
        # Eval region
        if epoch >= self.valid_freq and epoch % self.valid_freq == 0:
            val_metrics = {
                'accuracy': logs['val_accuracy'],
                'loss': logs['val_loss'],
                'precision': logs['val_precision'],
                'recall': logs['val_recall']
            }
            print("Validation " + format_metrics(val_metrics))
            str_val_metrics = format_metrics(val_metrics)
            num_val_metrics = re.findall(r'\d+\.\d+', str_val_metrics)
            self.log_train.write_mylines("Validation " + format_metrics(val_metrics) + '\n')

            confusion_matrix = logs['val_confusion_matrix']
            cm_str = print_cm(confusion_matrix.copy(), self.lbl_train.keys(), False)
            self.log_train.write_mylines("Confusion \n" + cm_str + '\n')

            report_row['accuracy_eval'] = val_metrics['accuracy']
            report_row['recall_eval'] = val_metrics['recall']
            report_row['precision_eval'] = val_metrics['precision']
            report_row['loss_eval'] = val_metrics['loss']

            FP = confusion_matrix.sum(axis=0) - np.diag(confusion_matrix)
            FN = confusion_matrix.sum(axis=1) - np.diag(confusion_matrix)
            TP = np.diag(confusion_matrix)
            TPR = TP / (TP + FN)
            PPV = TP / (TP + FP)
            squared_error_metrics_eval = 0
            f1_score_metrics_eval = 0
            for i, c in enumerate(self.lbl_train.keys()):
                if (1 - TPR[i]) > 0 and (1 - PPV[i]) > 0:
                    if i > 1:
                        squared_error_metrics_eval += (1 - TPR[i]) * (1 - TPR[i]) + (1 - PPV[i]) * (1 - PPV[i])
                        f1_score_metrics_eval += 2 * TPR[i] * PPV[i] / (TPR[i] + PPV[i])
                    print('{} - se: {}; p+: {}; f1: {}'.format(c, TPR[i], PPV[i],
                                                               2 * TPR[i] * PPV[i] / (TPR[i] + PPV[i])))
                    self.log_train.write_mylines(
                        '{} - se: {}; p+: {}; f1: {}'.format(c, TPR[i], PPV[i], 2 * TPR[i] * PPV[i]
                                                             / (TPR[i] + PPV[i])) + '\n')
                else:
                    print('{} - se: {}; p+: {}; f1: {}'.format(c, TPR[i], PPV[i],
                                                               2 * TPR[i] * PPV[i] / (TPR[i] + PPV[i])))
                    self.log_train.write_mylines('{} - se: {}; p+: {}; f1: {}'
                                                 .format(c, TPR[i], PPV[i],
                                                         2 * TPR[i] * PPV[i] / (TPR[i] + PPV[i])) + '\n')
                    squared_error_metrics_eval = np.nan
                    f1_score_metrics_eval = np.nan
                    break

            f1_score_metrics_eval = f1_score_metrics_eval / (len(self.lbl_train.keys()) - 2)

            if self.tensorboard_dir is not None:
                with tf.summary.create_file_writer(self.tensorboard_dir + '/validation').as_default():
                    tf.summary.scalar('squared_error', squared_error_metrics_eval, step=epoch - 1)
                    tf.summary.scalar('f1_score', f1_score_metrics_eval, step=epoch - 1)

            report_row['squared_error_metrics_eval'] = squared_error_metrics_eval
            report_row['f1_score_metrics_eval'] = f1_score_metrics_eval
            print('squared_error_metrics_eval :{}'.format(squared_error_metrics_eval))
            print('f1_score_metrics_eval :{}'.format(f1_score_metrics_eval))
            self.log_train.write_mylines('squared_error_metrics_eval :{}'.format(squared_error_metrics_eval) + '\n')
            self.log_train.write_mylines('f1_score_metrics_eval :{}'.format(f1_score_metrics_eval) + '\n')
            # endregion Eval

            # region Save model
            if self.best_loss < 0 or float(num_val_metrics[1]) < self.best_loss:
                self.log_train.write_mylines("======================================================================\n")
                self.log_train.write_mylines(
                    "Found better checkpoint! Saving to {}\n".format(self.best_loss_checkpoint_dir))
                self.log_train.write_mylines("======================================================================\n")
                print("===========================================================================")
                print("Found loss better checkpoint! Saving to {}".format(self.best_loss_checkpoint_dir))
                print("===========================================================================")
                self.model.save_weights(
                    os.path.join(self.best_loss_checkpoint_dir, self.name + "-epoch-{}".format(epoch)))
                self.best_loss = float(num_val_metrics[1])
                self.bk_metric["best_loss"] = self.best_loss
                bk_metric_file = open('{}/{}_{}_bk_metric.txt'.format(self.output_dir, self.ver, self.name), 'w')
                json.dump(self.bk_metric, bk_metric_file)
                bk_metric_file.close()

            if not math.isnan(squared_error_metrics_eval) and \
                    (squared_error_metrics_eval < self.best_squared_error_metrics or
                     self.best_squared_error_metrics < 0):
                self.log_train.write_mylines("======================================================================\n")
                self.log_train.write_mylines(
                    "Found better checkpoint! Saving to {}\n".format(self.best_ozsun_checkpoint_dir))
                self.log_train.write_mylines("======================================================================\n")
                print("===========================================================================")
                print("Found best new metric checkpoint! Saving to {}".format(self.best_ozsun_checkpoint_dir))
                print("===========================================================================")
                self.model.save_weights(
                    os.path.join(self.best_ozsun_checkpoint_dir, self.name + "-epoch-{}".format(epoch)))
                self.best_squared_error_metrics = squared_error_metrics_eval
                self.bk_metric["best_squared_error_metrics"] = self.best_squared_error_metrics
                bk_metric_file = open('{}/{}_{}_bk_metric.txt'.format(self.output_dir, self.ver, self.name), 'w')
                json.dump(self.bk_metric, bk_metric_file)
                bk_metric_file.close()

            if not math.isnan(f1_score_metrics_eval) and (f1_score_metrics_eval > self.best_f1_score_metrics
                                                          or self.best_f1_score_metrics < 0):
                self.log_train.write_mylines("======================================================================\n")
                self.log_train.write_mylines(
                    "Found better checkpoint! Saving to {}\n".format(self.best_f1_checkpoint_dir))
                self.log_train.write_mylines("======================================================================\n")
                print("===========================================================================")
                print("Found best new f1 score checkpoint! Saving to {}".format(self.best_f1_checkpoint_dir))
                print("===========================================================================")
                self.model.save_weights(
                    os.path.join(self.best_f1_checkpoint_dir, self.name + "-epoch-{}".format(epoch)))
                self.best_f1_score_metrics = f1_score_metrics_eval
                self.bk_metric["best_f1_score_metrics"] = self.best_f1_score_metrics
                bk_metric_file = open('{}/{}_{}_bk_metric.txt'.format(self.output_dir, self.ver, self.name), 'w')
                json.dump(self.bk_metric, bk_metric_file)
                bk_metric_file.close()

            # elif not math.isnan(f1_score_metrics_eval):
            #     self.model.stop_training = True
            #     self.bk_metric["stop_train"] = True
            #     bk_metric_file = open('{}/{}_{}_bk_metric.txt'.format(self.output_dir, self.ver, self.name), 'w')
            #     json.dump(self.bk_metric, bk_metric_file)
            #     bk_metric_file.close()
            #     sys.stdout.flush()
            #     with open(self.output_dir + '/{}_{}_log.csv'.format(self.ver, self.name), mode='a+') as report_file:
            #         report_writer = csv.DictWriter(report_file, fieldnames=self.fieldnames)
            #         report_writer.writerow(report_row)

            # elif not math.isnan(squared_error_metrics_eval):
            #     self.model.stop_training = True
            #     self.bk_metric["stop_train"] = True
            #     bk_metric_file = open('{}/{}_{}_bk_metric.txt'.format(self.output_dir, self.ver, self.name), 'w')
            #     json.dump(self.bk_metric, bk_metric_file)
            #     bk_metric_file.close()
            #     sys.stdout.flush()
            #     with open(self.output_dir + '/{}_{}_log.csv'.format(self.ver, self.name), mode='a+') as report_file:
            #         report_writer = csv.DictWriter(report_file, fieldnames=self.fieldnames)
            #         report_writer.writerow(report_row)

            sys.stdout.flush()
            # endregion Save model

        with open(self.output_dir + '/{}_{}_log.csv'.format(self.ver, self.name), mode='a+') as report_file:
            report_writer = csv.DictWriter(report_file, fieldnames=self.fieldnames)
            report_writer.writerow(report_row)


def train_beat(use_gpu_index,
               name,
               ver,
               output_dir,
               model_dir,
               lbl_train,
               gamma,
               class_weights,
               resume_from,
               train_directory,
               eval_directory,
               batch_size,
               epoch_num):
    """

    :param use_gpu_index:
    :param name:
    :param ver:
    :param output_dir:
    :param model_dir:
    :param lbl_train:
    :param gamma:
    :param class_weights:
    :param resume_from:
    :param train_directory:
    :param eval_directory:
    :param batch_size:
    :param epoch_num:
    :return:
    """
    print('model_dir: {}\n'.format(model_dir))

    last_checkpoint_dir = model_dir + '/last'
    best_ozsun_checkpoint_dir = model_dir + '/best_new_metric'
    best_loss_checkpoint_dir = model_dir + '/best_loss'
    best_f1_checkpoint_dir = model_dir + '/best_new_f1'
    for i in [last_checkpoint_dir, best_ozsun_checkpoint_dir,
              best_loss_checkpoint_dir, best_f1_checkpoint_dir]:
        if not os.path.exists(i):
            os.makedirs(i)

    with open('{}/num_samples.txt'.format(output_dir), 'r') as json_file:
        num_samples = json.load(json_file)

    bk_metric = None
    if os.path.exists('{}/{}_{}_bk_metric.txt'.format(output_dir, ver, name)):
        with open('{}/{}_{}_bk_metric.txt'.format(output_dir, ver, name), 'r') as json_file:
            bk_metric = json.load(json_file)
            try:
                if bk_metric["stop_train"]:
                    return True
            except:
                bk_metric["stop_train"] = False

    if class_weights is None:
        class_weights = np.zeros(len(lbl_train.keys()))
        total_train = num_samples['train']['total']
        for i, k in enumerate(lbl_train.keys()):
            class_weights[i] = (1.0 / num_samples['train'][k]) * total_train / len(lbl_train.keys())

    class_weights = np.asarray(class_weights)

    fieldnames = ['epoch',
                  'accuracy_train', 'loss_train', 'precision_train', 'recall_train',
                  'accuracy_eval', 'loss_eval', 'precision_eval', 'recall_eval',
                  'squared_error_metrics_train', 'squared_error_metrics_eval',
                  'f1_score_metrics_train', 'f1_score_metrics_eval']

    if not os.path.exists(output_dir + '/{}_{}_log.csv'.format(ver, name)):
        with open(output_dir + '/{}_{}_log.csv'.format(ver, name), mode='a+') as report_file:
            report_writer = csv.DictWriter(report_file, fieldnames=fieldnames)
            report_writer.writeheader()

    log_train = TextLogging(output_dir + '/{}_{}_training_log.txt'.format(ver, name), 'a+')

    log_train.write_mylines('Begin : {}\n'.format(str(datetime.datetime.now())))
    log_train.write_mylines('Batch size : {}\n'.format(batch_size))
    log_train.write_mylines('Epoch : {}\n'.format(epoch_num))
    log_train.write_mylines('Class weights : {}\n'.format(class_weights))
    log_train.write_mylines('Dataset size : {}\n'.format(num_samples))

    gpu_devices = tf.config.experimental.list_physical_devices('GPU')
    if len(gpu_devices) > 0:
        tf.config.experimental.set_memory_growth(gpu_devices[use_gpu_index], True)

    with tf.device("/cpu:0"):
        # Put data pipeline on CPU for performance, per:
        # https://www.tensorflow.org/guide/performance/overview#preprocessing_on_the_cpu
        train_dataset = load_tfrecords(
            dir_path=train_directory,
            batch_size=batch_size,
            is_training=True
        )
        val_dataset = load_tfrecords(
            dir_path=eval_directory,
            batch_size=batch_size
        )
    train_model = configure_model(
        name=name,
        # class_weights=class_weights,
        gamma=float(gamma),
        learning_rate=1e-3,
        label=lbl_train
    )
    valid_freq = 3
    if resume_from is not None:
        begin_at_epoch = int(resume_from.split("-")[-1])
        log_train.write_mylines("Restoring checkpoint from {}\n".format(resume_from))
        log_train.write_mylines("Beginning at epoch {}\n".format(begin_at_epoch + 1))
        print("Restoring checkpoint from {}".format(resume_from))
        print("Beginning at epoch {}".format(begin_at_epoch + 1))
        train_model.load_weights(tf.train.latest_checkpoint(last_checkpoint_dir)).expect_partial()
    else:
        begin_at_epoch = 0
        print("===============================================================================")
        print("WARNING: --resume_from checkpoint flag is not set. Training model from scratch.")
        print("===============================================================================")

    if bk_metric is None:
        best_loss = -1
        best_squared_error_metrics = -1
        best_f1_score_metrics = -1
        bk_metric = dict()
        bk_metric["best_loss"] = -1
        bk_metric["best_squared_error_metrics"] = -1
        bk_metric["best_f1_score_metrics"] = -1
        bk_metric["stop_train"] = False
        bk_metric_file = open('{}/{}_{}_bk_metric.txt'.format(output_dir, ver, name), 'w')
        json.dump(bk_metric, bk_metric_file)
        bk_metric_file.close()
    else:
        best_loss = bk_metric["best_loss"]
        best_squared_error_metrics = bk_metric["best_squared_error_metrics"]
        best_f1_score_metrics = bk_metric["best_f1_score_metrics"]

    tensorboard_dir = model_dir + '/logs'
    if not os.path.exists(tensorboard_dir):
        os.makedirs(tensorboard_dir)
        os.makedirs(tensorboard_dir + '/train')
        os.makedirs(tensorboard_dir + '/validation')

    log_callback = CustomCallback(name=name,
                                  log_train=log_train,
                                  stopped_epoch=begin_at_epoch + epoch_num,
                                  last_checkpoint_dir=last_checkpoint_dir,
                                  best_loss_checkpoint_dir=best_loss_checkpoint_dir,
                                  best_ozsun_checkpoint_dir=best_ozsun_checkpoint_dir,
                                  best_f1_checkpoint_dir=best_f1_checkpoint_dir,
                                  bk_metric=bk_metric,
                                  lbl_train=lbl_train,
                                  output_dir=output_dir,
                                  ver=ver,
                                  field_name=fieldnames,
                                  best_loss=best_loss,
                                  best_squared_error_metrics=best_squared_error_metrics,
                                  best_f1_score_metrics=best_f1_score_metrics,
                                  length_train=_calc_num_steps(num_samples['train']['total_sample'], batch_size),
                                  length_valid=_calc_num_steps(num_samples['eval']['total_sample'], batch_size),
                                  valid_freq=valid_freq,
                                  tensorboard_dir=tensorboard_dir
                                  )

    with tf.device('/gpu:{}'.format(use_gpu_index if use_gpu_index >= 0 else 0)):
        train_model.fit(x=train_dataset,
                        epochs=begin_at_epoch + epoch_num,
                        verbose=0,
                        callbacks=[log_callback, tf.keras.callbacks.TensorBoard(log_dir=tensorboard_dir)],
                        validation_data=val_dataset,
                        validation_freq=[valid_freq * (x + 1) for x in
                                         range((begin_at_epoch + epoch_num) // valid_freq)],
                        initial_epoch=begin_at_epoch,
                        )

    bk_metric["stop_train"] = True
    bk_metric_file = open('{}/{}_{}_bk_metric.txt'.format(output_dir, ver, name), 'w')
    json.dump(bk_metric, bk_metric_file)
    bk_metric_file.close()

    log_train.write_mylines('\nEnd : {}\n'.format(str(datetime.datetime.now())))
    # report_file.close()
    return False


def process_run_qrs_beat_noise(file_list,
                               path2model_noise,
                               path2model_beat,
                               use_gpu_index,
                               beat_ext_ai,
                               write_mit_annotation,
                               write_hes_annotation,
                               channel=0):
    """

    :param file_list:
    :param path2model_noise:
    :param path2model_beat:
    :param use_gpu_index:
    :param beat_ext_ai:
    :param write_mit_annotation:
    :param write_hes_annotation:
    :param channel:
    :return:
    """
    log_lines = []

    for file in file_list:
        dir_test = os.path.dirname(file)
        try:
            if basename(file) == '114':
                channel_ecg = 1
            else:
                channel_ecg = channel

            start_time = time.time()
            if path2model_noise is not None:
                fs_origin, total_peak, total_noise, total_symbol = run_qrs_beat_noise(
                    check_point_noise=path2model_noise,
                    check_point_beat=path2model_beat,
                    file_test=file,
                    channel_ecg=channel_ecg,
                    use_gpu_index=use_gpu_index)
                total_symbol = ['~' if total_noise[i] > 0 else total_symbol[i] for i in range(len(total_peak))]
            else:
                fs_origin, total_peak, total_symbol = run_qrs_beat(check_point_beat=path2model_beat,
                                                                   file_test=file,
                                                                   channel_ecg=channel_ecg,
                                                                   use_gpu_index=use_gpu_index)

            end_time = time.time()
            str_log = '{} with {} beats take {} seconds\n'.format(
                basename(dirname(file)) + '/' + basename(file),
                len(total_peak),
                end_time - start_time)
            print(str_log)
            log_lines.append(str_log)

            if write_mit_annotation:
                curr_dir = os.getcwd()
                os.chdir(dir_test + '/')
                annotation2 = wf.Annotation(record_name=basename(file),
                                            extension=beat_ext_ai,
                                            sample=np.asarray(total_peak),
                                            symbol=np.asarray(total_symbol),
                                            fs=fs_origin)
                annotation2.wrann(write_fs=True)
                os.chdir(curr_dir)

            if write_hes_annotation:
                try:
                    file_name = os.path.basename(file)
                    utc_offset_of_study = 60 * 60 * int(file_name[-3:]) // 4
                    if "data-all-" in file_name:
                        start_time_of_study = int(
                            datetime.datetime.strptime(file_name
                                                       [len("data-all-"):-3], '%m-%d-%y-%H-%M-%S').timestamp())
                    elif "event-auto-" in file_name:
                        header = wf.rdheader(file)
                        time_from_header = int(header.base_datetime.timestamp())
                        time_from_name = int(
                            datetime.datetime.strptime(file_name[len("event-auto-"):-3],
                                                       '%m-%d-%y-%H-%M-%S').timestamp())
                        if abs(time_from_name - time_from_header) > abs(utc_offset_of_study / 2):
                            start_time_of_study = time_from_header + utc_offset_of_study
                        else:
                            start_time_of_study = time_from_header

                    elif "event-manual-" in file_name:
                        header = wf.rdheader(file)
                        time_from_header = int(header.base_datetime.timestamp())
                        time_from_name = int(
                            datetime.datetime.strptime(file_name[len("event-manual-"):-3],
                                                       '%m-%d-%y-%H-%M-%S').timestamp())
                        if abs(time_from_name - time_from_header) > abs(utc_offset_of_study / 2):
                            start_time_of_study = time_from_header + utc_offset_of_study
                        else:
                            start_time_of_study = time_from_header

                    else:
                        start_time_of_study = 1577811600
                        utc_offset_of_study = 0

                except Exception as err:
                    print(err)
                    start_time_of_study = 0
                    utc_offset_of_study = 0

                study_time_utc = start_time_of_study - utc_offset_of_study
                pause_detection = PauseEvents(3000)
                tb_detection = TBEvents(110, 40)
                index = -1

                list_beat = []
                list_symbol = []
                list_event = []
                list_sv_event1 = []
                list_sv_event2 = []
                list_sv_offset1 = []
                list_sv_offset2 = []
                list_sv_duration1 = []
                list_sv_duration2 = []

                len_afib_event = 0
                len_tachy_event = 0
                len_brady_event = 0
                len_pause_event = 0

                for peak, symbol in zip(total_peak, total_symbol):
                    hes_peak = int(peak * data_model.HES_SAMPLING_RATE / fs_origin)
                    mark = 0
                    if symbol == 'N':
                        sym = '01'
                    elif symbol == 'V':
                        sym = '60'
                    elif symbol == 'A':
                        sym = '70'
                    elif symbol == '|':
                        sym = '80'
                    else:
                        sym = '01'

                    af = 0

                    af_pause = af + max(pause_detection.pause_detect(hes_peak, mark), 0)
                    noise_tb = mark + max(tb_detection.tb_detect(hes_peak, mark), 0)

                    event = '0x0{}0{}'.format(str(af_pause), str(noise_tb))
                    event = int(event, 0)
                    is_last_beat = (index == len(total_peak) - 1)
                    list_event, len_tachy_event, len_brady_event, len_afib_event, len_pause_event = \
                        remove_short_event(list_event,
                                           event,
                                           len_afib_event,
                                           len_brady_event,
                                           len_tachy_event,
                                           len_pause_event,
                                           is_last_beat)

                    list_sv_event1.append(0)
                    list_sv_event2.append(0)
                    list_sv_offset1.append(0)
                    list_sv_offset2.append(0)
                    list_sv_duration1.append(0)
                    list_sv_duration2.append(0)

                    list_beat.append(int(peak))
                    list_symbol.append(int(sym))

                ain_f = 'Time\t' + \
                        'SampleCnt\t' + \
                        'DetectedBeatSample\t' + \
                        'HesBeatStatus\t' + \
                        'HesEventSampleDiff\t' + \
                        'HesEventStatus\t' + \
                        'HesEventStatus2\t' + \
                        'HesMeanRrInSamples\t' + \
                        'HeartRate\t' + \
                        'svEvent1\t' + \
                        'svOffset1\t' + \
                        'svDuration1\t' + \
                        'svEvent2\t' + \
                        'svOffset2\t' + \
                        'svDuration2\t' + \
                        'AccX\t' + \
                        'AccY\t' + \
                        'AccZ\n'

                ain_f += datetime.datetime.fromtimestamp(study_time_utc).strftime('%y%m%d:%H%M%S') + '\t' \
                         + str(0) + '\t' \
                         + str(0) + '\t' \
                         + '00\t0\t0x0000\t0x0000\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\n'

                for peak, symbol, event, sv_event1, sv_event2, sv_offset1, sv_offset2, sv_duration1, sv_duration2 in zip(
                        list_beat,
                        list_symbol,
                        list_event,
                        list_sv_event1,
                        list_sv_event2,
                        list_sv_offset1,
                        list_sv_offset2,
                        list_sv_duration1,
                        list_sv_duration2):
                    hes_peak = int(peak * data_model.HES_SAMPLING_RATE / fs_origin)
                    epoch_line = study_time_utc + (peak * 1 / fs_origin)
                    ain_f += datetime.datetime.fromtimestamp(epoch_line).strftime('%y%m%d:%H%M%S') + '\t' \
                             + str(hes_peak) + '\t' \
                             + str(hes_peak) + '\t' \
                             + '{}'.format(symbol) \
                             + '\t0\t0x{:04x}\t0x0000\t0\t0'.format(event) \
                             + '\t{}\t{}\t{}\t{}\t{}\t{}\t0\t0\t0\n'.format(str(sv_event1), str(sv_offset1),
                                                                            str(sv_duration1),
                                                                            str(sv_event2), str(sv_offset2),
                                                                            str(sv_duration2))

                ain_f += 'End\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\tEnd\n'
                ain_f += 'tachylimitbpm= {}; bradylimitbpm= {}; pause_thresh= {}; artifact_run= {}\n'.format(
                    110,
                    40,
                    3000,
                    False)
                ain_f += 'channel= {};'.format(channel_ecg)
                hes_file = open(file + "." + beat_ext_ai, "w")
                hes_file.write(ain_f)
                hes_file.close()

        except Exception as e:
            print(e)
            pass

    return log_lines


def eval_beat(use_gpu_index,
              path2model_noise,
              path2model_beat,
              output_dir,
              test_ec57_dir,
              output_ec57_directory,
              physionet_directory,
              num_of_process):
    """

    :param use_gpu_index:
    :param path2model_noise:
    :param path2model_beat:
    :param output_dir:
    :param test_ec57_dir:
    :param output_ec57_directory:
    :param physionet_directory:
    :param num_of_process:
    :return:
    """

    gpu_devices = tf.config.experimental.list_physical_devices('GPU')
    if len(gpu_devices) > 0:
        tf.config.experimental.set_memory_growth(gpu_devices[use_gpu_index], True)

    if os.path.exists(output_ec57_directory + '/finish.txt'):
        return

    ds_file = sorted(glob(output_dir + '/ds*.txt'))
    if not os.path.isdir(output_ec57_directory):
        os.makedirs(output_ec57_directory)

    fstatus = open(output_ec57_directory + '/start.txt', 'w')
    fstatus.writelines(str(datetime.datetime.now()))
    fstatus.close()

    if path2model_noise is not None:
        str_version = path2model_noise.split('/')
        for i, v in enumerate(str_version):
            if v == "checkpoint":
                break

        noise_ext_ai = str_version[i - 1]
        model_noise_ext_ai = str_version[i + 1]

        replacements = dict()
        for m in range(26):
            replacements[str(m)] = str(chr(m + 97))

        for m in range(26, 52):
            replacements[str(m)] = str(chr(m - 26 + 65))

        for m in range(52, 78):
            replacements[str(m)] = 'a'

        for m in range(78, 104):
            replacements[str(m)] = 'b'

        for m in range(104, 130):
            replacements[str(m)] = 'c'

        for m in range(130, 156):
            replacements[str(m)] = 'd'

        for m in range(156, 182):
            replacements[str(m)] = 'e'

        for m in range(182, 208):
            replacements[str(m)] = 'f'

        for m in range(208, 2500):
            replacements[str(m)] = 'g'

        noise_ext_ai = noise_ext_ai.replace("noise", "")
        noise_ext_ai = noise_ext_ai.replace("subject", "")

        noise_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], noise_ext_ai)
        noise_ext_ai = noise_ext_ai.replace('.', '')
        noise_ext_ai = noise_ext_ai.replace('-', '')
        model_noise_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], model_noise_ext_ai)
        model_noise_ext_ai = model_noise_ext_ai.replace('.', '')
        model_noise_ext_ai = model_noise_ext_ai.replace('-', '')

        if 'best_loss' in path2model_noise:
            noise_ext_ai += model_noise_ext_ai + 'l'
        else:
            noise_ext_ai += model_noise_ext_ai + 'm'

    else:
        noise_ext_ai = ""

    str_version = path2model_beat.split('/')
    for i, v in enumerate(str_version):
        if v == "checkpoint":
            break

    beat_ext_ai = str_version[i - 1]
    model_beat_ext_ai = str_version[i + 1]

    replacements = dict()
    for m in range(26):
        replacements[str(m)] = str(chr(m + 97))

    for m in range(26, 52):
        replacements[str(m)] = str(chr(m - 26 + 65))

    for m in range(52, 78):
        replacements[str(m)] = 'a'

    for m in range(78, 104):
        replacements[str(m)] = 'b'

    for m in range(104, 130):
        replacements[str(m)] = 'c'

    for m in range(130, 156):
        replacements[str(m)] = 'd'

    for m in range(156, 182):
        replacements[str(m)] = 'e'

    for m in range(182, 208):
        replacements[str(m)] = 'f'

    for m in range(208, 2500):
        replacements[str(m)] = 'g'

    beat_ext_ai = beat_ext_ai.replace("beat", "")
    beat_ext_ai = beat_ext_ai.replace("subject", "")

    beat_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], beat_ext_ai)
    beat_ext_ai = beat_ext_ai.replace('.', '')
    beat_ext_ai = beat_ext_ai.replace('-', '')
    beat_ext_ai = beat_ext_ai.replace('_', '')
    model_beat_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], model_beat_ext_ai)
    model_beat_ext_ai = model_beat_ext_ai.replace('.', '')
    model_beat_ext_ai = model_beat_ext_ai.replace('-', '')
    model_beat_ext_ai = model_beat_ext_ai.replace('_', '')

    if 'best_loss' in path2model_beat:
        beat_ext_ai += model_beat_ext_ai + 'l'
    else:
        beat_ext_ai += model_beat_ext_ai + 'm'

    ext_ai = noise_ext_ai + beat_ext_ai
    ext_ai = ''.join(sorted(set(ext_ai), key=ext_ai.index))
    ext_ai += 'atr'

    with open(output_ec57_directory + '/eval_timing.txt', 'w') as log_eval:
        for db in test_ec57_dir:
            ds_eval = []
            if len(ds_file) > 1:
                with open(ds_file[-1], 'rt')as f:
                    content = f.readlines()
                    for row in content:
                        if basename(dirname(re.split('\t\t\t\t', row)[0])) == db[0]:
                            ds_eval.append(basename(re.split('\t\t\t\t', row)[0]))

            del_result(db[0], physionet_directory, output_ec57_directory)
            path2db = physionet_directory + db[0]

            file_names = glob(path2db + '/*.dat')
            # Get rid of the extension
            file_names = [p[:-4] for p in file_names
                          if basename(p)[:-4] not in ['104', '102', '107', '217', 'bw', 'em', 'ma']
                          if '_200hz' not in basename(p)[:-4]]

            file_names = sorted(file_names)

            num_file_each_process = int(len(file_names) / num_of_process)
            file_process_split = [file_names[x:x + num_file_each_process] for x in range(0, len(file_names),
                                                                                         num_file_each_process)]
            arg_list = list()
            for file_list in file_process_split:
                arg = (file_list, path2model_noise, path2model_beat, use_gpu_index, ext_ai, True, False)
                arg_list.append(arg)

            process_start_time = time.time()
            with Pool(processes=num_of_process) as pool:
                # print same numbers in arbitrary order
                for log_lines in pool.starmap(process_run_qrs_beat_noise, arg_list):
                    log_eval.writelines(log_lines)

            process_end_time = time.time()
            str_log = 'multiprocess {} take {} seconds\n'.format(basename(os.path.dirname(file_names[0])),
                                                                 process_end_time - process_start_time)
            print(str_log)

            # EC57 Eval-Full db
            ec57_eval(db[0],
                      output_ec57_directory,
                      physionet_directory,
                      db[1],
                      db[2],
                      ext_ai,
                      None)

            clone_result(ext_ai, '/'.join([physionet_directory, db[0]]),
                         '/'.join([output_ec57_directory, db[0], 'Annotation']))
            # del_result(db[0], physionet_directory, output_ec57_directory)

    fstatus = open(output_ec57_directory + '/finish.txt', 'w')
    fstatus.writelines(str(datetime.datetime.now()))
    fstatus.close()
    return ext_ai


def check_studies(use_gpu_index,
                  path2model_noise,
                  path2model_beat,
                  path2portal_data,
                  study_ids,
                  num_of_process):
    """

    :param use_gpu_index:
    :param path2model_noise:
    :param path2model_beat:
    :param path2portal_data:
    :param study_ids:
    :param num_of_process:
    :return:
    """

    if path2model_noise is not None:
        str_version = path2model_noise.split('/')
        for i, v in enumerate(str_version):
            if v == "checkpoint":
                break

        noise_ext_ai = str_version[i - 1]
        model_noise_ext_ai = str_version[i + 1]

        replacements = dict()
        for m in range(26):
            replacements[str(m)] = str(chr(m + 97))

        for m in range(26, 52):
            replacements[str(m)] = str(chr(m - 26 + 65))

        for m in range(52, 78):
            replacements[str(m)] = 'a'

        for m in range(78, 104):
            replacements[str(m)] = 'b'

        for m in range(104, 130):
            replacements[str(m)] = 'c'

        for m in range(130, 156):
            replacements[str(m)] = 'd'

        for m in range(156, 182):
            replacements[str(m)] = 'e'

        for m in range(182, 208):
            replacements[str(m)] = 'f'

        for m in range(208, 2500):
            replacements[str(m)] = 'g'

        noise_ext_ai = noise_ext_ai.replace("noise", "")
        noise_ext_ai = noise_ext_ai.replace("subject", "")

        noise_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], noise_ext_ai)
        noise_ext_ai = noise_ext_ai.replace('.', '')
        noise_ext_ai = noise_ext_ai.replace('-', '')
        model_noise_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], model_noise_ext_ai)
        model_noise_ext_ai = model_noise_ext_ai.replace('.', '')
        model_noise_ext_ai = model_noise_ext_ai.replace('-', '')

        if 'best_loss' in path2model_noise:
            noise_ext_ai += model_noise_ext_ai + 'l'
        else:
            noise_ext_ai += model_noise_ext_ai + 'm'

    else:
        noise_ext_ai = ""

    str_version = path2model_beat.split('/')
    for i, v in enumerate(str_version):
        if v == "checkpoint":
            break

    beat_ext_ai = str_version[i - 1]
    model_beat_ext_ai = str_version[i + 1]

    replacements = dict()
    for m in range(26):
        replacements[str(m)] = str(chr(m + 97))

    for m in range(26, 52):
        replacements[str(m)] = str(chr(m - 26 + 65))

    for m in range(52, 78):
        replacements[str(m)] = 'a'

    for m in range(78, 104):
        replacements[str(m)] = 'b'

    for m in range(104, 130):
        replacements[str(m)] = 'c'

    for m in range(130, 156):
        replacements[str(m)] = 'd'

    for m in range(156, 182):
        replacements[str(m)] = 'e'

    for m in range(182, 208):
        replacements[str(m)] = 'f'

    for m in range(208, 2500):
        replacements[str(m)] = 'g'

    beat_ext_ai = beat_ext_ai.replace("beat", "")
    beat_ext_ai = beat_ext_ai.replace("subject", "")

    beat_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], beat_ext_ai)
    beat_ext_ai = beat_ext_ai.replace('.', '')
    beat_ext_ai = beat_ext_ai.replace('-', '')
    beat_ext_ai = beat_ext_ai.replace('_', '')
    model_beat_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], model_beat_ext_ai)
    model_beat_ext_ai = model_beat_ext_ai.replace('.', '')
    model_beat_ext_ai = model_beat_ext_ai.replace('-', '')
    model_beat_ext_ai = model_beat_ext_ai.replace('_', '')

    if 'best_loss' in path2model_beat:
        beat_ext_ai += model_beat_ext_ai + 'l'
    else:
        beat_ext_ai += model_beat_ext_ai + 'm'

    ext_ai = noise_ext_ai + beat_ext_ai
    ext_ai = ''.join(sorted(set(ext_ai), key=ext_ai.index))

    def get_files(dir_name):
        """"
        """
        list_file = os.listdir(dir_name)
        complete_file_list = list()
        for file in list_file:
            complete_path = os.path.join(dir_name, file)
            if os.path.isdir(complete_path):
                complete_file_list = complete_file_list + get_files(complete_path)
            else:
                complete_file_list.append(complete_path)

        return complete_file_list

    if study_ids is not None:
        file_names = [fn[:-4] for fn in get_files(path2portal_data)
                      if "data-all-" in basename(fn)
                      if ".dat" in basename(fn)
                      if basename(dirname(fn)) in study_ids]
    else:
        file_names = [fn[:-4] for fn in get_files(path2portal_data)
                      if "data-all-" in basename(fn)
                      if ".dat" in basename(fn)]

    file_names = sorted(file_names)

    num_file_each_process = int(len(file_names) / num_of_process)
    file_process_split = [file_names[x:x + num_file_each_process] for x in range(0, len(file_names),
                                                                                 num_file_each_process)]
    arg_list = list()
    for file_list in file_process_split:
        arg = (file_list, path2model_noise, path2model_beat, use_gpu_index, ext_ai, False, True, FLAGS.beat_channel_num)
        arg_list.append(arg)

    with Pool(processes=num_of_process) as pool:
        for _ in pool.starmap(process_run_qrs_beat_noise, arg_list):
            pass

    # return ain_ext
    return ext_ai


def check_events(use_gpu_index,
                 path2model_noise,
                 path2model_beat,
                 path2portal_data,
                 event_ids,
                 num_of_process,
                 channel=-1):
    """

    :param channel:
    :param use_gpu_index:
    :param path2model_noise:
    :param path2model_beat:
    :param path2portal_data:
    :param event_ids:
    :param num_of_process:
    :return:
    """

    if path2model_noise is not None:
        str_version = path2model_noise.split('/')
        for i, v in enumerate(str_version):
            if v == "checkpoint":
                break

        noise_ext_ai = str_version[i - 1]
        model_noise_ext_ai = str_version[i + 1]

        replacements = dict()
        for m in range(26):
            replacements[str(m)] = str(chr(m + 97))

        for m in range(26, 52):
            replacements[str(m)] = str(chr(m - 26 + 65))

        for m in range(52, 78):
            replacements[str(m)] = 'a'

        for m in range(78, 104):
            replacements[str(m)] = 'b'

        for m in range(104, 130):
            replacements[str(m)] = 'c'

        for m in range(130, 156):
            replacements[str(m)] = 'd'

        for m in range(156, 182):
            replacements[str(m)] = 'e'

        for m in range(182, 208):
            replacements[str(m)] = 'f'

        for m in range(208, 2500):
            replacements[str(m)] = 'g'

        noise_ext_ai = noise_ext_ai.replace("noise", "")
        noise_ext_ai = noise_ext_ai.replace("subject", "")

        noise_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], noise_ext_ai)
        noise_ext_ai = noise_ext_ai.replace('.', '')
        noise_ext_ai = noise_ext_ai.replace('-', '')
        model_noise_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], model_noise_ext_ai)
        model_noise_ext_ai = model_noise_ext_ai.replace('.', '')
        model_noise_ext_ai = model_noise_ext_ai.replace('-', '')

        if 'best_loss' in path2model_noise:
            noise_ext_ai += model_noise_ext_ai + 'l'
        else:
            noise_ext_ai += model_noise_ext_ai + 'm'

    else:
        noise_ext_ai = ""

    str_version = path2model_beat.split('/')
    for i, v in enumerate(str_version):
        if v == "checkpoint":
            break

    beat_ext_ai = str_version[i - 1]
    model_beat_ext_ai = str_version[i + 1]

    replacements = dict()
    for m in range(26):
        replacements[str(m)] = str(chr(m + 97))

    for m in range(26, 52):
        replacements[str(m)] = str(chr(m - 26 + 65))

    for m in range(52, 78):
        replacements[str(m)] = 'a'

    for m in range(78, 104):
        replacements[str(m)] = 'b'

    for m in range(104, 130):
        replacements[str(m)] = 'c'

    for m in range(130, 156):
        replacements[str(m)] = 'd'

    for m in range(156, 182):
        replacements[str(m)] = 'e'

    for m in range(182, 208):
        replacements[str(m)] = 'f'

    for m in range(208, 2500):
        replacements[str(m)] = 'g'

    beat_ext_ai = beat_ext_ai.replace("beat", "")
    beat_ext_ai = beat_ext_ai.replace("subject", "")

    beat_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], beat_ext_ai)
    beat_ext_ai = beat_ext_ai.replace('.', '')
    beat_ext_ai = beat_ext_ai.replace('-', '')
    beat_ext_ai = beat_ext_ai.replace('_', '')
    model_beat_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], model_beat_ext_ai)
    model_beat_ext_ai = model_beat_ext_ai.replace('.', '')
    model_beat_ext_ai = model_beat_ext_ai.replace('-', '')
    model_beat_ext_ai = model_beat_ext_ai.replace('_', '')

    if 'best_loss' in path2model_beat:
        beat_ext_ai += model_beat_ext_ai + 'l'
    else:
        beat_ext_ai += model_beat_ext_ai + 'm'

    ext_ai = noise_ext_ai + beat_ext_ai
    ext_ai = ''.join(sorted(set(ext_ai), key=ext_ai.index))

    def get_files(dir_name):
        """"
        """
        list_file = os.listdir(dir_name)
        complete_file_list = list()
        for file in list_file:
            complete_path = os.path.join(dir_name, file)
            if os.path.isdir(complete_path):
                complete_file_list = complete_file_list + get_files(complete_path)
            else:
                complete_file_list.append(complete_path)

        return complete_file_list

    if event_ids is not None:
        file_names = [fn[:-4] for fn in get_files(path2portal_data)
                      if "event-" in basename(fn)
                      if ".dat" in basename(fn)
                      if basename(dirname(fn)) in event_ids]
    else:
        file_names = [fn[:-4] for fn in get_files(path2portal_data)
                      if "event-" in basename(fn)
                      if ".dat" in basename(fn)]

    file_names = sorted(file_names)

    num_file_each_process = int(len(file_names) / num_of_process)
    if num_file_each_process > 1:
        file_process_split = [file_names[x:x + num_file_each_process] for x in range(0, len(file_names),
                                                                                     num_file_each_process)]
    else:
        file_process_split = [file_names]

    if 0 < channel < 3:
        arg_list = list()
        for file_list in file_process_split:
            arg = (file_list, path2model_noise, path2model_beat, use_gpu_index, ext_ai + ain_ext[channel], False, True,
                   channel)
            arg_list.append(arg)

        with Pool(processes=num_of_process) as pool:
            for _ in pool.starmap(process_run_qrs_beat_noise, arg_list):
                pass
    else:
        for channel in range(3):
            arg_list = list()
            for file_list in file_process_split:
                arg = (
                    file_list, path2model_noise, path2model_beat, use_gpu_index, ext_ai + ain_ext[channel], False, True,
                    channel)
                arg_list.append(arg)

            with Pool(processes=num_of_process) as pool:
                for _ in pool.starmap(process_run_qrs_beat_noise, arg_list):
                    pass

    return ext_ai


from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc
import grpc
# from utils.docker_config import *


def run_qrs_beat(check_point_beat,
                 file_test,
                 channel_ecg=0,
                 use_gpu_index=0):
    """
    :param check_point_beat:
    :param file_test:
    :param channel_ecg:
    :param use_gpu_index:
    :return:
    """
    total_peak = []
    total_symbol = []
    fs_origin = 0
    label_arrhythmia = data_model.LABEL_ARRHYTHMIA[FLAGS.beat_class]

    # tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
    # host = "{0}:{1}".format(DEFAULT_TF_SERVER_NAME, DEFAULT_TF_SERVER_PORT)
    # channel_stub = grpc.insecure_channel(host)

    # stub = prediction_service_pb2_grpc.PredictionServiceStub(channel_stub)
    # request_predict_image = predict_pb2.PredictRequest()

    # request_predict_image.model_spec.name = IMAGE_MODEL_NAME
    # request_predict_image.model_spec.signature_name = IMAGE_PREDICTION_SIGNATURE

    # def return_prediction_beat(logits, axis=2, threshold=0.7):
    #     softmax = tf.nn.softmax(logits, axis=axis)
    #     predictions_index = tf.argmax(softmax, axis=axis)
    #     predictions_index = tf.where(tf.greater(tf.math.reduce_max(softmax, axis=axis), threshold) |
    #                                  ~ tf.equal(predictions_index, 0),
    #                                  predictions_index, tf.ones_like(predictions_index) * FLAGS.beat_class_num)
    #     return predictions_index

    with tf.device('/gpu:{}'.format(use_gpu_index if use_gpu_index >= 0 else 0)):
        str_version = check_point_beat.split('/')
        for i, v in enumerate(str_version):
            if v == "checkpoint":
                break

        model_name_beat = str_version[i + 1]
        eval_model = model.build(name=model_name_beat,
                                 is_training=True,
                                 inputs=FLAGS.beat_feature_len,
                                 num_of_class=FLAGS.beat_class_num,
                                 feature_len=FLAGS.beat_feature_len)
        eval_model.load_weights(tf.train.latest_checkpoint(check_point_beat)).expect_partial()
    try:
        header = wf.rdheader(file_test)
        samp_to = 0
        samp_from = 0
        fs_origin = header.fs

        while samp_to <= header.sig_len:
            try:
                samp_len = min(int(data_model.MAX_SAMPLE_PROCESS * fs_origin), (header.sig_len - samp_from))

                if samp_len <= 0:
                    break

                samp_to = samp_from + samp_len
                record = wf.rdsamp(file_test, sampfrom=samp_from, sampto=samp_to, channels=[channel_ecg])
                buf_record = np.nan_to_num(record[0][:, 0])
                fs_origin = record[1].get('fs')
                if fs_origin != data_model.SAMPLING_RATE:
                    buf_ecg, _ = resample_sig(buf_record, fs_origin, data_model.SAMPLING_RATE)
                else:
                    buf_ecg = buf_record.copy()

                buf_ecg = butter_lowpass_filter(buf_ecg, 40, data_model.SAMPLING_RATE)

                if FLAGS.beat_bwr == 1:
                    buf_ecg_beat = bwr(buf_ecg, data_model.SAMPLING_RATE)
                    if FLAGS.beat_norm:
                        buf_ecg_beat = norm(buf_ecg_beat, data_model.NUM_NORMALIZATION)
                elif FLAGS.beat_bwr == 2:
                    buf_ecg_beat = bwr_dwt(buf_ecg, data_model.SAMPLING_RATE)
                    if FLAGS.beat_norm:
                        buf_ecg_beat = norm(buf_ecg_beat, data_model.NUM_NORMALIZATION)
                elif FLAGS.beat_bwr == 3:
                    buf_ecg_beat = bwr_dwt2(buf_ecg, data_model.SAMPLING_RATE)
                    if FLAGS.beat_norm:
                        buf_ecg_beat = norm(buf_ecg_beat, data_model.NUM_NORMALIZATION)
                else:
                    buf_ecg_beat = buf_ecg.copy()

                # import matplotlib.pyplot as plt
                # plt.plot(buf_ecg_beat, label="beat_norm")
                # plt.plot(buf_ecg, label="beat_origin")
                # plt.legend(loc=4)
                # plt.show()

                # region BEATNET
                try:
                    sig_len = len(buf_ecg_beat)
                    if FLAGS.beat_overlap == 0:
                        sig_len = sig_len // FLAGS.beat_feature_len * FLAGS.beat_feature_len
                        beat_label_len = FLAGS.beat_feature_len // FLAGS.beat_block_len
                        frame_index = np.arange(FLAGS.beat_feature_len)[None, :] + \
                                      np.arange(0, sig_len, FLAGS.beat_feature_len)[:, None]

                        label_index = np.arange(beat_label_len)[None, None, :] + \
                                      np.arange(0, FLAGS.beat_feature_len, beat_label_len)[None, :, None] + \
                                      np.arange(0, sig_len, FLAGS.beat_feature_len)[:, None, None]

                    elif FLAGS.beat_overlap == -1:
                        sig_len = sig_len - FLAGS.beat_feature_len
                        beat_label_len = FLAGS.beat_feature_len // FLAGS.beat_block_len
                        frame_index = np.arange(FLAGS.beat_feature_len)[None, :] + \
                                      np.arange(0, sig_len, FLAGS.beat_feature_len)[:, None]

                        frame_index_ = []
                        for i in range(len(frame_index)):
                            frame_index_.append(frame_index[i])
                            frame_index_.append(frame_index[i] + 16)
                        frame_index = np.asarray(frame_index_, dtype='int32')

                        label_index = np.arange(beat_label_len)[None, None, :] + \
                                      np.arange(0, FLAGS.beat_feature_len, beat_label_len)[None, :, None] + \
                                      np.arange(0, sig_len, FLAGS.beat_feature_len)[:, None, None]
                        label_index_ = []
                        for i in range(len(label_index)):
                            label_index_.append(label_index[i])
                            label_index_.append(label_index[i] +
                                                np.ones((FLAGS.beat_block_len,
                                                         int(FLAGS.beat_feature_len / FLAGS.beat_block_len)),
                                                        dtype='int32') * 16)
                        label_index = np.asarray(label_index_, dtype='int32')
                    else:
                        sig_len = sig_len - FLAGS.beat_feature_len
                        beat_label_len = FLAGS.beat_feature_len // FLAGS.beat_block_len
                        frame_index = np.arange(FLAGS.beat_feature_len)[None, :] + \
                                      np.arange(0, sig_len, FLAGS.beat_overlap)[:, None]

                        label_index = np.arange(beat_label_len)[None, None, :] + \
                                      np.arange(0, FLAGS.beat_feature_len, beat_label_len)[None, :, None] + \
                                      np.arange(0, sig_len, FLAGS.beat_overlap)[:, None, None]

                    frame_data = buf_ecg_beat[frame_index]
                    samp_len = len(frame_data)
                    beat_candidate = []
                    batch_index = 0
                    batch_size = data_model.BATCH_SIZE
                    try:
                        while batch_index < samp_len:
                            try:
                                _data = np.reshape(frame_data[batch_index: min(batch_index + batch_size,
                                                                               samp_len)],
                                                   [-1, FLAGS.beat_feature_len, 1])
                                if len(_data) == 0:
                                    break
                                # request_predict_image.inputs['segment'].CopyFrom(
                                #     tf.make_tensor_proto(_data, dtype=tf.float32))
                                # result = stub.Predict(request_predict_image)
                                # _output = np.array(result.outputs['arrhythmia'].int64_val)
                                _output = eval_model.predict(_data)
                                _output = np.argmax(_output, -1)
                                _output = np.reshape(_output, (-1, 480))

                                if len(beat_candidate) == 0:
                                    beat_candidate = _output
                                else:
                                    beat_candidate = np.concatenate((beat_candidate, _output))

                                batch_index += _data.shape[0]
                            except Exception as err:
                                print(err)
                                return

                        if len(beat_candidate) > 0:
                            frame_label = buf_ecg_beat[label_index]
                            amps = np.max(np.absolute(frame_label[beat_candidate > 0, :]), axis=1)
                            beats = np.argmax(np.absolute(frame_label[beat_candidate > 0, :]), axis=1)

                            beats = beats + label_index[beat_candidate > 0, 0]
                            ind_revert = {i: k for i, k in enumerate(label_arrhythmia.keys())}
                            symbols = beat_candidate[beat_candidate > 0]
                            symbols = [
                                label_arrhythmia[ind_revert[sym]][0] if sym < FLAGS.beat_class_num else '|'
                                for sym in symbols]
                            symbols = [x for _, x in sorted(zip(beats, symbols))]
                            amps = [x for _, x in sorted(zip(beats, amps))]
                            beats = np.sort(beats)
                        else:
                            beats = []
                            amps = []
                            symbols = []

                    except Exception as e:  # pylint: disable=broad-except:
                        print(e)
                        break

                    beats = np.asarray(beats)
                    valid_beats = []
                    valid_symbols = []
                    if len(beats) > 0:
                        groups_beat = [beats[0]]
                        groups_amp = [amps[0]]
                        groups_symbol = [symbols[0]]
                        for index in range(1, len(beats)):
                            if ((beats[index] - groups_beat[-1]) / data_model.SAMPLING_RATE) > \
                                    data_model.MIN_RR_INTERVAL:

                                check_q = np.where('|' == np.asarray(groups_symbol))[0]
                                if 0 < len(check_q) < len(groups_symbol):
                                    for q in check_q:
                                        groups_symbol[q] = 'N'

                                valid_beats.append(groups_beat[int(np.argmax(np.asarray(groups_amp)))])

                                d = defaultdict(int)
                                for i in groups_symbol:
                                    d[i] += 1

                                result = max(d.items(), key=lambda x: x[1])

                                valid_symbols.append(result[0])
                                groups_beat.clear()
                                groups_symbol.clear()
                                groups_amp.clear()

                            groups_amp.append(amps[index])
                            groups_beat.append(beats[index])
                            groups_symbol.append(symbols[index])

                        if len(groups_beat) > 0:
                            valid_beats.append(groups_beat[int(np.argmax(np.asarray(groups_amp)))])

                            d = defaultdict(int)
                            for i in groups_symbol:
                                d[i] += 1

                            result = max(d.items(), key=lambda x: x[1])
                            valid_symbols.append(result[0])

                        valid_beats = np.asarray(valid_beats)
                        valid_symbols = np.asarray(valid_symbols)

                    beats = valid_beats.copy()
                    symbols = valid_symbols.copy()

                except Exception as e:  # pylint: disable=broad-except
                    print("BEATNET: {}".format(e))
                # endregion BEATNET

                if len(total_peak) == 0 and len(beats) > 2:
                    beats = (beats * fs_origin) // data_model.SAMPLING_RATE
                    total_peak = beats + samp_from
                    total_symbol = symbols

                elif len(beats) > 2:
                    beats = (beats * fs_origin) // data_model.SAMPLING_RATE
                    tmp_peaks = beats + samp_from
                    inx = len(total_peak)
                    for inx in reversed(range(0, len(total_peak))):
                        if (data_model.MIN_RR_INTERVAL * fs_origin) < tmp_peaks[0] - total_peak[inx]:
                            inx += 1
                            break

                    total_peak = np.concatenate((total_peak[:inx], tmp_peaks), axis=0)
                    total_symbol = np.concatenate((total_symbol[:inx], symbols), axis=0)

                if samp_from < (samp_to - int(data_model.NUM_OVERLAP * fs_origin)):
                    samp_to -= int(data_model.NUM_OVERLAP * fs_origin)

                samp_from = samp_to

            except Exception as e:
                print("FULL: {}".format(e))
                break

    except Exception as e:  # pylint: disable=broad-except
        print(e)

    return fs_origin, np.asarray(total_peak, dtype=int), total_symbol


def run_qrs_beat_noise(check_point_noise,
                       check_point_beat,
                       file_test,
                       channel_ecg=0,
                       use_gpu_index=0):
    return data_model.SAMPLING_RATE, np.asarray([], dtype=int), np.asarray([], dtype=int), []


def run_qrs(check_point_qrs,
            file_test,
            channel_ecg=0,
            use_gpu_index=0):
    """

    :param check_point_qrs:
    :param file_test:
    :param channel_ecg:
    :param use_gpu_index:
    :param lowpass:
    :return:
    """
    total_peak = []
    fs_origin = 0

    tf.logging.set_verbosity(tf.logging.ERROR)

    with tf.device('/gpu:{}'.format(use_gpu_index if use_gpu_index >= 0 else 0)):
        qrs = tf.Graph()
        with qrs.as_default():
            str_version = check_point_qrs.split('/')
            for i, v in enumerate(str_version):
                if v == "checkpoint":
                    break

            version_qrs = str_version[i - 1]
            model_name_qrs = str_version[i + 1]
            qrs_inputs = tf.placeholder(tf.float32, shape=(None, FLAGS.beat_qrs_num_feature, 1))
            with tf.variable_scope("{}_{}".format(model_name_qrs, version_qrs)):
                qrs_logits = model.build(
                    name=model_name_qrs,
                    inputs=qrs_inputs,
                    num_of_class=FLAGS.beat_qrs_num_class,
                    is_training=False
                )
                qrs_predictions = tf.argmax(qrs_logits, axis=1)

    try:
        header = wf.rdheader(file_test)

        samp_to = 0
        samp_from = 0
        fs_origin = header.fs

        while samp_to <= header.sig_len:
            try:
                samp_len = min(int(data_model.MAX_SAMPLE_PROCESS * fs_origin), (header.sig_len - samp_from))

                if samp_len <= 0:
                    break

                samp_to = samp_from + samp_len
                record = wf.rdsamp(file_test, sampfrom=samp_from, sampto=samp_to, channels=[channel_ecg])
                buf_record = np.nan_to_num(record[0][:, 0])
                fs_origin = record[1].get('fs')
                if fs_origin != data_model.SAMPLING_RATE:
                    buf_ecg, _ = resample_sig(buf_record, fs_origin, data_model.SAMPLING_RATE)
                else:
                    buf_ecg = buf_record.copy()

                # high_pass_buf = butter_highpass_filter(buf_ecg, 0.5, data_model.SAMPLING_RATE)
                # low_pass_buf = butter_lowpass_filter(high_pass_buf, 40, data_model.SAMPLING_RATE)
                buf_ecg_qrs = buf_ecg.copy()
                bwr_buf_ecg = bwr_dwt2(buf_ecg, data_model.SAMPLING_RATE)
                if FLAGS.beat_qrs_bwr:
                    buf_ecg_qrs = bwr_buf_ecg.copy()

                nor_buf_ecg = norm(bwr_buf_ecg, data_model.NUM_NORMALIZATION, samp_from, samp_to)
                if FLAGS.beat_qrs_norm:
                    buf_ecg_qrs = nor_buf_ecg.copy()

                index_qrs_sample = np.arange(FLAGS.beat_qrs_num_feature)[None, :] + \
                                   np.arange(len(nor_buf_ecg) - FLAGS.beat_qrs_num_feature)[:, None]

                nor_buf_qrs = buf_ecg_qrs[index_qrs_sample]
                batch_index = 0
                qrs_candidate = []
                samp_len = len(nor_buf_qrs)
                batch_size = data_model.BATCH_SIZE
                # region QRS
                try:
                    if use_gpu_index >= 0:
                        config = tf.ConfigProto(allow_soft_placement=True)
                    else:
                        config = tf.ConfigProto()

                    config.gpu_options.allow_growth = True
                    with tf.Session(config=config, graph=qrs) as sess:
                        sess.run(tf.global_variables_initializer())
                        ckpt = tf.train.get_checkpoint_state(check_point_qrs)
                        if ckpt and ckpt.model_checkpoint_path:
                            restorer = tf.train.Saver()
                            restorer.restore(sess, tf.train.latest_checkpoint(check_point_qrs))
                        else:
                            print('No check_point_qrs file found')
                            return

                        # Start the queue runners.
                        coord = tf.train.Coordinator()
                        try:
                            threads = []
                            for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
                                threads.extend(qr.create_threads(sess, coord=coord, daemon=True, start=True))

                            while batch_index <= len(index_qrs_sample):
                                try:
                                    _data = np.reshape(
                                        nor_buf_qrs[batch_index: min(batch_index + batch_size, samp_len)],
                                        [-1, FLAGS.beat_qrs_num_feature, 1])
                                    if len(_data) == 0:
                                        break
                                    if len(qrs_candidate) == 0:
                                        qrs_candidate = (sess.run(qrs_predictions,
                                                                  feed_dict={qrs_inputs: _data})).flatten()
                                    else:
                                        qrs_candidate = np.concatenate((qrs_candidate,
                                                                        (sess.run(qrs_predictions,
                                                                                  feed_dict={
                                                                                      qrs_inputs: _data})).flatten()))

                                    batch_index += _data.shape[0]
                                except Exception as err:
                                    print(err)
                                    return

                            # region post-processing NEW
                            groups, groups_len = agglomerative_clustering(qrs_candidate, data_model.SAMPLING_RATE)
                            if len(groups) > 0:
                                beats = select_beat_from_group(groups,
                                                               groups_len,
                                                               bwr_buf_ecg,
                                                               data_model.SAMPLING_RATE,
                                                               FLAGS.beat_qrs_offset_len,
                                                               data_model.MIN_RR_INTERVAL)

                            else:
                                beats = []
                            # endregion post-processing NEW
                        except Exception as e:  # pylint: disable=broad-except
                            coord.request_stop(e)

                        coord.request_stop()
                        coord.join(threads, stop_grace_period_secs=10)
                        beats = np.asarray(beats)

                except Exception as e:  # pylint: disable=broad-except
                    print("QRS: {}".format(e))
                # endregion QRS

                beats = (beats * fs_origin) // data_model.SAMPLING_RATE
                if len(total_peak) == 0 and len(beats) > FLAGS.beat_beat_befor:
                    total_peak = beats + samp_from
                elif len(beats) > FLAGS.beat_beat_befor and \
                        ((beats[FLAGS.beat_beat_befor] + samp_from) - total_peak[-FLAGS.beat_beat_after] <
                         (data_model.MIN_RR_INTERVAL * fs_origin)):
                    tmp_peaks = beats[FLAGS.beat_beat_befor:] + samp_from
                    inx = len(total_peak) - FLAGS.beat_beat_after
                    for inx in reversed(range(0, len(total_peak) - FLAGS.beat_beat_after)):
                        if (data_model.MIN_RR_INTERVAL * fs_origin) < tmp_peaks[0] - total_peak[inx]:
                            inx += 1
                            break

                    total_peak = np.concatenate((total_peak[:inx], tmp_peaks), axis=0)

                elif len(beats) > FLAGS.beat_beat_befor:
                    tmp_peaks = beats + samp_from
                    inx = len(total_peak)
                    for inx in reversed(range(0, len(total_peak))):
                        if (data_model.MIN_RR_INTERVAL * fs_origin) < tmp_peaks[0] - total_peak[inx]:
                            inx += 1
                            break

                    total_peak = np.concatenate((total_peak[:inx], tmp_peaks), axis=0)

                if samp_from < (samp_to - int(data_model.NUM_OVERLAP * fs_origin)):
                    samp_to -= int(data_model.NUM_OVERLAP * fs_origin)

                samp_from = samp_to

            except Exception as e:
                print("FULL: {}".format(e))
                break

    except Exception as e:  # pylint: disable=broad-except
        coord.request_stop(e)

    return fs_origin, np.asarray(total_peak, dtype=int)


def process_run_qrs(file_list,
                    path2model_qrs,
                    use_gpu_index,
                    beat_ext_ai):
    """

    :param file_list:
    :param path2model_qrs:
    :param path2model_noise:
    :param path2model_beat:
    :param use_gpu_index:
    :param beat_ext_ai:
    :return:
    """
    log_lines = []

    for file in file_list:
        dir_test = os.path.dirname(file)
        try:

            channel = 0
            if basename(file) == '114':
                channel = 1

            start_time = time.time()
            fs_origin, total_peak = run_qrs(check_point_qrs=path2model_qrs,
                                            file_test=file,
                                            channel_ecg=channel,
                                            use_gpu_index=use_gpu_index)

            end_time = time.time()
            str_log = '{} with {} beats take {} seconds\n'.format(
                basename(dirname(file)) + '/' + basename(file),
                len(total_peak),
                end_time - start_time)
            print(str_log)
            log_lines.append(str_log)

            curr_dir = os.getcwd()
            os.chdir(dir_test + '/')
            total_symbol = ['N' for _ in range(len(total_peak))]
            annotation2 = wf.Annotation(record_name=basename(file),
                                        extension=beat_ext_ai,
                                        sample=np.asarray(total_peak),
                                        symbol=np.asarray(total_symbol),
                                        fs=fs_origin)
            annotation2.wrann(write_fs=True)
            os.chdir(curr_dir)

        except Exception as e:
            print(e)
            pass

    return log_lines


def check_qrs(use_gpu_index,
              path2model_qrs,
              test_dir,
              physionet_directory,
              num_of_process):
    """

    :param use_gpu_index:
    :param path2model_qrs:
    :param test_dir:
    :param physionet_directory:
    :param num_of_process:
    :return:
    """

    str_version = path2model_qrs.split('/')
    for i, v in enumerate(str_version):
        if v == "checkpoint":
            break

    qrs_ext_ai = str_version[i - 1]

    replacements = dict()
    for m in range(26):
        replacements[str(m)] = str(chr(m + 97))

    for m in range(26, 52):
        replacements[str(m)] = str(chr(m - 26 + 65))

    for m in range(52, 78):
        replacements[str(m)] = 'a'

    for m in range(78, 104):
        replacements[str(m)] = 'b'

    for m in range(104, 130):
        replacements[str(m)] = 'c'

    for m in range(130, 156):
        replacements[str(m)] = 'd'

    for m in range(156, 182):
        replacements[str(m)] = 'e'

    for m in range(182, 208):
        replacements[str(m)] = 'f'

    for m in range(208, 513):
        replacements[str(m)] = 'g'

    qrs_ext_ai = re.sub('(\\d+)', lambda m: replacements[m.group()], qrs_ext_ai)
    qrs_ext_ai = qrs_ext_ai.replace('.', '')
    qrs_ext_ai = qrs_ext_ai.replace('-', '')
    if 'best_loss' in path2model_qrs:
        qrs_ext_ai += 'l'
    else:
        qrs_ext_ai += 'm'

    ext_ai = qrs_ext_ai + 'atr'

    path2db = physionet_directory + test_dir

    file_names = glob(path2db + '/*.dat')
    # Get rid of the extension
    file_names = [p[:-4] for p in file_names
                  if basename(p)[:-4] not in ['104', '102', '107', '217', 'bw', 'em', 'ma']
                  if '_200hz' not in basename(p)[:-4]]

    file_names = sorted(file_names)

    num_file_each_process = int(len(file_names) / num_of_process)
    file_process_split = [file_names[x:x + num_file_each_process] for x in range(0, len(file_names),
                                                                                 num_file_each_process)]
    arg_list = list()
    for file_list in file_process_split:
        arg = (file_list, path2model_qrs, use_gpu_index, ext_ai)
        arg_list.append(arg)

    with Pool(processes=num_of_process) as pool:
        for log_lines in pool.starmap(process_run_qrs, arg_list):
            print(log_lines)


if __name__ == '__main__':
    tf.compat.v1.flags.DEFINE_integer('beat_block_len', 480, '')
    tf.compat.v1.flags.DEFINE_integer('beat_segment_len', 60, '')
    tf.compat.v1.flags.DEFINE_integer('beat_overlap', -1, '')
    tf.compat.v1.flags.DEFINE_integer('beat_feature_len', 15360, '')
    tf.compat.v1.flags.DEFINE_integer('beat_channel_num', 0, '')
    tf.compat.v1.flags.DEFINE_float('beat_offset_len', 0.000, 'ms')
    tf.compat.v1.flags.DEFINE_integer('beat_bwr', 3, '')
    tf.compat.v1.flags.DEFINE_boolean('beat_norm', False, '')
    tf.compat.v1.flags.DEFINE_string('beat_class', "11", '')
    tf.compat.v1.flags.DEFINE_integer('beat_class_num', 4, '')
    run_qrs_beat(
        check_point_beat='/mnt/ai_data/MegaDataset/beat_classification/210126test/'
                         'beatv63subject.480.60.0.0.0.3.0.12.0.0/checkpoint/beat_seq2/best_new_f1',
        # file_test='/mnt/ai_data/PhysionetData/ahadb/8210',
        # file_test='/mnt/ai_data/PhysionetData/ahadb/4206',
        file_test='/mnt/ai_data/PhysionetData/mitdb/208',
        # file_test='/mnt/ai_data/PhysionetData/escdb/e0110',
        # file_test='/mnt/ai_data/PortalData/data/study-22489/data-all-09-02-20-13-41-37-16',
        channel_ecg=0,
    )
